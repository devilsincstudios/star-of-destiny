// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(BoxCollider))]
[AddComponentMenu("AI/Waypoint AI/Star Waypoint")]
public class StarAIWaypoint : AIWaypoint
{
	public List<AIWaypoint> meleeWaypoints;
	public List<AIWaypoint> rangedWaypoints;
	public AIWaypoint bossWaypoint;

	public Color meleeColor;
	public bool displayMeleeGizmo = false;
	public Color rangedColor;
	public bool displayRangedGizmo = false;
	public Color bossColor;
	public bool displayBossGizmo = false;

	public AIWaypoint getNextWaypoint(Enemies type)
	{
		if (type == Enemies.SNOWMAN_BOSSMAN) {
			return bossWaypoint;
		}
		else if (type == Enemies.SNOWMAN_RANGEMAN || type == Enemies.SNOWMAN_RAMBOMAN) {
			return rangedWaypoints[UnityEngine.Random.Range(0, rangedWaypoints.Count)];
		}
		else {
			return meleeWaypoints[UnityEngine.Random.Range(0, meleeWaypoints.Count)];
		}
	}

	new public void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Enemy") {
			Enemy enemy = other.GetComponent<Enemy>();
			nextWaypoint = getNextWaypoint(enemy.type);
			if (nextWaypoint != null) {
				other.GetComponent<WaypointAI>().target = nextWaypoint;
			}
		}
	}
	
	new public void OnDrawGizmosSelected()
	{
		if (displayMeleeGizmo) {
			if (meleeWaypoints.Count > 0) {
				for (int index = 0; index < meleeWaypoints.Count; ++index) {
					if (meleeWaypoints[index] != null) {
						Gizmos.color = meleeColor;
						Gizmos.DrawLine(transform.position, meleeWaypoints[index].transform.position);
					}
				}
			}
		}

		if (displayRangedGizmo) {
			if (rangedWaypoints.Count > 0) {
				for (int index = 0; index < rangedWaypoints.Count; ++index) {
					if (rangedWaypoints[index] != null) {
						Gizmos.color = rangedColor;
						Gizmos.DrawLine(transform.position, rangedWaypoints[index].transform.position);
					}
				}
			}
		}

		if (displayBossGizmo) {
			if (bossWaypoint != null) {
				Gizmos.color = bossColor;
				Gizmos.DrawLine(transform.position, bossWaypoint.transform.position);
			}
		}
	}
}
