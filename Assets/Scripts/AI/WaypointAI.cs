// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System;

[RequireComponent(typeof(NavMeshAgent))]
//[RequireComponent(typeof(CharacterController))]
[AddComponentMenu("AI/Waypoint AI/Waypoint AI Agent")]
public class WaypointAI : MonoBehaviour
{
	[Header("Agent Settings")]
	[HideInInspector]
	public NavMeshAgent agent;
	public bool isEnemy = true;
	public bool agentActive = true;

	[Header("Waypoint Settings")]
	public AIWaypoint target;
	public bool reachedDestination = false;
	public bool waypointIsStar = false;
	public float distanceToTarget = 0.0f;

	[Header("Entity Settings")]
	public Enemy enemyData;
	private RangedEnemy rangedEnemyData;
	private RamboMan ramboManData;
	public Entity entity;

	[Header("Animation Settings")]
	public bool hasAnimations = false;
	
	[HideInInspector]
	public Animator animator;
	[HideInInspector]
	public GameStateController gsc;
	[HideInInspector]
	public Star star;
	private bool isEntityDead = false;

	public void OnEnable()
	{
		if (!isEntityDead) {
			agent = GetComponent<NavMeshAgent>();
			star = GameObject.FindWithTag("Star").GetComponent<Star>();
			gsc = GameObject.Find("Game State").GetComponent<GameStateController>();
			if (tag == "Enemy") {
				enemyData = GetComponent<Enemy>();
				agent.speed = enemyData.movementSpeed;
				if (!enemyData.canMove) {
					reachedDestination = true;
				}
				entity = GetComponent<Enemy>();
				isEnemy = true;
				if (enemyData.ranged) {
					//TODO Clean this up this is just bad.
					if (enemyData.type == Enemies.SNOWMAN_RAMBOMAN) {
						ramboManData = GetComponent<RamboMan>();
					}
					else {
						rangedEnemyData = GetComponent<RangedEnemy>();
					}
				}
			}
			else {
				isEnemy = false;
			}
			
			if (hasAnimations) {
				animator = GetComponentInChildren<Animator>();
			}
		}
		else {
			isEntityDead = false;
			reachedDestination = false;
			waypointIsStar = false;

			if (hasAnimations) {
				animator.SetBool("Dead", false);
				animator.SetFloat("Speed", 0.0f);
				animator.SetBool("Attacking", false);
				animator.Play("Idle");
			}
		}

		agentActive = true;
	}
	
	public void Update()
	{
		if (!isEntityDead) {
			if (gsc.gameState == GameStates.PLAYING) {
				if (entity.canMove) {
					if (entity.isDead) {
						try {
							agent.Stop();
							isEntityDead = true;
						}
						//Catch for not being on a navmesh
						catch (Exception) {
						}
					}
					else {
						agent.enabled = true;
						if (waypointIsStar) {
							reachedDestination = true;
							agent.Stop();
							if (enemyData.canAttack) {
								if (!enemyData.isAttacking) {
									if (enemyData.ranged) {
										if (enemyData.type == Enemies.SNOWMAN_RAMBOMAN) {
											StartCoroutine(ramboManData.Attack(star));
										}
										else {
											StartCoroutine(rangedEnemyData.Attack(star));
										}
									}
									else {
										StartCoroutine(enemyData.Attack(star));
									}
									StartAttack();
								}
							}
						}
						else {
							if (target != null) {
								distanceToTarget = Vector3.Distance(transform.position, target.transform.position);
								agent.SetDestination(target.transform.position);
								StartRun();

								if (distanceToTarget <= agent.stoppingDistance) {
									reachedDestination = true;
									agent.Stop();
									StartIdle();
								}
							}
						}
					}
				}
				else {
					stopAgent();
					StartIdle();
				}
			}
			else {
				agent.enabled = false;
				StartIdle();
			}
		}
	}
	
	public void LateUpdate()
	{
		if (reachedDestination == true) {
			// Don't look up at the star look forward towards it.
			transform.LookAt(new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z));
			transform.localRotation.Set(0, transform.localRotation.y, transform.localRotation.z, transform.localRotation.w);
		}
		else {
			agent.speed = enemyData.movementSpeed;
		}
	}

	public void stopAgent()
	{
		if (agentActive) {
			try {
				agent.Stop();
			}
			catch (Exception agentException) {
			}
			agentActive = false;
		}
	}

	public void startAgent()
	{
		if (!agentActive) {
			agent.Resume();
			agentActive = true;
		}
	}

	public void OnDrawGizmos()
	{
		if (target != null) {
			Gizmos.color = Color.yellow;
			Gizmos.DrawLine(transform.position, target.transform.position);
			Gizmos.DrawCube(target.transform.position, new Vector3 (0.2f, 1.0f, 0.2f));
		}
	}

	public void StartIdle()
	{
		if (hasAnimations) {
			animator.SetFloat("Speed", 0.0f);
			animator.SetBool("Attacking", false);
		}
	}

	public void StartRun()
	{
		if (hasAnimations) {
			animator.SetFloat("Speed", 1.0f);
			animator.SetBool("Attacking", false);
		}
	}

	public void StartAttack()
	{
		if (hasAnimations) {
			animator.SetFloat("Speed", 0.0f);
			animator.SetBool("Attacking", true);
		}
	}
}
