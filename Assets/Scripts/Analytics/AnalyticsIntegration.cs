//// Devils Inc Studios
//// // Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
////
//// TODO: Include a description of the file here.
////
//
//using UnityEngine;
//using System.Collections;
//using UnityEngine.Cloud.Analytics;
//
//[AddComponentMenu("Analytics/Integration")]
//public class AnalyticsIntegration : MonoBehaviour {
//	public string appId;
//
//	// Use this for initialization
//	void Start () {
//		UnityAnalytics.SetLogLevel(LogLevel.Info, true);
//		AnalyticsResult result = UnityAnalytics.StartSDK (appId);
//		Debug.Log(result.ToString());
//	}
//}