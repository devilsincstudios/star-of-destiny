﻿// // Devils Inc Studios
// // Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
// //
// // TODO: This script is to allow the player to switch weapons and shoot
// //


using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[AddComponentMenu("Character/FPS Shooting")]
public class FPSShooting : MonoBehaviour {
	[Header("Combat Settings")]
	public bool inCombat = false;
	public float combatTime = 5.0f;
	public float combatTimer = 0.0f;
	[HideInInspector]
	public List<float> shotDelayTimers;

	[Header("Key Binds")]
	public string shootKeyBinding;
	public string weaponSelectOneKeyBinding;
	public string weaponSelectTwoKeyBinding;
	public string weaponSelectThreeKeyBinding;
	public string weaponSelectFourKeyBinding;
	public string weaponSelectFiveKeyBinding;
	public string weaponSelectSixKeyBinding;
	public string weaponSelectSevenKeyBinding;

	[Header("Steam Thrower Settings")]
	public SteamThrower steamThrowerWeapon;

	[Header("Camera Setup")]
	public Camera playerCamera;

	[HideInInspector]
	public PlayerState playerState;
	[HideInInspector]
	public GameObject selectedWeapon;

	private Ammo ammo;
	private int ammoTypeId;
	private float shotDelayTimer = 0.0f;
	private float shotDelay = 0.0f;

	// Use this for initialization
	public void Start ()
	{
		playerState = this.GetComponent<PlayerState>();
		SetPlayersSelectedWeapon(WeaponType.SNOWBALL, AmmoTypes.AMMO_SNOWBALL);

		shotDelayTimers = new List<float>();
		foreach (WeaponType type in (WeaponType[]) Enum.GetValues(typeof(WeaponType))) {
			shotDelayTimers.Insert((int)type, 0.0f);
		}
	}
	
	// Update is called once per frame
	public void Update ()
	{
		if (playerState.gsc.gameState == GameStates.PLAYING) {
			if (!playerState.isPlacingTowers) {

				if(Input.GetButton(shootKeyBinding)){
					if (shotDelayTimer == 0.0f) {
						ShootProjectile();
					}
				}
				
				if(Input.GetButtonDown(weaponSelectOneKeyBinding)){
					SetPlayersSelectedWeapon(WeaponType.SNOWBALL, AmmoTypes.AMMO_SNOWBALL);
				}
				
				if(Input.GetButtonDown(weaponSelectTwoKeyBinding)){
					SetPlayersSelectedWeapon(WeaponType.CANDY_CANE, AmmoTypes.AMMO_CANDY_CANE);
				}
				
				if(Input.GetButtonDown(weaponSelectThreeKeyBinding)){
					SetPlayersSelectedWeapon(WeaponType.ICICLE, AmmoTypes.AMMO_ICICLE);
				}
				
				if(Input.GetButtonDown(weaponSelectFourKeyBinding)){
					SetPlayersSelectedWeapon(WeaponType.STEAM, AmmoTypes.AMMO_STEAM);
				}
			
				if(Input.GetButtonDown(weaponSelectFiveKeyBinding)){
					SetPlayersSelectedWeapon(WeaponType.BAUBLE_GRENADE, AmmoTypes.AMMO_BAUBLE_GRENADE);
				}

				if(Input.GetButtonDown(weaponSelectSixKeyBinding)){
					SetPlayersSelectedWeapon(WeaponType.PRESENT_BOMB, AmmoTypes.AMMO_PRESENT_BOMB);
				}

				if(Input.GetButtonDown(weaponSelectSevenKeyBinding)){
					SetPlayersSelectedWeapon(WeaponType.SANTA_BOMBER, AmmoTypes.AMMO_SANTA_BOMBER);
				}

				for (int index = 0; index < shotDelayTimers.Count; ++index) {
					if (shotDelayTimers[index] > 0.0f) {
						shotDelayTimers[index] -= Time.deltaTime;
					}
					if (shotDelayTimers[index] < 0.0f) {
						shotDelayTimers[index] = 0.0f;
					}
				}

				combatTimer = Mathf.Clamp(combatTimer - Time.deltaTime, 0, combatTime);
				
				if (combatTimer <= 0.0f) {
					combatTimer = 0.0f;
					inCombat = false;
					DI_Events.EventCenter.invoke("OnExitCombat");
				}
			}
			else {
				if (!playerState.gridSelected() && !playerState.gridHighlighted()) {
					if(Input.GetButton(shootKeyBinding)) {
						playerState.isPlacingTowers = false;
						playerState.deselectGrid();
						playerState.unHighlightGrid();
						DI_Events.EventCenter.invoke("onDisableGrids");
					}
				}
			}
		}
	}

	public void SetPlayersSelectedWeapon(WeaponType weaponType, AmmoTypes ammoType)
	{
		if (weaponType != WeaponType.STEAM) {
			selectedWeapon = playerState.prefabLoader.getPrefab(weaponType.ToString());
			#if DEBUG
				Debug.Log (weaponType.ToString());
			#endif
			playerState.setSelectedWeapon(weaponType);
			playerState.setSelectedAmmo(ammoType);
			if (selectedWeapon.GetComponent<ProjectileController>() == null) {
				if (weaponType == WeaponType.SANTA_BOMBER) {
					shotDelay =	selectedWeapon.GetComponent<SantaBomber>().shotDelay;
				}
				else {
					shotDelay =	selectedWeapon.GetComponent<BaubleProjectileController>().shotDelay;
				}
			}
			else {
				shotDelay = selectedWeapon.GetComponent<ProjectileController>().shotDelay;
			}
		}
		else {
			selectedWeapon = steamThrowerWeapon.gameObject;
			playerState.setSelectedAmmo(AmmoTypes.AMMO_STEAM);
			playerState.setSelectedWeapon(WeaponType.STEAM);
			shotDelay = steamThrowerWeapon.attackSpeed;
		}
	}

	public void ShootProjectile()
	{
		int ammoTypeId;
		float ammoCount;

		if (shotDelayTimers[(int)playerState.getSelectedWeapon()] <= 0.0f) {
			switch (playerState.getSelectedWeapon()) {
			case WeaponType.SNOWBALL:
				ammoTypeId = playerState.getAmmoTypeId(AmmoTypes.AMMO_SNOWBALL);
				ammoCount = playerState.getAmmo(AmmoTypes.AMMO_SNOWBALL);
				break;
			case WeaponType.CANDY_CANE:
				ammoTypeId = playerState.getAmmoTypeId(AmmoTypes.AMMO_CANDY_CANE);
				ammoCount = playerState.getAmmo(AmmoTypes.AMMO_CANDY_CANE);
				break;
			case WeaponType.ICICLE:
				ammoTypeId = playerState.getAmmoTypeId(AmmoTypes.AMMO_ICICLE);
				ammoCount = playerState.getAmmo(AmmoTypes.AMMO_ICICLE);
				break;
			case WeaponType.STEAM:
				ammoTypeId = playerState.getAmmoTypeId(AmmoTypes.AMMO_STEAM);
				ammoCount = playerState.getAmmo(AmmoTypes.AMMO_STEAM);
				break;
			case WeaponType.BAUBLE_GRENADE:
				ammoTypeId = playerState.getAmmoTypeId(AmmoTypes.AMMO_BAUBLE_GRENADE);
				ammoCount = playerState.getAmmo(AmmoTypes.AMMO_BAUBLE_GRENADE);
				break;
			case WeaponType.PRESENT_BOMB:
				ammoTypeId = playerState.getAmmoTypeId(AmmoTypes.AMMO_PRESENT_BOMB);
				ammoCount = playerState.getAmmo(AmmoTypes.AMMO_PRESENT_BOMB);
				break;
			case WeaponType.SANTA_BOMBER:
				ammoTypeId = playerState.getAmmoTypeId(AmmoTypes.AMMO_SANTA_BOMBER);
				ammoCount = playerState.getAmmo(AmmoTypes.AMMO_SANTA_BOMBER);
				break;
			default:
				ammoTypeId = -1;
				ammoCount = 0;
				break;
			}

			if (ammoTypeId != -1) {
				if (ammoCount > 0) {
					if (playerState.getSelectedWeapon() != WeaponType.STEAM) {
						steamThrowerWeapon.isActiveWeapon = false;
					}
					else {
						steamThrowerWeapon.isActiveWeapon = true;
					}

					// Get the center of the view port.
					Vector3 instPoint = playerCamera.ViewportToWorldPoint(new Vector3(0.51f, 0.5f, Camera.main.nearClipPlane + 1.5f));
					// Spawn the prefab and move / rotate it to the desired location

					#if DEBUG
					// Debug stuff
	//				Debug.Log("Player ---> X: " + playerCamera.transform.rotation.x + " Y: " + transform.rotation.y + " Z: " + transform.rotation.z + " W: " + transform.rotation.w);
	//				Debug.Log("Quaternion ---> X: " + rotation.x + " Y: " + rotation.y + " Z: " + rotation.z + " W: " + rotation.w);
	//				Debug.Log("Prefab ---> X: " + firedProjectile.transform.rotation.x + " Y: " + firedProjectile.transform.rotation.y + " Z: " + firedProjectile.transform.rotation.z + " W: " + firedProjectile.transform.rotation.w);
					#endif
					//Bauble Grenade Throw Code
					if(ammoTypeId == playerState.getAmmoTypeId(AmmoTypes.AMMO_BAUBLE_GRENADE)) {
						GameObject firedProjectile = PoolController.instance.getPooledObject(selectedWeapon.name);
						firedProjectile.SetActive(true);

						firedProjectile.transform.position = instPoint;
						// Take the cameras X and the players Y as the mouse look script changes those.
						firedProjectile.transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0);

						// Get the projectiles controller
						//TODO Is BaubleProjectileController still needed after the AOEDamage merge?
						BaubleProjectileController projCont = firedProjectile.GetComponent<BaubleProjectileController>();
						float speed = projCont.projectileSpeed;
						// Apply force to move the projectile
						firedProjectile.GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * speed);
						RemovePlayerAmmo(ammoCount);

						// XP System bonus damage.
						// Exploding Ammo base x 25
						int xpId = playerState.getXpId(AmmoTypes.AMMO_BAUBLE_GRENADE);
						AOEDamage aoeDamage = firedProjectile.GetComponent<AOEDamage>();
						aoeDamage.damage = aoeDamage.originalDamage + (playerState.getLevel(xpId) * 25);

						// Presents and Baubles share a common cooldown.
						if (shotDelayTimers[(int)WeaponType.PRESENT_BOMB] < shotDelay) {
							shotDelayTimers[(int)WeaponType.PRESENT_BOMB] = shotDelay;
						}
						shotDelayTimers[(int)WeaponType.BAUBLE_GRENADE] = shotDelay;

						DI_Events.EventCenter<AmmoTypes, PlayerState>.invoke("OnFire", playerState.getSelectedAmmoType(), playerState);
					}
					//Present Bomb place code
					else if(ammoTypeId == playerState.getAmmoTypeId(AmmoTypes.AMMO_PRESENT_BOMB)) {
						GameObject presentBomb = PoolController.instance.getPooledObject(selectedWeapon.name);
						presentBomb.SetActive(true);

						presentBomb.transform.position = Camera.main.ViewportToWorldPoint(new Vector3(0.51f, 0.5f, Camera.main.nearClipPlane + 2.5f));
						presentBomb.transform.localEulerAngles = new Vector3(0.0f, (float)UnityEngine.Random.Range(0, 180), 0.0f);
						RemovePlayerAmmo(ammoCount);

						// XP System bonus damage.
						// Exploding Ammo base x 25
						int xpId = playerState.getXpId(AmmoTypes.AMMO_PRESENT_BOMB);
						AOEDamage aoeDamage = presentBomb.GetComponent<AOEDamage>();
						aoeDamage.damage = aoeDamage.originalDamage + (playerState.getLevel(xpId) * 25);
						// Presents and Baubles share a common cooldown.
						if (shotDelayTimers[(int)WeaponType.BAUBLE_GRENADE] < shotDelay) {
							shotDelayTimers[(int)WeaponType.BAUBLE_GRENADE] = shotDelay;
						}
						shotDelayTimers[(int)WeaponType.PRESENT_BOMB] = shotDelay;
						DI_Events.EventCenter<AmmoTypes, PlayerState>.invoke("OnFire", playerState.getSelectedAmmoType(), playerState);
					}
					//Santa Bomber Code
					else if(ammoTypeId == playerState.getAmmoTypeId(AmmoTypes.AMMO_SANTA_BOMBER)) {

						GameObject projectile = (GameObject) Instantiate(selectedWeapon);
						projectile.transform.position.Set(0, 10, 0);
						RemovePlayerAmmo(ammoCount);

						// XP System bonus damage.
						// Exploding Ammo base x 25
						int xpId = playerState.getXpId(AmmoTypes.AMMO_SANTA_BOMBER);
						SantaBomber bomber = projectile.GetComponent<SantaBomber>();
						bomber.damage = bomber.originalDamage + (playerState.getLevel(xpId) * 25);

						DI_Events.EventCenter<AmmoTypes, PlayerState>.invoke("OnFire", playerState.getSelectedAmmoType(), playerState);
						shotDelayTimers[(int)playerState.getSelectedWeapon()] = shotDelay;
					}

					else if (ammoTypeId == playerState.getAmmoTypeId(AmmoTypes.AMMO_STEAM)) {
						RemovePlayerAmmo(ammoCount);
						DI_Events.EventCenter<AmmoTypes, PlayerState>.invoke("OnFire", playerState.getSelectedAmmoType(), playerState);
						shotDelayTimers[(int)playerState.getSelectedWeapon()] = shotDelay;
					}

					//everything else that follows the same code e.g snowball, icicle etc
					else {
						//GameObject firedProjectile = (GameObject) Instantiate(selectedWeapon);
						GameObject firedProjectile = PoolController.instance.getPooledObject(selectedWeapon.name);
						firedProjectile.SetActive(true);
						firedProjectile.transform.position = instPoint;
						// Take the cameras X and the players Y as the mouse look script changes those.
						firedProjectile.transform.rotation = Quaternion.Euler(playerCamera.transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);

						// Get the projectiles controller
						ProjectileController projCont = firedProjectile.GetComponent<ProjectileController>();
						float speed = projCont.projectileSpeed;

						// XP System bonus damage.
						// Regular (Non Exploding Ammo) base x level x level/2
						int xpId = playerState.getXpId(projCont.ammoType);
						projCont.pointReduction = projCont.originalPointReduction * playerState.getLevel(xpId) * (playerState.getLevel(xpId) / 2) + projCont.pointReduction;

						// Apply force to move the projectile
						firedProjectile.GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * speed);
						RemovePlayerAmmo(ammoCount);
						DI_Events.EventCenter<AmmoTypes, PlayerState>.invoke("OnFire", playerState.getSelectedAmmoType(), playerState);

						// Snowballs, Candy Canes, and Icicles share a common cooldown.
						if (shotDelayTimers[(int)WeaponType.SNOWBALL] < shotDelay) {
							shotDelayTimers[(int)WeaponType.SNOWBALL] = shotDelay;
						}
						if (shotDelayTimers[(int)WeaponType.ICICLE] < shotDelay) {
							shotDelayTimers[(int)WeaponType.ICICLE] = shotDelay;
						}
						if (shotDelayTimers[(int)WeaponType.CANDY_CANE] < shotDelay) {
							shotDelayTimers[(int)WeaponType.CANDY_CANE] = shotDelay;
						}
					}

					// Update the player
					combatTimer = combatTime;
					inCombat = true;
					DI_Events.EventCenter.invoke("OnEnterCombat");
				}
				else {
					steamThrowerWeapon.isActiveWeapon = false;
				}
			}
			else {
				steamThrowerWeapon.isActiveWeapon = false;
			}
		}
	}

	public void RemovePlayerAmmo(float ammocount){
		Ammo newammo;
		newammo.type = playerState.getSelectedAmmoType();
		newammo.amount = ammocount;
		playerState.removeAmmo(newammo);
	}
}
