﻿// Devils Inc Studios
	// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
	//
	// TODO: Include a description of the file here.
	//

using UnityEngine;
using DI_Events;
using System.Collections.Generic;

[AddComponentMenu("Character/Siege Mode")]
public class SiegeModeController : MonoBehaviour
{
	public GameStateController gsc;
	public PlayerState playerState;
	public FPSController fpsController;

	public bool isSieged = false;
	
	public void OnEnable()
	{
		gsc = GameObject.Find("Game State").GetComponent<GameStateController>();
		playerState = this.gameObject.GetComponent<PlayerState>();
		fpsController = this.gameObject.GetComponent<FPSController>();
	}

	public void Update()
	{
		if(Input.GetButtonDown("Mode")){
			#if DEBUG
				Debug.Log ("Mode pressed");
			#endif
			OnModeChange();
		}
	}

	public void OnModeChange()
	{
		if(gsc.gameState == GameStates.PLAYING){
			if(!isSieged){
				fpsController.SiegeModeEnable();
			}else{
				fpsController.SiegeModeDisable();
			}
			isSieged = !isSieged;
		}
	}
	
	public void OnFire()
	{
		DI_Events.EventCenter<PlayerState>.invoke("OnFire", playerState);
	}
}