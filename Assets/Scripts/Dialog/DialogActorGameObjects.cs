// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System;

[Serializable]
public struct DialogActorGameObjects
{
	public DialogActor actor;
	public GameObject actorAvatar;
}