// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014
//
// TODO: Include a description of the file here.
//

public enum DialogTriggerTypes
{
	TRIGGER_ON_START,
	TRIGGER_ON_TRIGGER,
	TRIGGER_ON_EVENT,
	TRIGGER_ON_TIMER,
}