// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
// TODO: Include a description of the file here.
//

using UnityEditor;
using UnityEngine;

public class BuildInfoEditor : EditorWindow
{
	public string displayName = PlayerPrefs.GetString("Build Display Name", "");
	public string buildName = PlayerPrefs.GetString("Build Name", "");
	public float majorRevNumber = PlayerPrefs.GetFloat("Build Major Rev", 0f);
	public float minorRevNumber = PlayerPrefs.GetFloat("Build Minor Rev", 0f);
	public float buildNumber = PlayerPrefs.GetFloat("Build Number", 0f);
	[MenuItem ("Devils Inc Studios/Build Tools/Build Info Manager")]
	public static void showWindow()
	{
		EditorWindow buildInfoEditor = EditorWindow.GetWindow(typeof(BuildInfoEditor));
		buildInfoEditor.maxSize = new Vector2(600, 200);
		buildInfoEditor.minSize = new Vector2(600, 200);
	}

	public void OnGUI()
	{
		EditorGUILayout.BeginVertical();
		displayName = EditorGUILayout.TextField("Build Display Name", displayName);
		buildName = EditorGUILayout.TextField("Build File Name", buildName);
		majorRevNumber = EditorGUILayout.FloatField("Major Rev Number", majorRevNumber);
		minorRevNumber = EditorGUILayout.FloatField("Minor Rev Number", minorRevNumber);
		buildNumber = EditorGUILayout.FloatField("Build Number", buildNumber);
		if (GUILayout.Button("Save")) {
			PlayerPrefs.SetString("Build Display Name", displayName);
			PlayerPrefs.SetString("Build Name", buildName);
			PlayerPrefs.SetFloat("Build Major Rev", majorRevNumber);
			PlayerPrefs.SetFloat("Build Minor Rev", minorRevNumber);
			PlayerPrefs.SetFloat("Build Number", buildNumber);
			BuildInfo.refreshInfo();
		}
		EditorGUILayout.EndVertical();
	}
}