// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
// TODO: Include a description of the file here.
//

using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Diagnostics;
using System;

public class BuildManager : EditorWindow
{
	private string zipTool = PlayerPrefs.GetString("Zip Tool Location", "D:\\Program Files (x86)\\7-Zip\\7z.exe");
	private string zipArgs = PlayerPrefs.GetString("Zip Args", "a -mx9 -tzip");
	private string buildsFolder = PlayerPrefs.GetString("Builds Folder", "Builds");
	private string copyToFolder = PlayerPrefs.GetString("Builds Copy To Folder", "");
	private float progressbarProgress;
	private string progressbarMessage;
	private string progressbarTitle;
	private bool showBuildSettings = false;
	private bool showZipToolEditor = false;
	private bool showArgsEditor = false;
	private bool showBuildFolderEditor = false;
	private bool showCopyToFolderEditor = false;
	private string tempArgs;
	private string tempZipTool;
	private string tempBuildFolder;
	private string tempCopyToFolder;
	private bool isBuildingGame = false;
	private string buildLog;
	private Vector2 logScroll = Vector2.zero;
	private int startTime;


	public void OnGUI()
	{
		EditorGUILayout.SelectableLabel(BuildInfo.buildDisplayName);
		EditorGUILayout.Separator();

		showBuildSettings = EditorGUILayout.Foldout(showBuildSettings, "Build Settings");
		if (showBuildSettings) {
			EditorGUILayout.BeginVertical();
			EditorGUILayout.SelectableLabel("Zip Tool: " + zipTool, GUILayout.MaxWidth(1000));
			if (GUILayout.Button("Modify")) {
				tempZipTool = EditorUtility.OpenFilePanel("Zip Tool Location", "", "");
				showZipToolEditor = true;
			}
			if (showZipToolEditor) {
				EditorGUILayout.SelectableLabel("Selected Zip Tool: " + tempZipTool, GUILayout.MaxWidth(1000));
				if (GUILayout.Button("Save")) {
					showZipToolEditor = false;
					zipTool = tempZipTool;
					PlayerPrefs.SetString("Zip Tool Location", zipTool);
				}
				if (GUILayout.Button("Discard")) {
					showZipToolEditor = false;
				}
			}

			EditorGUILayout.SelectableLabel("Zip Arguments: " + zipArgs, GUILayout.MaxWidth(1000));
			if (!showArgsEditor) {
				if (GUILayout.Button("Modify")) {
					showArgsEditor = true;
					tempArgs = zipArgs;
				}
			}
			if (showArgsEditor) {
				tempArgs = EditorGUILayout.TextField("Zip Arguments: ", tempArgs, GUILayout.MaxWidth(1000));
				if (GUILayout.Button("Save")) {
					showArgsEditor = false;
					zipArgs = tempArgs;
					PlayerPrefs.SetString("Zip Args", zipArgs);
				}
				if (GUILayout.Button("Discard")) {
					showArgsEditor = false;
				}
			}

			EditorGUILayout.SelectableLabel("Builds Folder: " + buildsFolder, GUILayout.MaxWidth(1000));
			if (!showBuildFolderEditor) {
				if (GUILayout.Button("Modify")) {
					showBuildFolderEditor = true;
					tempBuildFolder = EditorUtility.OpenFolderPanel("Build Folder Location", "", "");
				}
			}
			if (showBuildFolderEditor) {
				EditorGUILayout.SelectableLabel("Selected Build Folder Location: " + tempBuildFolder, GUILayout.MaxWidth(1000));
				if (GUILayout.Button("Save")) {
					showBuildFolderEditor = false;
					buildsFolder = tempBuildFolder;
					PlayerPrefs.SetString("Builds Folder", buildsFolder);
				}
				if (GUILayout.Button("Discard")) {
					showBuildFolderEditor = false;
				}
			}

			EditorGUILayout.SelectableLabel("Copy Builds To Folder: " + copyToFolder, GUILayout.MaxWidth(1000));
			if (!showCopyToFolderEditor) {
				if (GUILayout.Button("Modify")) {
					showCopyToFolderEditor = true;
					tempCopyToFolder = EditorUtility.OpenFolderPanel("Copy Builds To Folder", "", "");
				}
			}
			if (showCopyToFolderEditor) {
				EditorGUILayout.SelectableLabel("Selected Build Folder Copy To Location: " + tempCopyToFolder, GUILayout.MaxWidth(1000));
				if (GUILayout.Button("Save")) {
					showCopyToFolderEditor = false;
					copyToFolder = tempCopyToFolder;
					PlayerPrefs.SetString("Builds Copy To Folder", copyToFolder);
				}
				if (GUILayout.Button("Discard")) {
					showCopyToFolderEditor = false;
				}
			}

			EditorGUILayout.EndVertical();
		}

		EditorGUILayout.Separator();

		if (GUILayout.Button("Build Game")) {
			isBuildingGame = true;
			BuildGame();
		}

		if (isBuildingGame) {
			EditorUtility.DisplayProgressBar(progressbarTitle, progressbarMessage, progressbarProgress);
		}

		EditorGUILayout.LabelField("Build Log:");
		logScroll = EditorGUILayout.BeginScrollView(logScroll, false, true);
		EditorGUILayout.TextArea(buildLog, GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
		EditorGUILayout.EndScrollView();
	}

	[MenuItem ("Devils Inc Studios/Build Tools/Build Manager")]
	public static void showWindow()
	{
		EditorWindow buildManager = EditorWindow.GetWindow(typeof(BuildManager));
		buildManager.maxSize = new Vector2(1000, 400);
		buildManager.minSize = new Vector2(1000, 400);
	}

	public string[] getScenes()
	{
		List<string> scenes = new List<string>();
		foreach (EditorBuildSettingsScene scene in EditorBuildSettings.scenes) {
			if (scene.enabled) {
				scenes.Add(scene.path);
			}
		}
		return scenes.ToArray();
	}

	public void updateProgressBar(float updatedProgress, string updatedMessage)
	{
		progressbarProgress = updatedProgress;
		progressbarMessage = updatedMessage;
		EditorUtility.DisplayProgressBar(progressbarTitle, progressbarMessage, progressbarProgress);
	}

	public void logMessage(string logMessage)
	{
		buildLog += System.Environment.NewLine + logMessage;
	}

	public void BuildGame ()
	{
		if (!BuildPipeline.isBuildingPlayer) {
			progressbarTitle = "Building Game";
			buildLog = "Starting Build";
			startTime = (int)(System.DateTime.UtcNow.Subtract(new System.DateTime(1970, 1, 1))).TotalSeconds;

			// Build X86 Version
			updateProgressBar(0f, "Building x86 Build");
			logMessage("Started x86 Build at: " + startTime);
			BuildPipeline.BuildPlayer(getScenes(), buildsFolder + "/" + BuildInfo.buildName + ".exe", BuildTarget.StandaloneWindows, BuildOptions.None);
			int buildTime = (int)(System.DateTime.UtcNow.Subtract(new System.DateTime(1970, 1, 1))).TotalSeconds;
			logMessage("Finished x86 Build at: " + buildTime);
			logMessage("Built x86 in: " + (buildTime - startTime) + " seconds");
			logMessage("Build Saved as: " + buildsFolder + "/" + BuildInfo.buildName + ".exe");
			updateProgressBar(25f, "Compressing x86 Build");
			Process zipProcess = new System.Diagnostics.Process();
			zipProcess.StartInfo.Arguments = zipArgs
				+ " \"" + copyToFolder + "\\" + BuildInfo.buildName + ".zip\""
					+ " \"" + buildsFolder + "\\" + BuildInfo.buildName + ".exe\""
					+ " \"" + buildsFolder + "\\" + BuildInfo.buildName + "_Data\"";
			zipProcess.StartInfo.CreateNoWindow = true;
			zipProcess.StartInfo.FileName = "\"" + zipTool + "\"";
			zipProcess.StartInfo.RedirectStandardOutput = true;
			zipProcess.StartInfo.RedirectStandardError = true;
			zipProcess.StartInfo.UseShellExecute = false;
			logMessage("Compressing using zip args: " + zipProcess.StartInfo.Arguments);
			zipProcess.Start();
			zipProcess.WaitForExit();
			logMessage(zipProcess.StandardOutput.ReadToEnd());
			
			int zipStartTime = (int) zipProcess.StartTime.Subtract(new System.DateTime(1970, 1, 1)).TotalSeconds;
			int zipExitTime = (int) zipProcess.ExitTime.Subtract(new System.DateTime(1970, 1, 1)).TotalSeconds;
			logMessage("Finished Compression of x86 Build in: " + (zipExitTime - zipStartTime) + " seconds");

			// Build X64 Version
			updateProgressBar(50f, "Building x64 Build");
			logMessage("Started x64 Build at: " + (int)(System.DateTime.UtcNow.Subtract(new System.DateTime(1970, 1, 1))).TotalSeconds);
			BuildPipeline.BuildPlayer(getScenes(), buildsFolder + "/" + BuildInfo.buildName + "_x64.exe", BuildTarget.StandaloneWindows64, BuildOptions.None);
			buildTime = (int)(System.DateTime.UtcNow.Subtract(new System.DateTime(1970, 1, 1))).TotalSeconds;
			logMessage("Finished x64 Build at: " + buildTime);
			logMessage("Built x64 in: " + (buildTime - startTime) + " seconds");
			logMessage("Build Saved as: " + buildsFolder + "/" + BuildInfo.buildName + "_x64.exe");

			updateProgressBar(75f, "Compressing x64 Build");
			Process zipProcess64 = new System.Diagnostics.Process();
			zipProcess64.StartInfo.Arguments = zipArgs
				+ " \"" + copyToFolder + "\\" + BuildInfo.buildName + "_x64.zip\""
					+ " \"" + buildsFolder + "\\" + BuildInfo.buildName + "_x64.exe\""
					+ " \"" + buildsFolder + "\\" + BuildInfo.buildName + "_x64_Data\"";
			zipProcess64.StartInfo.CreateNoWindow = true;
			zipProcess64.StartInfo.FileName = "\"" + zipTool + "\"";
			zipProcess64.StartInfo.RedirectStandardOutput = true;
			zipProcess64.StartInfo.RedirectStandardError = true;
			zipProcess64.StartInfo.UseShellExecute = false;
			logMessage("Compressing using zip args: " + zipProcess64.StartInfo.Arguments);
			zipProcess64.Start();
			zipProcess64.WaitForExit();
			logMessage(zipProcess64.StandardOutput.ReadToEnd());

			int zip64StartTime = (int) zipProcess64.StartTime.Subtract(new System.DateTime(1970, 1, 1)).TotalSeconds;
			int zip64ExitTime = (int) zipProcess64.ExitTime.Subtract(new System.DateTime(1970, 1, 1)).TotalSeconds;
			logMessage("Finished Compression of x64 Build in: " + (zip64ExitTime - zip64StartTime) + " seconds");

			PlayerPrefs.SetFloat("Build Number", BuildInfo.buildNumber + 1);
			BuildInfo.refreshInfo();
			logMessage("Finished building in: " + ((int)(System.DateTime.UtcNow.Subtract(new System.DateTime(1970, 1, 1))).TotalSeconds - startTime) + " seconds");
			EditorUtility.ClearProgressBar();
			isBuildingGame = false;
		}
	}
}