// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using UnityEditorInternal;

[CustomEditor(typeof(StarAIWaypoint))]
public class StarAIWaypointEditor : Editor
{
	private StarAIWaypoint waypoint;
	private ReorderableList _meleeWaypoints;
	private ReorderableList _rangedWaypoints;
	private bool displayMeleeWaypoints = false;
	private bool displayRangedWaypoints = false;

	public void OnEnable()
	{
		waypoint = (StarAIWaypoint)target;
		_meleeWaypoints = new ReorderableList(serializedObject, serializedObject.FindProperty("meleeWaypoints"), true, true, true, true);
		_meleeWaypoints.drawHeaderCallback = (Rect rect) => {  
			EditorGUI.LabelField(rect, "Melee Waypoints");
		};
		_meleeWaypoints.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) => {
			var element = _meleeWaypoints.serializedProperty.GetArrayElementAtIndex(index);
			rect.y += 2;
			EditorGUI.PropertyField(new Rect(rect.x, rect.y, 250, EditorGUIUtility.singleLineHeight), element, GUIContent.none);
		};

		_rangedWaypoints = new ReorderableList(serializedObject, serializedObject.FindProperty("rangedWaypoints"), true, true, true, true);
		_rangedWaypoints.drawHeaderCallback = (Rect rect) => {  
			EditorGUI.LabelField(rect, "Ranged Waypoints");
		};
		_rangedWaypoints.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) => {
			var element = _rangedWaypoints.serializedProperty.GetArrayElementAtIndex(index);
			rect.y += 2;
			EditorGUI.PropertyField(new Rect(rect.x, rect.y, 250, EditorGUIUtility.singleLineHeight), element, GUIContent.none);
		};
	}

	public override void OnInspectorGUI() {
		serializedObject.Update();

		EditorGUIUtility.LookLikeControls();
		EditorGUILayout.BeginVertical();
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("Melee Path Color:");
		waypoint.meleeColor = EditorGUILayout.ColorField(waypoint.meleeColor);
		waypoint.displayMeleeGizmo = EditorGUILayout.Toggle(waypoint.displayMeleeGizmo);
		EditorGUILayout.EndHorizontal();
		EditorGUILayout.EndVertical();

		EditorGUILayout.BeginVertical();
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("Ranged Path Color:");
		waypoint.rangedColor = EditorGUILayout.ColorField(waypoint.rangedColor);
		waypoint.displayRangedGizmo = EditorGUILayout.Toggle(waypoint.displayRangedGizmo);
		EditorGUILayout.EndHorizontal();
		EditorGUILayout.EndVertical();

		EditorGUILayout.BeginVertical();
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("Boss Path Color:");
		waypoint.bossColor = EditorGUILayout.ColorField(waypoint.bossColor);
		waypoint.displayBossGizmo = EditorGUILayout.Toggle(waypoint.displayBossGizmo);
		EditorGUILayout.EndHorizontal();
		EditorGUILayout.EndVertical();

		EditorGUILayout.BeginVertical();
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("Boss Path Waypoint:");
		waypoint.bossWaypoint = (AIWaypoint)EditorGUILayout.ObjectField(waypoint.bossWaypoint, typeof(AIWaypoint), true);
		EditorGUILayout.EndHorizontal();
		EditorGUILayout.EndVertical();

		EditorGUILayout.BeginVertical();
		if (GUILayout.Button("Set Nodes")) {
			waypoint.meleeWaypoints = new List<AIWaypoint>();
			waypoint.rangedWaypoints = new List<AIWaypoint>();
			GameObject[] waypoints = GameObject.FindGameObjectsWithTag("Star AI Waypoint Melee");
			foreach (GameObject node in waypoints) {
				waypoint.meleeWaypoints.Add(node.GetComponent<AIWaypoint>());
			}

			waypoints = GameObject.FindGameObjectsWithTag("Star AI Waypoint Ranger");
			foreach (GameObject node in waypoints) {
				waypoint.rangedWaypoints.Add(node.GetComponent<AIWaypoint>());
			}
		}

		EditorGUILayout.EndVertical();
		displayMeleeWaypoints = EditorGUILayout.Foldout(displayMeleeWaypoints, "Melee Waypoints");
		if (displayMeleeWaypoints) {
			_meleeWaypoints.DoLayoutList();
		}

		displayRangedWaypoints = EditorGUILayout.Foldout(displayRangedWaypoints, "Ranged Waypoints");
		if (displayRangedWaypoints) {
			_rangedWaypoints.DoLayoutList();
		}
		serializedObject.ApplyModifiedProperties();
	}
}
