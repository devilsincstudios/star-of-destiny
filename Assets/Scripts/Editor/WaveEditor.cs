//// Devils Inc Studios
//// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
////
//// TODO: Include a description of the file here.
//
//
//#region Includes
//using UnityEditor;
//using UnityEngine;
//using System.Collections.Generic;
//using UnityEditorInternal;
//#endregion
//
//[CustomEditor(typeof(WaveController))]
//public class WaveEditor : Editor
//{
//	#region Private Variables
//	private WaveController waveController;
//	#endregion
//
//	#region Public Methods
//	public void OnEnable()
//	{
//		waveController = (WaveController)target;
//	}
//	
//	public override void OnInspectorGUI() {
//		EditorGUIUtility.LookLikeControls();
//		int waveIndex = 0;
//		foreach (Wave wave in waveController.waves.ToArray()) {
//			EditorGUILayout.BeginVertical();
//				EditorGUILayout.BeginVertical();
//				EditorGUILayout.LabelField("Wave: " + waveIndex);
//				EditorGUILayout.EndVertical();
//				foreach (SpawnPoints point in waveController.waves[waveIndex].spawnPoints) {
//					EditorGUILayout.BeginVertical();
//					EditorGUILayout.EnumPopup();
//					EditorGUILayout.EndVertical();
//			EditorGUILayout.EndVertical();
//		}
//	}
//	#endregion
//}