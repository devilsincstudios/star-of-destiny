// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[AddComponentMenu("Entities/BossSnowman")]
public class BossSnowman : RangedEnemy
{
	[Header("Projectile Settings")]
	public GameObject towerProjectile;
	public GameObject playerTrackerPrefab;
	[Header("Target Settings")]
	public GameObject target;
	public Tower[] possibleTowerTargets;
	public List<PlayerEntity> possiblePlayerTargets;
	public PlayerEntity[] possiblePlayerOutOfRange;
	[Header("Agent Settings")]
	public NavMeshAgent agent;
	[Header("Phase Settings")]
	public WaveController waveController;
	public bool isPlayerInRange = false;
	public int phaseOneSpawnAmount = 3;
	public int phaseTwoSpawnAmount = 6;
	public int phaseThreeSpawnAmount = 8;
	public float immortalTime = 10;
	public bool phaseOneEntered = false;
	public bool phaseTwoEntered = false;
	public bool phaseThreeEntered = false;
	[Header("Body Parts Settings")]
	public GameObject headTarget;
	public GameObject bodyTarget;
	public GameObject legTarget;
	
	[HideInInspector]
	public GameObject prefabNormalMan;

	private float immortalTimer = 0;

	new public void OnEnable()
	{
		prefabNormalMan = PrefabLoader.instance.getPrefab("SNOWMAN_NORMALMAN");
		waveController = FindObjectOfType<WaveController>();
		base.OnEnable();
	}
	public void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player")
		{
			PlayerEntity targetInfo = other.GetComponent<PlayerEntity>();
			if (!possiblePlayerTargets.Contains(targetInfo))
			{
				possiblePlayerTargets.Add(targetInfo);
			}
		}
	}
	
	public void OnTriggerStay(Collider other)
	{
		if (other.tag == "Player")
		{
			PlayerEntity targetInfo = other.GetComponent<PlayerEntity>();
			if (!possiblePlayerTargets.Contains(targetInfo))
			{
				possiblePlayerTargets.Add(targetInfo);
			}
		}
	}
	
	public void OnTriggerExit(Collider other)
	{
		if (other.tag == "Player")
		{
			PlayerEntity targetInfo = other.GetComponent<PlayerEntity>();
			possiblePlayerTargets.Remove(targetInfo);
			if (target == targetInfo)
			{
				target = null;
			}
		}
	}
	
	public void LateUpdate()
	{
		if (gsc.gameState == GameStates.PLAYING)
		{
			if (health <= 300 && phaseThreeEntered == false)
			{
				phaseThreeEntered = true;
				ActivatephaseThree();
			}
			else if (health <= 600 && phaseTwoEntered == false)
			{
				phaseTwoEntered = true;
				ActivatePhaseTwo();
			}
			else if (health <= 900 && phaseOneEntered == false)
			{
				phaseOneEntered = true;
				ActivatePhaseOne();
			}
		}

		if (immortalTimer <= 0) {
			isImmortal = false;
			canMove = true;
		} else {
			immortalTimer -= Time.deltaTime;
		}
	}
	
	public void spawnProjectile(GameObject target)
	{
		if (target != null)
		{
			//TODO: Clean this up its a mess.......
			GameObject projectile;
			ProjectileController projectileController;
			if (target.tag == "Tower")
			{
				projectile = (GameObject)Instantiate(towerProjectile);
				projectileController = projectile.GetComponent<ProjectileController>();
				projectile.transform.position = new Vector3(target.transform.position.x, target.transform.position.y + 18, target.transform.position.z);
				projectile.transform.LookAt(new Vector3(target.transform.position.x, target.transform.position.y + 2.5f, target.transform.position.z));
			}
			else if (target.tag == "Player" && isPlayerInRange)
			{
				projectile = (GameObject)Instantiate(projectilePrefab);
				projectileController = projectile.GetComponent<ProjectileController>();
				projectile.transform.position = projectileLaunchPoint.transform.position;
				projectile.transform.LookAt(new Vector3(target.transform.position.x, target.transform.position.y + 2.5f, target.transform.position.z));
			}
			else if (target.tag == "Player" && !isPlayerInRange)
			{
				projectile = (GameObject)Instantiate(playerTrackerPrefab);
				projectileController = projectile.GetComponent<ProjectileController>();
				BossTrackerSnowball trackerSnowball = projectile.GetComponent<BossTrackerSnowball>();
				trackerSnowball.enemyTransform = target.transform;
				trackerSnowball.moveSpeed = projectileController.projectileSpeed;
				projectile.transform.position = projectileLaunchPoint.transform.position;
				projectile.transform.LookAt(new Vector3(target.transform.position.x, target.transform.position.y + 2.5f, target.transform.position.z));
			}
			else
			{
				projectile = (GameObject)Instantiate(projectilePrefab);
				projectileController = projectile.GetComponent<ProjectileController>();
				projectile.transform.position = projectileLaunchPoint.transform.position;
				projectile.transform.LookAt(new Vector3(target.transform.position.x, target.transform.position.y + 1.0f, target.transform.position.z));
			}
			projectileController.owner = (Entity)this;
			projectileController.pointReduction = damage;
			projectile.GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * projectileController.projectileSpeed);
		}
	}
	
	void ActivatePhaseOne()
	{
		isImmortal = true;
		immortalTimer = immortalTime;
		canMove = false;

		
		//shoot
		InitialiseAttack();
		//summon
		for (int i = 0; i < phaseOneSpawnAmount; i++)
		{
			SpawnEnemy();
		}
	}
	
	void ActivatePhaseTwo()
	{
		isImmortal = true;
		immortalTimer = immortalTime;
		canMove = false;

		
		//shoot
		InitialiseAttack();
		//summon
		for (int i = 0; i < phaseTwoSpawnAmount; i++)
		{
			SpawnEnemy();
		}
	}
	
	void ActivatephaseThree()
	{
		isImmortal = true;
		immortalTimer = immortalTime;
		canMove = false;

		
		//shoot
		InitialiseAttack();
		
		//summon
		for (int i = 0; i < phaseThreeSpawnAmount; i++)
		{
			SpawnEnemy();
		}
	}
	
	void InitialiseAttack()
	{
		possibleTowerTargets = GameObject.FindObjectsOfType<Tower>();
		if (possibleTowerTargets.Length != 0)
		{
			for (int i = 0; i < possibleTowerTargets.Length; i++)
			{
				target = possibleTowerTargets[i].gameObject;
				transform.LookAt(new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z));
				transform.localRotation.Set(0, transform.localRotation.y, transform.localRotation.z, transform.localRotation.w);
				spawnProjectile(target);
			}
		}
		
		if (possiblePlayerTargets.Count != 0)
		{
			for (int i = 0; i < possiblePlayerTargets.Count; i++)
			{
				isPlayerInRange = true;
				target = possiblePlayerTargets[i].gameObject;
				transform.LookAt(new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z));
				transform.localRotation.Set(0, transform.localRotation.y, transform.localRotation.z, transform.localRotation.w);
				spawnProjectile(target);
			}
		}
		else
		{
			isPlayerInRange = false;
			possiblePlayerOutOfRange = GameObject.FindObjectsOfType<PlayerEntity>();
			for (int i = 0; i < possiblePlayerOutOfRange.Length; i++)
			{
				target = possiblePlayerOutOfRange[i].gameObject;
				transform.LookAt(new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z));
				transform.localRotation.Set(0, transform.localRotation.y, transform.localRotation.z, transform.localRotation.w);
				spawnProjectile(target);
				
			}
			possiblePlayerOutOfRange = null;
		}
	}
	
	void SpawnEnemy()
	{
		GameObject enemy = (GameObject)GameObject.Instantiate(prefabNormalMan);
		Enemy enemyData = enemy.GetComponent<Enemy>();
		enemyData.setHealth(enemyData.maxHealth);
		//TODO: change spawnpoint and enemy to random!
		DI_Events.EventCenter<SpawnPoints, GameObject>.invoke("OnRequestEnemySpawn", SpawnPoints.RED, enemy);
		waveController.enemiesInWave++;
		waveController.enemiesSpawned++;
	}
}