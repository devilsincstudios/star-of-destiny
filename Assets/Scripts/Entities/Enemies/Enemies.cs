// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

public enum Enemies
{
	SNOWMAN_FLYINGMAN,
	SNOWMAN_NORMALMAN,
	SNOWMAN_RAMBOMAN,
	SNOWMAN_RANGEMAN,
	SNOWMAN_SHIELDMAN,
	SNOWMAN_SPEEDMAN,
	SNOWMAN_TANKMAN,
	SNOWMAN_BOSSMAN
}