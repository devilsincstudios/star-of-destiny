// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;
//using UnityEditor;

[AddComponentMenu("Entities/Enemy")]
public class Enemy : Entity
{
	[Header("Enemy Settings")]
	public Enemies type;
	public bool ranged = false;
	[HideInInspector]
	public bool isAttacking = false;
	[HideInInspector]
	public bool shielded = false;
	[HideInInspector]
	public float shieldTimer = 0.0f;

	[Header("Damage Settings")]
	public bool canAttack = true;
	public float damage = 10.0f;
	public float attackSpeed = 1.0f;

	[Header("Loot Settings")]
	public float points = 0.0f;
	public float coinsMin = 10.0f;
	public float coinsMax = 25.0f;
	public float coinsPercent = 25.0f;
	public float ammoPercent = 25.0f;
	public AmmoDrops ammoDrops;

	[Header("GUI Settings")]
	public GameObject statusBar;
	public GameObject burningIcon;
	public GameObject chilledIcon;
	public GameObject frozenIcon;
	public GameObject poisonedIcon;

	[HideInInspector]
	public WaypointAI waypointAgent;
	[HideInInspector]
	public bool isBurningImmuneOriginal;
	[HideInInspector]
	public bool isChilledImmuneOriginal;
	[HideInInspector]
	public bool isFrozenImmuneOriginal;
	[HideInInspector]
	public bool isPoisonImmuneOriginal;
	[HideInInspector]
	public GameStateController gsc;
	[HideInInspector]
	public PrefabLoader prefabLoader;

	#region Public Methods
	new public void OnEnable()
	{
		gsc = GameStateController.instance;
		if (canMove) {
			waypointAgent = this.GetComponent<WaypointAI>();
		}
		// If this isn't the first spawn we need to clean up some stuff.
		if (isDead) {
			isDead = false;
			isChilled = false;
			isPoisoned = false;
			isFrozen = false;
			isBurning = false;
			burnTimeRemaining = 0.0f;
			burnDamage = 0.0f;
			chilledTimeRemaining = 0.0f;
			chilledSpeedReduction = 0.0f;
			poisonDamage = 0.0f;
			poisonedTimeRemaining = 0.0f;
			frozenTimeRemaining = 0.0f;
			isBurningImmune = isBurningImmuneOriginal;
			isFrozenImmune = isFrozenImmuneOriginal;
			isChilledImmune = isChilledImmuneOriginal;
			isPoisonImmune = isPoisonImmuneOriginal;
			shieldTimer = 0.0f;
			shielded = false;
			this.GetComponent<CharacterController>().enabled = true;
			setEffectColor(originalColor);
		}
		else {
			originalMovementSpeed = movementSpeed;
			originalMaxHealth = maxHealth;
			isBurningImmuneOriginal = isBurningImmune;
			isChilledImmuneOriginal = isChilledImmune;
			isFrozenImmuneOriginal = isFrozenImmune;
			isPoisonImmuneOriginal = isPoisonImmune;
			prefabLoader = gsc.gameObject.GetComponent<PrefabLoader>();
			originalColor = new List<Color>();
			renderers = new List<Renderer>();
			foreach (Renderer childRenderer in this.gameObject.transform.GetComponentsInChildren<Renderer>()) {
				if (childRenderer.GetComponent<Renderer>().material != null) {
					if (childRenderer.GetComponent<Renderer>().material.HasProperty("_Color")) {
						originalColor.Add(childRenderer.GetComponent<Renderer>().material.color);
						renderers.Add(childRenderer);
					}
				}
			}
			if (hasAnimations) {
				entityAnimator = GetComponentInChildren<Animator>();
				if (entityAnimator == null) {
					hasAnimations = false;
				}
			}
		}


		// The base class will remove the events
		DI_Events.EventCenter<Entity, Entity, float, AmmoTypes>.addListener("OnHit", handleOnHit);
		DI_Events.EventCenter<Entity, Entity, float, float>.addListener("OnChill", base.handleOnChill);
		DI_Events.EventCenter<Entity, Entity, float, float>.addListener("OnBurn", base.handleOnBurn);
		DI_Events.EventCenter<Entity, Entity, float>.addListener("OnFreeze", base.handleOnFreeze);
		DI_Events.EventCenter<Entity, Entity, float, float>.addListener("OnPoison", base.handleOnPoison);

		//DI_Events.EventCenter<Entity, Entity, SlowEffect>.addListener("OnSlow", base.handleOnSlow);
		health = maxHealth;
		if (!isImmortal && healthBar != null) {
			healthBar.fillAmount = health / maxHealth;
		}
	}

	new public void OnDisable()
	{
		DI_Events.EventCenter<Entity, Entity, float, float>.removeListener("OnChill", base.handleOnChill);
		DI_Events.EventCenter<Entity, Entity, float, float>.removeListener("OnBurn", base.handleOnBurn);
		DI_Events.EventCenter<Entity, Entity, float>.removeListener("OnFreeze", base.handleOnFreeze);
		DI_Events.EventCenter<Entity, Entity, float, float>.removeListener("OnPoison", base.handleOnPoison);
		DI_Events.EventCenter<Entity, Entity, float, AmmoTypes>.removeListener("OnHit", handleOnHit);
	}

	new public void Update()
	{
		if (!isDead) {
			if (shieldTimer > 0.0f) {
				shieldTimer -= Time.deltaTime;
				isBurningImmune = true;
				isChilledImmune = true;
				isFrozenImmune = true;
				isPoisonImmune = true;
			}
			if (shielded && shieldTimer <= 0.0f) {
				shielded = false;
				isBurningImmune = isBurningImmuneOriginal;
				isChilledImmune = isChilledImmuneOriginal;
				isFrozenImmune = isFrozenImmuneOriginal;
				isPoisonImmune = isPoisonImmuneOriginal;
			}

			if (isBurning || isChilled || isFrozen || isPoisoned) {
				if (shielded) {
					burnTimeRemaining = 0.0f;
					chilledTimeRemaining = 0.0f;
					frozenTimeRemaining = 0.0f;
					poisonedTimeRemaining = 0.0f;
				}

				statusBar.SetActive(true);
				if (isBurning) {
					burningIcon.SetActive(true);
				}
				else {
					burningIcon.SetActive(false);
				}

				if (isFrozen) {
					frozenIcon.SetActive(true);
				}
				else {
					frozenIcon.SetActive(false);
				}

				if (isChilled) {
					chilledIcon.SetActive(true);
				}
				else {
					chilledIcon.SetActive(false);
				}

				if (isPoisoned) {
					poisonedIcon.SetActive(true);
				}
				else {
					poisonedIcon.SetActive(false);
				}
			}
			else {
				statusBar.SetActive(false);
			}

			if (health <= 0.0f && !isDead) {
				OnDeath(gsc.playerState.entity);
			}
		}
		CancelInvoke();
		base.Update();
	}

	new public void OnDeath(Entity attacker)
	{
		DI_Events.EventCenter<PlayerState>.invoke("OnUpdateHudRequest", gsc.playerState);
		statusBar.SetActive(false);
		int coinsRoll = UnityEngine.Random.Range(0, 100);
		#if DEBUG
			Debug.Log("Coins Roll: " + coinsRoll);
		#endif
		if (coinsPercent >= coinsRoll) {
			#if DEBUG
				Debug.Log("Coins Roll: SUCCESS");
			#endif
			float coinsDropped = UnityEngine.Random.Range(coinsMin, coinsMax);
			GameObject coin = PoolController.instance.getPooledObject("Coin");
			coin.GetComponent<Coin>().amount = Mathf.CeilToInt(coinsDropped);
			coin.SetActive(true);
			coin.transform.position = transform.position;
			coin.transform.rotation = transform.rotation;
		}
		else {
			#if DEBUG
				Debug.Log("Coins Roll: FAIL");
			#endif
		}

		float ammoRoll = UnityEngine.Random.Range(0, 100);
		#if DEBUG
			Debug.Log("Ammo Roll: " + ammoRoll);
		#endif
		if (ammoPercent >= ammoRoll) {
			#if DEBUG
				Debug.Log("Ammo Roll: SUCCESS");
			#endif
			spawnRandomPowerup();
		}
		else {
			#if DEBUG
				Debug.Log("Ammo Roll: FAIL");
			#endif
		}

		if (this.GetComponent<CharacterController>() != null) {
			this.GetComponent<CharacterController>().enabled = false;
		}
		StopAllCoroutines();
		base.OnDeath(attacker);
	}

	public void spawnRandomPowerup()
	{
		int weightRoll = UnityEngine.Random.Range(0, 100);

		foreach (AmmoWeights data in ammoDrops.drops) {
			if (weightRoll > data.rangeMin && weightRoll <= data.rangeMax) {
				#if DEBUG
					Debug.Log("selectRandomPowerUp - Rolled a " + weightRoll + " this is translated to: " + data.type.ToString());
				#endif
				GameObject powerUp = prefabLoader.getPrefab(data.type.ToString());
				if (powerUp != null) {
					GameObject _spawnedPowerUp = PoolController.instance.getPooledObject(powerUp.name);
					_spawnedPowerUp.SetActive(true);
					_spawnedPowerUp.GetComponent<AmmoBox>().amount = UnityEngine.Random.Range(data.minAmount, data.maxAmount);
					_spawnedPowerUp.transform.position = transform.position;
					_spawnedPowerUp.transform.rotation = transform.rotation;
				}
			}
		}
	}

	/// <summary>
	/// Handles the on hit.
	/// </summary>
	/// <param name="target">Target.</param>
	/// <param name="attacker">Attacker.</param>
	/// <param name="damage">Damage.</param>
	new public void handleOnHit(Entity target, Entity attacker, float damage, AmmoTypes ammoType)
	{
		if (target == this) {
			if (!isDead) {
				if (!isImmortal) {
					//Shieldman's shield cuts off 90% damage for everyone except shieldmen.
					if (shielded && type != Enemies.SNOWMAN_SHIELDMAN) {
						// If its a single target ammo type allow it to deal damage.
						switch (ammoType) {
							case AmmoTypes.AMMO_CANDY_CANE:
							case AmmoTypes.AMMO_CHEAT:
							case AmmoTypes.AMMO_ICICLE:
							case AmmoTypes.AMMO_NONE:
							case AmmoTypes.AMMO_SANTA_BOMBER:
							case AmmoTypes.AMMO_SNOWBALL:
							case AmmoTypes.AMMO_STEAM:
							case AmmoTypes.AMMO_TOWER_BASIC:
							case AmmoTypes.AMMO_TOWER_LASER:
							case AmmoTypes.AMMO_TOWER_SPLIT_SHOT:
								damage *= 0.10f;
							break;

							// Sniper does full damage
							case AmmoTypes.AMMO_TOWER_SNIPER:
							break;

							// AOE effects do 0 damage
							default:
								damage = 0.0f;
							break;
						}
					}
					health -= damage;
					healthBar.fillAmount = health / maxHealth;

					if (health <= 0) {
						if (attacker.gameObject.tag == "Tower" || attacker.gameObject.tag == "Tower Ammo") {
							DI_Events.EventCenter<Entity, Entity>.invoke("OnTowerKill", target, attacker);
							this.OnDeath(attacker.gameObject.GetComponent<Tower>().owner);
						}
						else {
							this.OnDeath(attacker);
						}
					}
					if (attacker.gameObject.tag == "Tower" || attacker.gameObject.tag == "Tower Ammo") {
						DI_Events.EventCenter<float, Entity>.invoke("OnTowerDamage", damage, attacker);
					}
					else {
						DI_Events.EventCenter<float, Enemies, AmmoTypes>.invoke("OnDamage", damage, this.type, ammoType);
					}
				}
			}
		}
	}

	public void gainShield()
	{
		shielded = true;
		shieldTimer = 0.15f;
	}

	public IEnumerator Attack(Entity target) {
		isAttacking = true;
		DI_Events.EventCenter<Entity, Entity, float, AmmoTypes>.invoke("OnHit", target, this, damage, AmmoTypes.AMMO_NONE);
		yield return new WaitForSeconds(attackSpeed);
		isAttacking = false;
	}

	#endregion
}