// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[AddComponentMenu("Entities/Ramboman")]
public class RamboMan : RangedEnemy
{
	[Header("Target Settings")]
	public GameObject target;
	public List<Tower> possibleTargets;
	public bool hasTarget = false;
	[Header("Effects Settings")]
	public LineRenderer laserSight;
	[Header("Agent Settings")]
	public NavMeshAgent agent;

	new public void OnEnable()
	{
		if (isDead) {
			hasTarget = false;
			target = null;
			laserSight.enabled = false;
			possibleTargets.Clear();
		}
		else {
			possibleTargets = new List<Tower>();
		}
		UpdateLaserSight(laserSight, laserSight.transform.position);

		base.OnEnable();
		DI_Events.EventCenter<Entity, Entity>.addListener("OnDeath", handleKillEnemy);

	}

	new public void OnDisable()
	{
		DI_Events.EventCenter<Entity, Entity>.removeListener("OnDeath", handleKillEnemy);
	}

	public void handleKillEnemy(Entity victim, Entity attacker)
	{
		if (victim.gameObject == target) {
			possibleTargets.Remove(target.GetComponent<Tower>());
			target = null;
			hasTarget = false;
			laserSight.enabled = false;
		}
	}

	public void OnTriggerEnter(Collider other)
	{
		if (!hasTarget) {
			if (other.tag == "Tower") {
				Tower targetInfo = other.GetComponent<Tower>();
				if (!possibleTargets.Contains(targetInfo)) {
					possibleTargets.Add(targetInfo);
				}
			}
		}
	}
	
	public void OnTriggerStay(Collider other)
	{
		if (!hasTarget) {
			if (other.tag == "Tower") {
				Tower targetInfo = other.GetComponent<Tower>();
				if (!possibleTargets.Contains(targetInfo)) {
					possibleTargets.Add(targetInfo);
				}
			}
		}
	}
	
	public void OnTriggerExit(Collider other)
	{
		if (other.tag == "Tower") {
			Tower targetInfo = other.GetComponent<Tower>();
			possibleTargets.Remove(targetInfo);
			if (target == targetInfo) {
				hasTarget = false;
				target = null;
				laserSight.enabled = false;
			}
		}
	}
	
	public void LateUpdate()
	{
		if (gsc.gameState == GameStates.PLAYING) {
			if (hasTarget) {
				if (target != null) {
					if (target.activeInHierarchy) {
						//Debug.Log(System.Reflection.MethodBase.GetCurrentMethod().Name);
						if (waypointAgent.agentActive) {
							waypointAgent.stopAgent();
						}
						transform.LookAt(new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z));
						transform.localRotation.Set(0, transform.localRotation.y, transform.localRotation.z, transform.localRotation.w);
						UpdateLaserSight(laserSight, target.GetComponent<Rigidbody>().worldCenterOfMass);
						if (!isAttacking) {
							StartCoroutine("Attack", target);
						}
					}
					else {
						hasTarget = false;
						laserSight.enabled = false;
					}
				}
				else {
					hasTarget = false;
					laserSight.enabled = false;
				}
			}
			
			if (!hasTarget) {
				if (!waypointAgent.agentActive) {
					waypointAgent.startAgent();
				}
				if (possibleTargets.Count > 0) {
					if (possibleTargets[0] != null) {
						target = possibleTargets[0].gameObject;
						if (target.activeInHierarchy) {
							laserSight.enabled = true;
							hasTarget = true;
						}
						else {
							possibleTargets.Remove(target.GetComponent<Tower>());
							hasTarget = false;
							laserSight.enabled = false;
							target = null;
						}
					}
				}
			}
		}
	}

	new public IEnumerator spawnProjectile()
	{
		if (target != null) {
			if (target.activeInHierarchy) {
				GameObject projectile = PoolController.instance.getPooledObject(projectilePrefab.name);
				//Debug.Log(System.Reflection.MethodBase.GetCurrentMethod().Name);
				ProjectileController projectileController = projectile.GetComponent<ProjectileController>();
				projectile.transform.position = projectileLaunchPoint.transform.position;
				projectile.SetActive(true);
				projectile.transform.LookAt(target.GetComponent<Rigidbody>().worldCenterOfMass);
				projectileController.owner = (Entity)this;
				projectileController.pointReduction = damage;
				projectile.GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * projectileController.projectileSpeed);
			}
			else {
				possibleTargets.Remove(target.GetComponent<Tower>());
				hasTarget = false;
				laserSight.enabled = false;
				target = null;
			}
		}
		yield return new WaitForSeconds(projectileSpawnDelay);
	}

	public void UpdateLaserSight(LineRenderer laser, Vector3 target)
	{
		laser.SetPosition(0, laser.transform.position);
		laser.SetPosition(1, target);
	}

	public IEnumerator Attack() {
		isAttacking = true;
		if (hasTarget) {
			if (target != null) {
				if (target.activeInHierarchy) {
					//Debug.Log(System.Reflection.MethodBase.GetCurrentMethod().Name);
					StartCoroutine("spawnProjectile");
					yield return new WaitForSeconds(attackSpeed);
				}
				else {
					possibleTargets.Remove(target.GetComponent<Tower>());
					hasTarget = false;
					laserSight.enabled = false;
					target = null;
				}
			}
		}
		isAttacking = false;
	}

	new public IEnumerator Attack(Entity _target) {
		isAttacking = true;
		hasTarget = true;
		target = _target.gameObject;

		if (target != null) {
			//Debug.Log(System.Reflection.MethodBase.GetCurrentMethod().Name);
			StartCoroutine("spawnProjectile");
			yield return new WaitForSeconds(attackSpeed);
		}
		isAttacking = false;
	}
}