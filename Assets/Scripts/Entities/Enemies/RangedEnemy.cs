// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[AddComponentMenu("Entities/Ranged Enemy")]
public class RangedEnemy: Enemy
{
	[HideInInspector]
	public GameObject star;

	[Header("Projectile Settings")]
	public GameObject projectilePrefab;
	public GameObject projectileLaunchPoint;
	public float projectileSpawnDelay = 0.2f;

	new public void OnEnable()
	{
		star = GameObject.FindGameObjectWithTag("Star");
		base.OnEnable();
	}

	public IEnumerator spawnProjectile()
	{
		yield return new WaitForSeconds(projectileSpawnDelay);
		GameObject projectile = PoolController.instance.getPooledObject(projectilePrefab.name);
		projectile.SetActive(true);
		ProjectileController projectileController = projectile.GetComponent<ProjectileController>();
		projectile.transform.position = projectileLaunchPoint.transform.position;
		projectile.transform.LookAt(star.transform.position);
		projectileController.owner = (Entity)this;
		projectileController.pointReduction = damage;
		projectile.GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * projectileController.projectileSpeed);
	}
	
	new public IEnumerator Attack(Entity target) {
		isAttacking = true;
		StartCoroutine("spawnProjectile");
		yield return new WaitForSeconds(attackSpeed);
		isAttacking = false;
	}
}