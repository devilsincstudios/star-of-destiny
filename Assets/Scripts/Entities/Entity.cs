// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

#region Includes
using UnityEngine;
//using UnityEditor;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
#endregion

public class Entity : MonoBehaviour
{
	#region Public Variables
	[Header("Movement Settings")]
	public bool canMove = true;
	public float movementSpeed = 1.0f;
	[HideInInspector]
	public bool isSlowed = false;

	[Header("Health Settings")]
	public bool isImmortal = false;
	public float maxHealth = 100.0f;
	[HideInInspector]
	public float originalMaxHealth = 0.0f;
	public float health = 100.0f;
	[HideInInspector]
	public bool isDead = false;

	[Header("Status Settings: Burning")]
	public bool isBurningImmune = false;
	public bool isBurning = false;
	public float burnTimeRemaining = 0.0f;
	public float burnDamage = 0.0f;
	public Color burningColor = Color.red;
	[Header("Status Settings: Frozen")]
	public bool isFrozenImmune = false;
	public bool isFrozen = false;
	public float frozenTimeRemaining = 0.0f;
	public Color frozenColor = Color.blue;
	[Header("Status Settings: Chilled")]
	public bool isChilledImmune = false;
	public bool isChilled = false;
	public Color chilledColor = Color.blue;
	public float chilledSpeedReduction = 0.0f;
	public float chilledTimeRemaining = 0.0f;
	[Header("Status Settings: Poisoned")]
	public bool isPoisonImmune = false;
	public bool isPoisoned = false;
	public float poisonDamage = 0.0f;
	public float poisonedTimeRemaining = 0.0f;
	public Color poisonedColor = Color.green;

	protected List<Color> originalColor;
	[HideInInspector]
	public float originalMovementSpeed = 0.0f;
	protected List<Renderer> renderers;

	[Header("GUI Settings")]
	public Image healthBar;

	[Header("SFX Settings")]
	public List<AudioClip> deathSounds;
	public float deathSoundsVolume;

	[Header("Animation Settings")]
	public bool hasAnimations;
	[HideInInspector]
	public Animator entityAnimator;

	private float lastStatusCheck = 0.0f;
	public bool hasDied = false;

	#endregion

	#region Public Methods
	public void OnEnable()
	{
		DI_Events.EventCenter<Entity, Entity, float, AmmoTypes>.addListener("OnHit", handleOnHit);
		DI_Events.EventCenter<Entity, Entity, float, float>.addListener("OnChill", handleOnChill);
		DI_Events.EventCenter<Entity, Entity, float, float>.addListener("OnBurn", handleOnBurn);
		DI_Events.EventCenter<Entity, Entity, float>.addListener("OnFreeze", handleOnFreeze);
		DI_Events.EventCenter<Entity, Entity, float, float>.addListener("OnPoison", handleOnPoison);

		health = maxHealth;

		if (!isDead) {
			originalColor = new List<Color>();
			renderers = new List<Renderer>();
			foreach (Renderer childRenderer in this.gameObject.transform.GetComponentsInChildren<Renderer>()) {
				if (childRenderer.GetComponent<Renderer>().material != null) {
					if (childRenderer.GetComponent<Renderer>().material.HasProperty("_Color")) {
						originalColor.Add(childRenderer.GetComponent<Renderer>().material.color);
						renderers.Add(childRenderer);
					}
				}
			}

			if (hasAnimations) {
				entityAnimator = GetComponentInChildren<Animator>();
				if (entityAnimator == null) {
					hasAnimations = false;
				}
			}
			originalMaxHealth = maxHealth;
		}
		else {
			isDead = false;
			isChilled = false;
			isPoisoned = false;
			isFrozen = false;
			isBurning = false;
			burnTimeRemaining = 0.0f;
			burnDamage = 0.0f;
			chilledTimeRemaining = 0.0f;
			chilledSpeedReduction = 0.0f;
			poisonDamage = 0.0f;
			poisonedTimeRemaining = 0.0f;
			frozenTimeRemaining = 0.0f;
		}

		originalMovementSpeed = movementSpeed;
		if (!isImmortal && healthBar != null) {
			healthBar.fillAmount = health / maxHealth;
		}
	}

	public void OnDisable()
	{
		DI_Events.EventCenter<Entity, Entity, float, AmmoTypes>.removeListener("OnHit", handleOnHit);
		DI_Events.EventCenter<Entity, Entity, float, float>.removeListener("OnChill", handleOnChill);
		DI_Events.EventCenter<Entity, Entity, float, float>.removeListener("OnBurn", handleOnBurn);
		DI_Events.EventCenter<Entity, Entity, float>.removeListener("OnFreeze", handleOnFreeze);
		DI_Events.EventCenter<Entity, Entity, float, float>.removeListener("OnPoison", handleOnPoison);
	}

	public void Update()
	{
		if (GameStateController.instance.gameState == GameStates.PLAYING) {
			if (!isDead) {
				lastStatusCheck += Time.deltaTime;
				if (lastStatusCheck >= 1.0f) {
					lastStatusCheck -= 1.0f;

					if (isBurning) {
						setEffectColor(burningColor);
						burnTimeRemaining -= 1.0f;
						setHealth(health - burnDamage);
						if (burnTimeRemaining <= 0.0f) {
							isBurning = false;
							burnTimeRemaining = 0.0f;
						}
					}

					if (isPoisoned) {
						setEffectColor(poisonedColor);
						poisonedTimeRemaining -= 1.0f;
						setHealth(health - poisonDamage);
						if (poisonedTimeRemaining <= 0.0f) {
							isPoisoned = false;
							poisonedTimeRemaining = 0.0f;
						}
					}

					if (isFrozen) {
						setEffectColor(frozenColor);
						frozenTimeRemaining -= 1.0f;
						if (frozenTimeRemaining <= 0.0f) {
							isFrozen = false;
							canMove = true;
							frozenTimeRemaining = 0.0f;
						}
					}

					if (isChilled) {
						setEffectColor(chilledColor);
						chilledTimeRemaining -= 1.0f;
						if (chilledTimeRemaining <= 0.0f) {
							isChilled = false;
							setSpeed(originalMovementSpeed * (chilledSpeedReduction/100));
							chilledTimeRemaining = 0.0f;
						}
					}
					else {
						setSpeed(originalMovementSpeed);
					}

					if (!isBurning && !isPoisoned && !isFrozen && !isChilled) {
						setEffectColor(originalColor);
					}
				}
			}
		}
	}
	
	/// <summary>
	/// Sets the health.
	/// </summary>
	/// <param name="newHealth">New health.</param>
	public void setHealth(float newHealth)
	{
		health = newHealth;
		if (health > maxHealth) {
			maxHealth = newHealth;
		}
		healthBar.fillAmount = health / maxHealth;
	}

	/// <summary>
	/// Sets the max health.
	/// </summary>
	/// <param name="newHealth">New health.</param>
	public void setMaxHealth(float newHealth)
	{
		maxHealth = newHealth;
		healthBar.fillAmount = health / maxHealth;
	}

	/// <summary>
	/// Sets the speed.
	/// </summary>
	/// <param name="newSpeed">New speed.</param>
	public void setSpeed(float newSpeed)
	{
		movementSpeed = newSpeed;
	}

	/// <summary>
	/// Raises the death event.
	/// </summary>
	/// <param name="attacker">Attacker.</param>
	public void OnDeath(Entity attacker)
	{
		isDead = true;
		hasDied = true;
		CancelInvoke();
		StopAllCoroutines();

		if (deathSounds.Count > 0) {
			DI_Events.EventCenter<AudioClip, float, Vector3>.invoke("OnPlayEffectAtPoint", 
			                                                        deathSounds[UnityEngine.Random.Range(0, deathSounds.Count)],
			                                                        deathSoundsVolume,
			                                                        this.transform.position);
		}
		DI_Events.EventCenter<Entity, Entity>.invoke("OnDeath", this, attacker);
		if (hasAnimations) {
			StartCoroutine("playDeathAnimation");
		}
		else {
			if (this.tag == "Tower") {
				Destroy(this.gameObject);
			}
			else {
				gameObject.SetActive(false);
			}
		}
	}

	/// <summary>
	/// Handles the on hit.
	/// </summary>
	/// <param name="target">Target.</param>
	/// <param name="attacker">Attacker.</param>
	/// <param name="damage">Damage.</param>
	public void handleOnHit(Entity target, Entity attacker, float damage, AmmoTypes ammoType)
	{
		if (target == this) {
			if (!isDead) {
				if (!isImmortal) {
					health -= damage;
					if (healthBar != null) {
						healthBar.fillAmount = health / maxHealth;
					}
					if (health <= 0) {
						this.OnDeath(attacker);
					}
				}
			}
		}
	}

	public void handleOnChill(Entity target, Entity attacker, float duration,  float speedReduction)
	{
		if (target == this) {
			if (!isDead) {
				if (!isImmortal) {
					if (!isChilledImmune) {
						setEffectColor(chilledColor);
						//Debug.Log(System.Reflection.MethodBase.GetCurrentMethod().Name);
						isChilled = true;
						chilledTimeRemaining = duration;
						if (speedReduction > chilledSpeedReduction) {
							chilledSpeedReduction = speedReduction;
						}
						setSpeed(originalMovementSpeed * (chilledSpeedReduction/100));
					}
				}
			}
		}
	}

	public void handleOnBurn(Entity target, Entity attacker, float burnTime, float burningDamage) {
		if (target == this) {
			if (!isDead) {
				if (!isImmortal) {
					if (!isBurningImmune) {
						setEffectColor(burningColor);
						//Debug.Log(System.Reflection.MethodBase.GetCurrentMethod().Name);
						isBurning = true;
						burnTimeRemaining = burnTime;
						if (burningDamage > burnDamage) {
							burnDamage = burningDamage;
						}
					}
				}
			}
		}
	}

	public void handleOnFreeze(Entity target, Entity attacker, float freezeTime) {
		if (target == this) {
			if (!isDead) {
				if (!isImmortal) {
					if (!isFrozenImmune) {
						setEffectColor(frozenColor);
						//Debug.Log(System.Reflection.MethodBase.GetCurrentMethod().Name);
						isFrozen = true;
						canMove = false;
						if (freezeTime > frozenTimeRemaining) {
							frozenTimeRemaining = freezeTime;
						}
					}
				}
			}
		}
	}

	public void handleOnPoison(Entity target, Entity attacker, float poisonTime, float poisoningDamage) {
		if (target == this) {
			if (!isDead) {
				if (!isImmortal) {
					if (!isPoisonImmune) {
						setEffectColor(poisonedColor);
						//Debug.Log(System.Reflection.MethodBase.GetCurrentMethod().Name);
						isPoisoned = true;
						poisonedTimeRemaining = poisonTime;
						if (poisoningDamage > poisonDamage) {
							poisonDamage = poisoningDamage;
						}
					}
				}
			}
		}
	}

	/// <summary>
	/// Plays the death animation.
	/// </summary>
	/// <returns>The death animation.</returns>
	public IEnumerator playDeathAnimation()
	{
		if (hasAnimations) {
			entityAnimator.SetBool("Dead", true);
		}
		yield return new WaitForSeconds(2.0f);
		gameObject.SetActive(false);
	}

	public void setEffectColor(List<Color> effectColors)
	{
		try {
			for (int index = 0; index < renderers.Count; ++index) {
				renderers[index].material.color = effectColors[index];
			}
		}
		catch (Exception) {
		}
	}

	public void setEffectColor(Color effectColor)
	{
		try {
			for (int index = 0; index < renderers.Count; ++index) {
				renderers[index].material.color = effectColor;
			}
		}
		catch (Exception) {
		}
	}
	#endregion
}