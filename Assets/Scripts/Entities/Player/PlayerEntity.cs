// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System;
[AddComponentMenu("Entities/Player")]
public class PlayerEntity : Entity
{
	public PlayerState playerState;

	new public void OnEnable()
	{
		base.OnEnable();
		playerState.entity = this;
	}
}