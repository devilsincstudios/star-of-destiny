﻿// // Devils Inc Studios
// // Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
// //
// // TODO: Include a description of the file here.
// //

using UnityEngine;
using System.Collections;

public class BaubleProjectileController : MonoBehaviour {
	public float spawnTimer = 10000;
	public float projectileSpeed;
	public float shotDelay = 0.0f;

	// Update is called once per frame
	public void Update ()
	{
		spawnTimer -= Time.deltaTime;
		if (spawnTimer <= 0.0f)
		{
			this.gameObject.SetActive(false);
		}
	}
}
