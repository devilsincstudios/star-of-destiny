﻿using UnityEngine;
using System.Collections;

public class BossTrackerSnowball : MonoBehaviour {

	public Transform enemyTransform;
	public float moveSpeed;

	private Vector3 enemyEditedTransform;
	

	// Update is called once per frame
	void Update () {
		float step = moveSpeed * Time.deltaTime;
		enemyEditedTransform = enemyTransform.position;
		enemyEditedTransform.y += 1;
		transform.position = Vector3.MoveTowards(transform.position, enemyEditedTransform, step);
	}
}
