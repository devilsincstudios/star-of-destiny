﻿// // Devils Inc Studios
// // Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
// //
// // TODO: Include a description of the file here.
// //

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PresentBomb : MonoBehaviour {
	public List<Enemy> enemyToDamage = new List<Enemy>();
	public float damageTimer;
	public float timeTillExplode;
	public PlayerEntity owner;
	public int pointReduction;

	// Use this for initialization
	void Start () {
		owner = GameObject.Find("Player One").GetComponent<PlayerEntity>();

		if (timeTillExplode == 0) {
			timeTillExplode = 5;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(damageTimer >= timeTillExplode){
			DealDamageToEnemy();
		}

		damageTimer += Time.deltaTime;
	}

	void OnTriggerEnter(Collider col)
	{
		if (col.tag == "Enemy") {
			if (!enemyToDamage.Contains(col.GetComponent<Enemy>())) {
				enemyToDamage.Add(col.GetComponent<Enemy>());
			}
		}
	}
	
	void OnTriggerExit(Collider col)
	{
		if (col.tag == "Enemy") {
			if (enemyToDamage.Contains(col.GetComponent<Enemy>())) {
				enemyToDamage.Add(col.GetComponent<Enemy>());
			}
		}
	}

	void DealDamageToEnemy(){
		for(int i = 0; i < enemyToDamage.Count; i++){
			Debug.Log ("Bang");
			DI_Events.EventCenter<Entity, Entity, float, AmmoTypes>.invoke("OnHit", enemyToDamage[i], owner, pointReduction, AmmoTypes.AMMO_PRESENT_BOMB);
		}
		Destroy(gameObject);
	}
}
