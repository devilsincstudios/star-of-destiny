﻿// // Devils Inc Studios
// // Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
// //
// // TODO: Include a description of the file here.
// //

using UnityEngine;
using System.Collections;

public class ProjectileController : MonoBehaviour {
	[Header("Projectile Settings")]
	public float spawnTimer = 5;
	public float projectileSpeed = 0.0f;
	public bool isEnemyProjectile;
	public AmmoTypes ammoType;

	[Header("SFX Settings")]
	public AudioClip splatSound;
	public float splatSoundVolume = 0.25f;
	public AudioClip weaponSFX;

	[Header("Damage Settings")]
	public float shotDelay = 0.0f;
	public float pointReduction = 0.0f;
	[HideInInspector]
	public float originalPointReduction = 0.0f;

	[Header("Status Settings: Burn")]
	public bool canBurn = false;
	public float burnChance = 0.0f;
	public float burnDamage = 0.0f;
	public float burnDuration = 0.0f;

	[Header("Status Settings: Freeze")]
	public bool canFreeze = false;
	public float freezeChance = 0.0f;
	public float freezeDuration = 0.0f;

	[Header("Status Settings: Chill")]
	public bool canChill = false;
	public float chillChance = 0.0f;
	public float chillSlow = 0.0f;
	public float chillDuration = 0.0f;

	[Header("Status Settings: Poison")]
	public bool canPoison = false;
	public float poisonChance = 0.0f;
	public float poisonDamage = 0.0f;
	public float poisonDuration = 0.0f;

	[HideInInspector]
	public int enemiesHit = 0;

	[HideInInspector]
	public Entity owner;

	[HideInInspector]
	public bool firstSpawn = true;

	public void OnEnable()
	{
		if (firstSpawn) {
			originalPointReduction = pointReduction;
		}

		GetComponent<Rigidbody>().velocity = Vector3.zero;
		GetComponent<Rigidbody>().angularVelocity = Vector3.zero;

		enemiesHit = 0;
		owner = GameStateController.instance.playerState.entity;

		DI_Events.EventCenter<AudioClip, float>.invoke("OnPlayEffect", weaponSFX, 0.25f);
		if (ammoType != AmmoTypes.AMMO_STEAM) {
			StartCoroutine(expire());
		}
	}

	public IEnumerator expire()
	{
		yield return new WaitForSeconds(spawnTimer);
		this.gameObject.SetActive(false);
	}

	public void OnCollisionEnter (Collision other)
	{

		if (this.ammoType != AmmoTypes.AMMO_BAUBLE_GRENADE 
		    && this.ammoType != AmmoTypes.AMMO_TOWER_CANNON 
		    && this.ammoType != AmmoTypes.AMMO_PRESENT_BOMB 
		    && this.ammoType != AmmoTypes.AMMO_CANDY_CANE) {

			if (isEnemyProjectile && other.gameObject.tag == "Star" && ammoType == AmmoTypes.AMMO_ENEMY) {
				DI_Events.EventCenter<AudioClip, float>.invoke("OnPlayEffect", splatSound, 1.0f);
				DI_Events.EventCenter<Entity, Entity, float, AmmoTypes>.invoke("OnHit", other.gameObject.GetComponent<Entity>(), owner, (float)pointReduction, AmmoTypes.AMMO_ENEMY);
				this.gameObject.SetActive(false);
			}

			if (isEnemyProjectile && other.gameObject.tag == "Star" && ammoType == AmmoTypes.AMMO_RAMBO) {
				DI_Events.EventCenter<AudioClip, float>.invoke("OnPlayEffect", splatSound, 1.0f);
				DI_Events.EventCenter<Entity, Entity, float, AmmoTypes>.invoke("OnHit", other.gameObject.GetComponent<Entity>(), owner, (float)pointReduction, AmmoTypes.AMMO_ENEMY);
				GameObject explosion = PoolController.instance.getPooledObject("Explosion");
				explosion.transform.position = this.transform.position;
				explosion.transform.localEulerAngles = Vector3.zero;
				explosion.SetActive(true);
				this.gameObject.SetActive(false);
			}
			else if (isEnemyProjectile && other.gameObject.tag == "Tower") {
				DI_Events.EventCenter<AudioClip, float>.invoke("OnPlayEffect", splatSound, 1.0f);
				DI_Events.EventCenter<Entity, Entity, float, AmmoTypes>.invoke("OnHit", other.gameObject.GetComponent<Entity>(), owner, (float)pointReduction, AmmoTypes.AMMO_ENEMY);
				GameObject explosion = PoolController.instance.getPooledObject("Explosion");
				explosion.transform.position = this.transform.position;
				explosion.transform.localEulerAngles = Vector3.zero;
				explosion.SetActive(true);
				this.gameObject.SetActive(false);
			}
			else if (!isEnemyProjectile && other.gameObject.tag == "Enemy") {
				Enemy enemy = other.gameObject.GetComponent<Enemy>();
				DI_Events.EventCenter<Entity, Entity, float, AmmoTypes>.invoke("OnHit", enemy, owner, pointReduction, ammoType);
				DI_Events.EventCenter<AudioClip, float, Vector3>.invoke("OnPlayEffectAtPoint", splatSound, splatSoundVolume, other.transform.position);

				float statusRoll = UnityEngine.Random.Range(0, 100);

				if (canBurn && burnChance >= statusRoll) {
					//Debug.Log("Projectile Controller: Burn!");
					DI_Events.EventCenter<Entity, Entity, float, float>.invoke("OnBurn", enemy, owner, burnDuration, burnDamage);
				}

				if (canFreeze && freezeChance >= statusRoll) {
					//Debug.Log("Projectile Controller: Freeze!");
					DI_Events.EventCenter<Entity, Entity, float>.invoke("OnFreeze", enemy, owner, freezeDuration);
				}

				if (canChill && chillChance >= statusRoll) {
					//Debug.Log("Projectile Controller: Chill!");
					DI_Events.EventCenter<Entity, Entity, float, float>.invoke("OnChill", enemy, owner, chillDuration, chillSlow);
				}

				if (canPoison && poisonChance >= statusRoll) {
					//Debug.Log("Projectile Controller: Poison!");
					DI_Events.EventCenter<Entity, Entity, float, float>.invoke("OnPoison", enemy, owner, poisonDuration, poisonDamage);
				}

				if (ammoType != AmmoTypes.AMMO_TOWER_ROCKET) {
					this.gameObject.SetActive(false);
				}
				else {
					AOEDamage damageScript = gameObject.GetComponent<AOEDamage>();
					damageScript.explosionDelay = 0.0f;
					StartCoroutine(damageScript.explode());
				}
			}
			else {
				if (ammoType != AmmoTypes.AMMO_TOWER_ROCKET && ammoType != AmmoTypes.AMMO_STEAM) {
					this.gameObject.SetActive(false);
				}
				else if (ammoType == AmmoTypes.AMMO_TOWER_ROCKET) {
					AOEDamage damageScript = gameObject.GetComponent<AOEDamage>();
					damageScript.explosionDelay = 0.0f;
					StartCoroutine(damageScript.explode());
				}
				// Don't destroy steam
			}
		}
	}

	public void OnTriggerEnter (Collider other)
	{
		if (this.ammoType == AmmoTypes.AMMO_CANDY_CANE) {
			if (other.gameObject.tag == "Enemy" && other.gameObject.name == "Hit Box") {
				++enemiesHit;
				#if DEBUG
					Debug.Log("[Good] Collided with: " + other.gameObject.name + " Tag: " + other.gameObject.tag + " Layer: " + other.gameObject.layer);
				#endif
				Enemy enemy = other.gameObject.transform.parent.GetComponent<Enemy>();
				DI_Events.EventCenter<Entity, Entity, float, AmmoTypes>.invoke("OnHit", enemy, owner, Mathf.Round(pointReduction / enemiesHit), ammoType);
				DI_Events.EventCenter<AudioClip, float, Vector3>.invoke("OnPlayEffectAtPoint", splatSound, splatSoundVolume, other.transform.position);

				if (enemiesHit > 3) {
					this.gameObject.SetActive(false);
				}
			}
			else if (other.gameObject.tag == "Enemy") {
				#if DEBUG
					Debug.Log("[Bad] Collided with: " + other.gameObject.name + " Tag: " + other.gameObject.tag + " Layer: " + other.gameObject.layer);
				#endif
			}
			else {
				#if DEBUG
					Debug.Log("[Miss] Collided with: " + other.gameObject.name + " Tag: " + other.gameObject.tag + " Layer: " + other.gameObject.layer);
				#endif
				this.gameObject.SetActive(false);
			}
		}
	}
}
