﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SantaBomber : MonoBehaviour
{
	[HideInInspector]
	public PlayerEntity owner;
	[Header("Audio")]
	public AudioClip attackSFX;
	public float attackSFXVolume;
	public AudioClip arrivalSound;
	public float arrivalSoundVolume;

	[Header("Damage Settings")]
	public float damage;
	public float lifeSpan = 10.0f;
	public float rescanDelay = 1.0f;
	public float shotDelay = 30.0f;
	[HideInInspector]
	public float originalDamage;

	[HideInInspector]
	public Enemy[] foundTargets;
	[HideInInspector]
	public bool isAttacking = false;
	[HideInInspector]
	public GameObject bomberEffect;
	[HideInInspector]
	public bool firstSpawn = true;

	//Use this for initialization
	public void OnEnable ()
	{
		if (firstSpawn) {
			firstSpawn = false;
			originalDamage = damage;
		}
		owner = GameObject.Find("Player One").GetComponent<PlayerEntity>();
		bomberEffect = owner.playerState.gsc.gameObject.GetComponent<PrefabLoader>().getPrefab("Bomber Effect");
		StartCoroutine("lifeTimer");
		DI_Events.EventCenter<AudioClip, float>.invoke("OnVoiceOver", arrivalSound, arrivalSoundVolume);
		transform.position = GameObject.FindGameObjectWithTag("Star").transform.position;
		transform.localPosition = new Vector3(0.0f, 10.0f, 0.0f);
	}

	public void Update()
	{
		if (!isAttacking)
		{
			StartCoroutine("attack");
		}
	}

	public IEnumerator attack()
	{
		isAttacking = true;
		yield return new WaitForSeconds(rescanDelay);
		DI_Events.EventCenter<AudioClip, float>.invoke("OnPlayEffect", attackSFX, attackSFXVolume);
		foundTargets = GameObject.FindObjectsOfType<Enemy>();
		for(int i = 0; i < foundTargets.Length; i++) {
			try {
				Enemy enemy = foundTargets[i];
				if (!enemy.isDead) {
					DI_Events.EventCenter<Entity, Entity, float, AmmoTypes>.invoke("OnHit", enemy, owner, damage, AmmoTypes.AMMO_SANTA_BOMBER);
					GameObject effect = (GameObject)Instantiate(bomberEffect);
					effect.transform.position = enemy.gameObject.transform.position;
				}
			}
			catch (Exception error) {
				Debug.LogException(error);
			}
		}
		isAttacking = false;
	}

	public IEnumerator lifeTimer()
	{
		yield return new WaitForSeconds(lifeSpan);
		Destroy(this.gameObject);
	}
}
