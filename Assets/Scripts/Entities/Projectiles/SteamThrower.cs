// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[AddComponentMenu("Entities/Projectiles/Steam Thrower")]
public class SteamThrower : ProjectileController
{
	[Header("Damage Settings")]
	public bool isActiveWeapon = false;
	public float rawDamagePerLevel = 0.0f;
	public float burnTimePerLevel = 0.0f;
	public float burnDamagePerLevel = 0.0f;
	public float attackSpeed = 0.1f;

	[Header("Effects Settings")]
	public GameObject effects;

	[HideInInspector]
	public List<Enemy> targets;
	[HideInInspector]
	public bool isAttacking = false;
	private int weaponId;

	new public void OnEnable()
	{
		weaponId = GameStateController.instance.playerState.getXpId(AmmoTypes.AMMO_STEAM);
		owner = GameObject.Find("Player One").GetComponent<PlayerEntity>();
	}

	new public void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Enemy" && other.name != "Hit Box") {
			Enemy enemy = other.GetComponent<Enemy>();
			if (!targets.Contains(enemy)) {
				targets.Add(enemy);
			}
		}
	}

	public void OnTriggerExit(Collider other)
	{
		if (other.tag == "Enemy" && other.name != "Hit Box") {
			Enemy enemy = other.GetComponent<Enemy>();
			if (targets.Contains(enemy)) {
				targets.Remove(enemy);
			}
		}
	}

	public void FixedUpdate()
	{
		if (GameStateController.instance.gameState == GameStates.PLAYING) {
			if (isActiveWeapon) {
				if (Input.GetButton("Fire1")) {
					effects.SetActive(true);
					if (!isAttacking) {
						List<Enemy> tempList = new List<Enemy>(targets);
						foreach (Enemy target in tempList) {
							if (target == null) {
								targets.Remove(target);
							}
						}
						tempList = null;
						
						if (targets.Count > 0) {
							StartCoroutine("Attack");
						}
					}
				}
				else {
					effects.SetActive(false);
				}
			}
			else {
				effects.SetActive(false);
			}
		}
		else {
			effects.SetActive(false);
		}
	}

	public IEnumerator Attack()
	{
		isAttacking = true;
		foreach (Enemy target in targets) {
			if (target != null) {
				if (!target.isDead) {
					float statusRoll = UnityEngine.Random.Range(0, 100);
					
					if (canBurn && burnChance >= statusRoll) {
						//Debug.Log("Projectile Controller: Burn!");
						DI_Events.EventCenter<Entity, Entity, float, float>.invoke("OnBurn", 
						                                                           target, 
						                                                           owner,
						                                                           burnDuration + (GameStateController.instance.playerState.getLevel(weaponId) * burnTimePerLevel),
						                                                           burnDamage + (GameStateController.instance.playerState.getLevel(weaponId) * burnDamagePerLevel));
					}
					DI_Events.EventCenter<Entity, Entity, float, AmmoTypes>.invoke("OnHit", target, 
					                                                               owner, 
					                                                               pointReduction + (GameStateController.instance.playerState.getLevel(weaponId) * rawDamagePerLevel), 
					                                                               AmmoTypes.AMMO_STEAM);
				}
			}
		}
		yield return new WaitForSeconds(attackSpeed);
		isAttacking = false;
	}
}
