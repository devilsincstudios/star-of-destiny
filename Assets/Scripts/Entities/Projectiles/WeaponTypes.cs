﻿// // Devils Inc Studios
// // Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
// //
// // TODO: Include a description of the file here.
// //

public enum WeaponType
{
	ICICLE,
	CANDY_CANE,
	SNOWBALL,
	STEAM,
	BAUBLE_GRENADE,
	PRESENT_BOMB,
	SANTA_BOMBER
}