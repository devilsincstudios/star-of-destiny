// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using UnityEngine.UI;

[AddComponentMenu("Entities/Star")]
public class Star : Entity
{
	public Text healthText;
	public GameStateController gsc;
	[Header("Star Attack Notification")]
	public GameObject starWarning;
	public float warningTimer = 5.0f;
	public float warningTimeRemaining = 0.0f;

	new public void OnEnable()
	{
		DI_Events.EventCenter<Entity, Entity, float, AmmoTypes>.addListener("OnHit", handleOnHit);
		UpdateHealthBar();
		gsc = GameObject.Find("Game State").GetComponent<GameStateController>();
	}

	new public void Update()
	{
		if (warningTimeRemaining > 0.0f) {
			warningTimeRemaining -= Time.deltaTime;
		}
		else {
			warningTimeRemaining = 0.0f;
			starWarning.SetActive(false);
		}
	}

	new public void OnDisable()
	{
		DI_Events.EventCenter<Entity, Entity, float, AmmoTypes>.removeListener("OnHit", handleOnHit);
	}

	new public void handleOnHit(Entity target, Entity attacker, float damage, AmmoTypes ammoType)
	{
		if (!gsc.cheatStarInvincible) {
			if (target == this) {
				if (!isDead) {
					health -= damage;
					warningTimeRemaining = warningTimer;
					starWarning.SetActive(true);
					if (health <= 0) {
						OnDeath(attacker);
						health = 0;
					}
					UpdateHealthBar();
				}
			}
		}
	}

	new public void OnDeath(Entity attacker)
	{
		//StartCoroutine("playDeathAnimation");
		//base.OnDeath(attacker);
		DI_Events.EventCenter.invoke("OnStarDeath");
	}

	public void UpdateHealthBar()
	{
		healthText.text = Mathf.RoundToInt((health / maxHealth) * 100) + "%";
		healthBar.fillAmount = health / maxHealth;
	}
}