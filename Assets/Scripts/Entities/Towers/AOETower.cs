// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;

[RequireComponent(typeof(SphereCollider))]
[AddComponentMenu("AI/Towers/AOE Tower")]
public class AOETower : Tower
{
	[Header("Target Settings")]
	public List<Enemy> targets;
	[Header("Effect Settings")]
	public GameObject particles;

	public void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Enemy" && other.name == "Hit Box") {
			Enemy enemy = other.transform.parent.GetComponent<Enemy>();
			if (!targets.Contains(enemy)) {
				if (!stats.canTargetAir && enemy.type == Enemies.SNOWMAN_FLYINGMAN) {
				}
				else {
					targets.Add(enemy);
				}
			}
		}
	}

	public void OnTriggerStay(Collider other)
	{
		if (other.tag == "Enemy" && other.name == "Hit Box") {
			Enemy enemy = other.transform.parent.GetComponent<Enemy>();
			if (!targets.Contains(enemy)) {
				if (!stats.canTargetAir && enemy.type == Enemies.SNOWMAN_FLYINGMAN) {
				}
				else {
						targets.Add(enemy);
				}
			}
		}
	}
	
	public void OnTriggerExit(Collider other)
	{
		if (other.tag == "Enemy" && other.name == "Hit Box") {
			if (targets.Contains(other.transform.parent.GetComponent<Enemy>())) {
				targets.Remove(other.transform.parent.GetComponent<Enemy>());
			}
		}
	}
	
	public void FixedUpdate()
	{
		if (owner.playerState.gsc.gameState == GameStates.PLAYING) {
			List<Enemy> tempList = new List<Enemy>(targets);
			if (!isAttacking) {
				foreach (Enemy target in tempList) {
					if (target == null) {
						targets.Remove(target);
					}
					else if (!target.gameObject.activeInHierarchy) {
						targets.Remove(target);
					}
					else if (target.isDead) {
						targets.Remove(target);
					}
				}
				tempList = null;

				if (targets.Count > 0) {
					particles.SetActive(true);
					StartCoroutine("Attack");
				}
				else {
					particles.SetActive(false);
				}
			}
		}
	}
	
	public IEnumerator Attack()
	{
		isAttacking = true;
		foreach (Enemy target in targets) {
			if (target != null) {
				if (target.gameObject.activeInHierarchy) {
					if (!target.isDead) {
						if (type == TowerTypes.TOWER_AOE_DAMAGE) {
							DI_Events.EventCenter<Entity, Entity, float, AmmoTypes>.invoke("OnHit", target, this, stats.baseDamage + (towerLevel * stats.upgradeDamage), AmmoTypes.AMMO_TOWER_AOE_DAMAGE);
						}
						else {
							DI_Events.EventCenter<Entity, Entity, float, AmmoTypes>.invoke("OnHit", target, this, stats.baseDamage + (towerLevel * stats.upgradeDamage), AmmoTypes.AMMO_TOWER_AOE_SLOW);
						}

						float statusRoll = UnityEngine.Random.Range(0, 100);

						if (stats.canBurn && (stats.baseBurnChance + (stats.upgradeBurnChance * towerLevel)) > statusRoll) {
							DI_Events.EventCenter<Entity, Entity, float, float>.invoke("OnBurn",
							                                                           target, 
							                                                           this, 
							                                                           (stats.baseBurnDamage + (stats.upgradeBurnDamage * towerLevel)),
							                                                           (stats.baseBurnDuration + (stats.upgradeBurnDuration * towerLevel))
							                                                           );
						}

						if (stats.canPoison && (stats.basePoisonChance + (stats.upgradePoisonChance * towerLevel)) > statusRoll) {
							DI_Events.EventCenter<Entity, Entity, float, float>.invoke("OnPoison",
							                                                           target, 
							                                                           this, 
							                                                           (stats.basePoisonDamage + (stats.upgradePoisonDamage * towerLevel)),
							                                                           (stats.basePoisonDuration + (stats.upgradePoisonDuration * towerLevel))
							                                                           );
						}

						if (stats.canChill && (stats.baseChillChance + (stats.upgradeChillChance * towerLevel)) > statusRoll) {
							DI_Events.EventCenter<Entity, Entity, float, float>.invoke("OnChill",
							                                                           target, 
							                                                           this, 
							                                                           (stats.baseChillDuration + (stats.upgradeChillDuration * towerLevel)),
							                                                           (stats.baseChillSlow + (stats.upgradeChillSlow * towerLevel))
							                                                           );
						}

						if (stats.canFreeze && (stats.baseFreezeChance + (stats.upgradeFreezeChance * towerLevel)) > statusRoll) {
							DI_Events.EventCenter<Entity, Entity, float>.invoke("OnFreeze",
							                                                           target, 
							                                                           this,
							                                                           (stats.baseFreezeDuration + (stats.upgradeFreezeDuration * towerLevel))
							                                                           );
						}
					}
				}
			}
		}
		DI_Events.EventCenter<AudioClip, float, Vector3>.invoke("OnPlayEffectAtPoint", towerSFX, towerSFXVolume, this.transform.position);
		yield return new WaitForSeconds(stats.baseSpeed + (towerLevel * stats.upgradeSpeed));
		isAttacking = false;
	}
}