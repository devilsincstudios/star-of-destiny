// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;

[RequireComponent(typeof(SphereCollider))]
[AddComponentMenu("AI/Towers/Targeted Tower")]
public class TargetedTower : Tower
{
	[Header("Target Settings")]
	public Enemy target;
	public bool hasTarget = false;
	public List<Enemy> possibleTargets;
	[Header("Tower Settings")]
	public GameObject turret;
	public Transform projectileSpawnPoint;
	public GameObject projectilePrefab;
	public LineRenderer laserSight;
	[Header("Star Settings")]
	public GameObject star;
	[Header("Effects Settings")]
	public GameObject shotEffectsObject;
	private CharacterController targetController;

	new public void OnEnable()
	{
		star = GameObject.Find("Star_01");
		base.OnEnable();
		laserSight.material.color = stats.towerColor;
		DI_Events.EventCenter<Entity, Entity>.addListener("OnDeath", handleKillEnemy);
	}

	new public void OnDisable()
	{
		DI_Events.EventCenter<Entity, Entity>.removeListener("OnDeath", handleKillEnemy);
		base.OnDisable();
	}

	public void handleKillEnemy(Entity victim, Entity attacker)
	{
		if (victim == (Entity) target) {
			if (possibleTargets.Contains(target)) {
				possibleTargets.Remove(target);
			}
			target = null;
			hasTarget = false;
			laserSight.enabled = false;
		}

		if (victim.tag == "Enemy") {
			Enemy enemy = victim.GetComponent<Enemy>();
			if (possibleTargets.Contains(enemy)) {
				possibleTargets.Remove(enemy);
			}
		}
	}

	public void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Enemy" && other.name == "Hit Box") {
			if (other.gameObject.activeInHierarchy) {
				Enemy targetInfo = other.transform.parent.GetComponent<Enemy>();
				if (!targetInfo.isDead) {
					if (!possibleTargets.Contains(targetInfo)) {
						if (!stats.canTargetAir && targetInfo.type == Enemies.SNOWMAN_FLYINGMAN) {
						}
						else {
							possibleTargets.Add(targetInfo);
						}
					}
				}
			}
		}
	}

	public void OnTriggerStay(Collider other)
	{
		if (other.tag == "Enemy" && other.name == "Hit Box") {
			if (other.gameObject.activeInHierarchy) {
				Enemy targetInfo = other.transform.parent.GetComponent<Enemy>();
				if (!targetInfo.isDead) {
					if (!possibleTargets.Contains(targetInfo)) {
						if (!stats.canTargetAir && targetInfo.type == Enemies.SNOWMAN_FLYINGMAN) {
						}
						else {
							possibleTargets.Add(targetInfo);
						}
					}
				}
			}
		}
	}
	
	public void OnTriggerExit(Collider other)
	{
		if (other.tag == "Enemy" && other.name == "Hit Box") {
			Enemy targetInfo = other.transform.parent.GetComponent<Enemy>();
			possibleTargets.Remove(targetInfo);
			if (target == targetInfo) {
				hasTarget = false;
				target = null;
				laserSight.enabled = false;
			}
		}
	}

	public void LateUpdate()
	{
		if (owner.playerState.gsc.gameState == GameStates.PLAYING) {
			if (hasTarget) {
				if (target != null) {
					if (target.gameObject.activeInHierarchy) {
						if (!target.isDead) {
							turret.transform.LookAt(target.transform.TransformPoint(targetController.center));
							UpdateLaserSight(laserSight, target.transform.TransformPoint(targetController.center));
							if (!isAttacking) {
								StartCoroutine("Attack");
							}
						}
						else {
							hasTarget = false;
							laserSight.enabled = false;
						}
					}
					else {
						hasTarget = false;
						laserSight.enabled = false;
					}
				}
				else {
					hasTarget = false;
					laserSight.enabled = false;
				}
			}

			if (!hasTarget) {
				float bestTarget = 1000.0f;
				foreach (Enemy potentialTarget in possibleTargets.ToArray()) {
					if (potentialTarget != null) {
						if (potentialTarget.gameObject.activeInHierarchy) {
							float distanceToStar;
							// A Tankman is always selected if its available.
							if (potentialTarget.type == Enemies.SNOWMAN_TANKMAN) {
								distanceToStar = 0.0f;
							}
							else {
								distanceToStar = Vector3.Distance(potentialTarget.transform.position, star.transform.position);
							}
							if (distanceToStar < bestTarget) {
								target = potentialTarget;
								bestTarget = distanceToStar;
								hasTarget = true;
								laserSight.enabled = true;
							}
						}
					}
					else {
						possibleTargets.Remove(potentialTarget);
					}
				}
				if (hasTarget) {
					targetController = target.GetComponent<CharacterController>();
				}
			}
		}
	}

	public IEnumerator Attack()
	{
		isAttacking = true;
		if (hasTarget) {
			if (target != null) {
				if (target.gameObject.activeInHierarchy) {
					GameObject projectile = PoolController.instance.getPooledObject(projectilePrefab.name);
					projectile.SetActive(true);
					ProjectileController projectileController = projectile.GetComponent<ProjectileController>();
					projectile.transform.position = projectileSpawnPoint.position;
					projectileController.owner = this;
					if (projectileController.ammoType == AmmoTypes.AMMO_TOWER_CANNON || projectileController.ammoType == AmmoTypes.AMMO_TOWER_ROCKET) {
						AOEDamage aoeScript = projectile.GetComponent<AOEDamage>();
						aoeScript.damage = stats.baseDamage + (towerLevel * stats.upgradeDamage);
						aoeScript.owner = (Entity)this;

						// Burn Effects
						aoeScript.canBurn = stats.canBurn;
						aoeScript.burnChance = stats.baseBurnChance + (towerLevel * stats.upgradeBurnChance);
						aoeScript.burnDamage = stats.baseBurnDamage + (towerLevel * stats.upgradeBurnDamage);
						aoeScript.burnDuration = stats.baseBurnDuration + (towerLevel * stats.upgradeBurnDuration);
						
						// Chill Effects
						aoeScript.canChill = stats.canChill;
						aoeScript.chillChance = stats.baseChillChance + (towerLevel * stats.upgradeChillChance);
						aoeScript.chillSlow = stats.baseChillSlow + (towerLevel * stats.upgradeChillSlow);
						aoeScript.chillDuration = stats.baseChillDuration + (towerLevel * stats.upgradeChillDuration);
						
						//Freeze Effects
						aoeScript.canFreeze = stats.canFreeze;
						aoeScript.freezeChance = stats.baseFreezeChance + (towerLevel * stats.upgradeFreezeChance);
						aoeScript.freezeDuration = stats.baseFreezeDuration + (towerLevel * stats.baseFreezeDuration);
						
						// Poison Effects
						aoeScript.canPoison = stats.canPoison;
						aoeScript.poisonChance = stats.basePoisonChance + (towerLevel * stats.upgradePoisonChance);
						aoeScript.poisonDamage = stats.basePoisonDamage + (towerLevel * stats.upgradePoisonDamage);
						aoeScript.poisonDuration = stats.basePoisonDuration + (towerLevel * stats.upgradePoisonDuration);

					}

					// Burn Effects
					projectileController.canBurn = stats.canBurn;
					projectileController.burnChance = stats.baseBurnChance + (towerLevel * stats.upgradeBurnChance);
					projectileController.burnDamage = stats.baseBurnDamage + (towerLevel * stats.upgradeBurnDamage);
					projectileController.burnDuration = stats.baseBurnDuration + (towerLevel * stats.upgradeBurnDuration);

					// Chill Effects
					projectileController.canChill = stats.canChill;
					projectileController.chillChance = stats.baseChillChance + (towerLevel * stats.upgradeChillChance);
					projectileController.chillSlow = stats.baseChillSlow + (towerLevel * stats.upgradeChillSlow);
					projectileController.chillDuration = stats.baseChillDuration + (towerLevel * stats.upgradeChillDuration);

					//Freeze Effects
					projectileController.canFreeze = stats.canFreeze;
					projectileController.freezeChance = stats.baseFreezeChance + (towerLevel * stats.upgradeFreezeChance);
					projectileController.freezeDuration = stats.baseFreezeDuration + (towerLevel * stats.baseFreezeDuration);

					// Poison Effects
					projectileController.canPoison = stats.canPoison;
					projectileController.poisonChance = stats.basePoisonChance + (towerLevel * stats.upgradePoisonChance);
					projectileController.poisonDamage = stats.basePoisonDamage + (towerLevel * stats.upgradePoisonDamage);
					projectileController.poisonDuration = stats.basePoisonDuration + (towerLevel * stats.upgradePoisonDuration);


					projectileController.pointReduction = stats.baseDamage + (towerLevel * stats.upgradeDamage);
					projectile.transform.LookAt(target.transform.TransformPoint(targetController.center));
					projectile.GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * projectileController.projectileSpeed);
				}
				else {
					hasTarget = false;
					laserSight.enabled = false;
					possibleTargets.Remove(target);
				}
			}
		}
		shotEffectsObject.SetActive(true);
		DI_Events.EventCenter<AudioClip, float, Vector3>.invoke("OnPlayEffectAtPoint", towerSFX, towerSFXVolume, this.transform.position);
		yield return new WaitForSeconds(stats.baseSpeed + (towerLevel * stats.upgradeSpeed));
		shotEffectsObject.SetActive(false);

		// Pick a new target after each shot.
		laserSight.enabled = false;
		target = null;
		hasTarget = false;

		isAttacking = false;
	}
}