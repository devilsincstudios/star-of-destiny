// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;

[RequireComponent(typeof(SphereCollider))]
[AddComponentMenu("AI/Towers/Base Tower")]

public class Tower : Entity
{
	[Header("XP Settings")]
	public float upgradeLevel;
	public int towerLevel = 1;
	public float towerXp = 0.0f;
	public float xpToNextLevel = 0.0f;
	public int kills = 0;
	[Header("GUI Settings")]
	public Image xpBar;
	[Header("Tower Stats Settings")]
	[HideInInspector]
	public PlayerEntity owner;
	[HideInInspector]
//	public float sellValue = 0.0f;
//	public float cost = 0.0f;
	[Header("Slow Effect Settings")]
//	public bool slowingTower = false;
//	public float slowPercent = 0.0f;
//	public float slowDuration = 0.0f;
	[Header("Damage Settings")]
//	public float damage = 0.0f;
//	public float attackSpeed = 0.0f;
//	public bool canTargetAir = true;
	public bool isAttacking = false;
	[Header("Tower Settings")]
	public TowerStats stats;
	public TowerTypes type;
	public TowerGrid gridSpace;
	[Header("Effect Settings")]
	public AudioClip towerSFX;
	public float towerSFXVolume;

	new public void OnEnable()
	{
		owner = GameObject.Find("Player One").GetComponent<PlayerEntity>();
		base.OnEnable();
		DI_Events.EventCenter<float, Entity>.addListener("OnTowerDamage", handleOnTowerDamage);
		xpToNextLevel = 200 * towerLevel;
		xpBar.fillAmount = towerXp / xpToNextLevel;
		DI_Events.EventCenter<Entity, Entity>.addListener("OnTowerKill", handleOnTowerKill);
		// Deregister the base classes instance of onHit.
		DI_Events.EventCenter<Entity, Entity, float, AmmoTypes>.removeListener("OnHit", base.handleOnHit);
		// Use our own instance.
		DI_Events.EventCenter<Entity, Entity, float, AmmoTypes>.addListener("OnHit", handleOnHit);
	}

	new public void handleOnHit(Entity target, Entity attacker, float damage, AmmoTypes ammoType)
	{
		if (target == this) {
			if (!isDead) {
				if (!isImmortal) {
					health -= damage;
					if (healthBar != null) {
						healthBar.fillAmount = health / maxHealth;
					}
					if (health <= 0.0f) {
						this.OnDeath(attacker);
					}
				}
			}
		}
	}

	public void handleOnTowerKill(Entity target, Entity attacker)
	{
		if (attacker == (Entity)this) {
			kills++;
		}
	}

	new public void OnDeath(Entity attacker)
	{
		Debug.Log(System.Reflection.MethodBase.GetCurrentMethod().Name);
		GameStateController.instance.playerState.activeTowers -= 1;
		DI_Events.EventCenter<PlayerState>.invoke("OnUpdateHudRequest", GameStateController.instance.playerState);
		gridSpace.hasTower = false;
		gridSpace.tower = TowerTypes.TOWER_NONE;
		gridSpace.validPlacement = true;

		base.OnDeath(attacker);
	}

	new public void OnDisable()
	{
		base.OnDisable();

		DI_Events.EventCenter<float, Entity>.removeListener("OnTowerDamage", handleOnTowerDamage);
		DI_Events.EventCenter<Entity, Entity>.removeListener("OnTowerKill", handleOnTowerKill);
		DI_Events.EventCenter<Entity, Entity, float, AmmoTypes>.removeListener("OnHit", handleOnHit);
	}

	public void handleOnTowerDamage(float damage, Entity attacker)
	{
		if (attacker == (Entity)this) {
			float xpGained = 1 * stats.baseSpeed + (towerLevel * stats.upgradeSpeed);

			// Split shot penalty
			if (type == TowerTypes.TOWER_LASER || type == TowerTypes.TOWER_SPLIT) {
				xpGained /= 2;
			}

			// AOE XP Gain Penalty
			if (type == TowerTypes.TOWER_ROCKET) {
				xpGained /= 5;
			}

			#if DEBUG
				Debug.Log("Tower: " + this.name + " has gained: " + xpGained);
			#endif
			addXp(xpGained);
		}
	}

	public void addXp(float amount)
	{
		if (towerLevel < 10) {
			towerXp += amount;
			if (towerXp >= xpToNextLevel) {
				towerXp -= xpToNextLevel;
				towerLevel++;
				//TODO GUI something for this
				#if DEBUG
					Debug.Log("Tower: " + this.name + " has reached level: " + towerLevel);
				#endif

//				switch (type) {
//					case TowerTypes.TOWER_AOE_DAMAGE:
//						attackSpeed -= 0.025f;
//						damage += 0.5f;
//					break;
//					case TowerTypes.TOWER_AOE_SLOW:
//						if (slowPercent < 50.0f) {
//							slowPercent += 10;
//						}
//						slowDuration += 0.25f;
//					break;
//					case TowerTypes.TOWER_BASIC:
//						attackSpeed -= 0.05f;
//						damage += 10.0f;
//					break;
//					case TowerTypes.TOWER_CANNON:
//						damage += 10.0f;
//						attackSpeed -= 1.0f;
//					break;
//					case TowerTypes.TOWER_LASER:
//						damage += 0.5f;
//					break;
//					case TowerTypes.TOWER_ROCKET:
//						damage += 10.0f;
//						attackSpeed -= 0.25f;
//					break;
//					case TowerTypes.TOWER_SPLIT:
//						damage += 7.5f;
//					break;
//					case TowerTypes.TOWER_SNIPER:
//						damage += 50.0f;
//						attackSpeed -= 1.0f;
//					break;
//				}
				xpToNextLevel = 200 * towerLevel;
			}
			xpBar.fillAmount = towerXp / xpToNextLevel;
		}
	}

	public void UpdateLaserSight(LineRenderer laser, Vector3 target)
	{
		laser.SetPosition(0, laser.transform.position);
		laser.SetPosition(1, target);
	}

	public float getXp()
	{
		return towerXp;
	}

	public int getLevel()
	{
		return towerLevel;
	}
}
