// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System;

[AddComponentMenu("AI/Towers/Tower Grid")]
public class TowerGrid : MonoBehaviour
{
	public bool validPlacement = true;
	public bool hasTower;
	public TowerTypes tower;
	public PrefabLoader prefabLoader;
	public BoxCollider boxCollider;
	public Material towerGridBase;
	public Material towerGridValid;
	public Material towerGridInvalid;
	public Material towerGridHighlighted;
	public bool selected = false;
	public PlayerState playerState;
	public float maxDistance = 3.0f;
	private float lastHit = 0.0f;
	public bool highlighted = false;
	private float distanceToPlayer;
	public GameObject infoScreen;

	public void OnEnable()
	{
		prefabLoader = GameObject.Find("Game State").GetComponent<PrefabLoader>();
		playerState = GameObject.Find("Player One").GetComponent<PlayerState>();

		boxCollider = GetComponent<BoxCollider>();
		boxCollider.enabled = false;
		GetComponent<Renderer>().enabled = false;

		if (hasTower || !validPlacement) {
			GetComponent<Renderer>().material = towerGridInvalid;
		}
	}

	public void OnDisable()
	{
		highlighted = false;
		infoScreen.SetActive(false);
		selected = false;
	}

	public void OnRayHit()
	{
		if (this.enabled) {
			if (!highlighted) {
				float distance = Vector3.Distance(Camera.main.transform.position, transform.position);
				if (distance <= maxDistance) {
					highlighted = true;
					infoScreen.SetActive(true);
					updateHighlight();
				}
			}
			lastHit = 0.0f;
		}
		else {
			highlighted = false;
			updateHighlight();
			infoScreen.SetActive(false);
		}
	}

	public void Update()
	{
		if (playerState.isPlacingTowers) {
			distanceToPlayer = Vector3.Distance(this.transform.position, playerState.gameObject.transform.position);
			if (distanceToPlayer > 3) {
				boxCollider.enabled = false;
				GetComponent<Renderer>().enabled = false;
			}
			else {
				boxCollider.enabled = true;
				GetComponent<Renderer>().enabled = true;
			}

			if (boxCollider.enabled) {
				if (highlighted) {
					if (lastHit < 0.1f) {
						lastHit += Time.deltaTime;
					}
					else {
						highlighted = false;
						infoScreen.SetActive(false);
						updateHighlight();
					}
				}
				if (highlighted) {
					if (Input.GetButtonDown("Tower Select")) {
						if (!selected) {
							playerState.selectGrid(this);
						}
						else {
							if (validPlacement && !hasTower) {
								buildTower();
							}
						}
					}
				}
				else {
					if (Input.GetKeyDown(KeyCode.Escape)) {
						if (playerState.getHighlightedGrid() == this) {
							playerState.unHighlightGrid();
						}
						if (playerState.getSelectedGrid() == this) {
							playerState.deselectGrid();
						}
						playerState.isPlacingTowers = false;
						DI_Events.EventCenter.invoke("onDisableGrids");
					}
					 if (Input.GetMouseButtonDown(0)) {
						if (playerState.getHighlightedGrid() == this) {
							playerState.unHighlightGrid();
						}
						if (playerState.getSelectedGrid() == this) {
							playerState.deselectGrid();
						}
						playerState.isPlacingTowers = false;
						DI_Events.EventCenter.invoke("onDisableGrids");
					}
				}
			}
		}
	}

	public void updateHighlight()
	{
		if (!selected) {
			if (highlighted)
			{
				GetComponent<Renderer>().material = towerGridHighlighted;
				playerState.highlightGrid(this);
			}
			else {
				enableGrid();
				if (playerState.getHighlightedGrid() == this) {
					playerState.unHighlightGrid();
				}
			}
		}
	}

	public void enableGrid()
	{
		if (hasTower || !validPlacement) {
			GetComponent<Renderer>().material = towerGridInvalid;
		}
		else {
			GetComponent<Renderer>().material = towerGridValid;
		}
		GetComponent<Renderer>().enabled = true;
		GetComponent<Collider>().enabled = true;
	}

	public void disableGrid()
	{
		GetComponent<Renderer>().enabled = false;
		GetComponent<Collider>().enabled = false;
		hideShadowTower();
	}

	public void selectGrid()
	{
		if (selected) {
			if (validPlacement && !hasTower) {
				buildTower();
			}
		}
		else {
			hideShadowTower();
			selected = true;
			if (validPlacement && !hasTower) {
				GetComponent<Renderer>().material = towerGridBase;
				showShadowTower();
			}
		}
	}

	public void deselectGrid()
	{
		hideShadowTower();
		if (validPlacement && !hasTower) {
			GetComponent<Renderer>().material = towerGridValid;
		}
		else {
			GetComponent<Renderer>().material = towerGridInvalid;
		}
	}

	public void hideShadowTower()
	{
		try {
			for (int index = 0; index < transform.childCount; index++) {
				if (transform.GetChild(index).name != infoScreen.name) {
					Destroy(transform.GetChild(index).gameObject);
				}
			}
		}
		catch (Exception error)
		{
			Debug.LogException(error);
		}
	}

	public void showShadowTower() {
		hideShadowTower();
		GameObject prefab = prefabLoader.getPrefab("Shadow " + playerState.selectedTowerType.ToString());
		if (prefab != null) {
			GameObject tower = (GameObject) Instantiate(prefab);
			tower.transform.parent = this.transform;
			tower.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
		}
	}

	public void sellTower()
	{
		hasTower = false;
		validPlacement = true;
	}

	public void buildTower()
	{
		if (playerState.activeTowers < playerState.maxTowers) {
			GameObject prefab = prefabLoader.getPrefab(playerState.selectedTowerType.ToString());
			if (prefab != null) {
				hideShadowTower();
				GameObject tower = (GameObject) Instantiate(prefab);
				Tower towerScript = tower.GetComponent<Tower>();
				if (playerState.getCoins() >= towerScript.stats.baseCost) {
					tower.transform.parent = this.transform;
					tower.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
					tower.transform.parent = GameObject.Find("Towers").transform;
					hasTower = true;
					towerScript.gridSpace = this;
					playerState.activeTowers += 1;
					playerState.removeCoins(playerState.getTowerCost(playerState.selectedTowerType));
					DI_Events.EventCenter<TowerTypes>.invoke("OnBuildTower", playerState.selectedTowerType);
					DI_Events.EventCenter<int, PlayerState>.invoke("OnSpendCoins", (int)playerState.getTowerCost(playerState.selectedTowerType), playerState);
					playerState.isPlacingTowers = false;
					DI_Events.EventCenter.invoke("onDisableGrids");
				}
				else {
					//Can't afford tower
					DI_Events.EventCenter<string, string, float>.invoke("OnDisplayNotification", "Not enough coins!", "You need more coins to place this tower", 5.0f);
					Destroy(tower);
					playerState.isPlacingTowers = false;
					DI_Events.EventCenter.invoke("onDisableGrids");
				}
			}
		}
		else {
			DI_Events.EventCenter<string, string, float>.invoke("OnDisplayNotification", "Tower Limit Reached!", "You will need to sell another tower before you can place this.", 5.0f);
		}
	}
}
