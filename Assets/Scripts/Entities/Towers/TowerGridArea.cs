// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System;

[RequireComponent(typeof(DI_Game.EditorOnlyObject))]
[RequireComponent(typeof(BoxCollider))]
[AddComponentMenu("AI/Towers/Tower Grid Area")]
public class TowerGridArea : MonoBehaviour
{
	public GameObject gridArea;
	public bool gridVisable;

	public void OnEnable()
	{
		DI_Events.EventCenter.addListener("onDisableGrids", handleDisableGrids);
		DI_Events.EventCenter.addListener("onEnableGrids", handleEnableGrids);
	}

	public void OnDisable()
	{
		DI_Events.EventCenter.removeListener("onDisableGrids", handleDisableGrids);
		DI_Events.EventCenter.removeListener("onEnableGrids", handleEnableGrids);
	}

	public void handleEnableGrids()
	{
		setGridState(gridVisable);
	}

	public void handleDisableGrids()
	{
		if (gridVisable == true) {
			setGridState(false);
		}
	}

	public void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player") {
			if (gridVisable == false) {
				gridVisable = true;
				setGridState(true);
			}
		}
	}

	public void OnTriggerExit(Collider other)
	{
		if (other.tag == "Player") {
			if (gridVisable == true) {
				gridVisable = false;
				setGridState(false);
			}
		}
	}

	public void setGridState(bool gridEnabled)
	{
		PlayerState playerState = GameObject.Find("Player One").GetComponent<PlayerState>();
		if (playerState.isPlacingTowers || gridEnabled == false) {
			for (int index = 0; index < gridArea.transform.childCount; ++index) {
				Transform row = gridArea.transform.GetChild(index);
				for (int subIndex = 0; subIndex < row.transform.childCount; ++subIndex) {
					try {
						GameObject gridNode = row.transform.GetChild(subIndex).gameObject;
						if (gridEnabled) {
							gridNode.transform.GetChild(0).gameObject.SetActive(true);
							gridNode.GetComponentInChildren<TowerGrid>().enableGrid();
						}
						else {
							gridNode.GetComponentInChildren<TowerGrid>().disableGrid();
							gridNode.transform.GetChild(0).gameObject.SetActive(false);
						}
					}
					catch (Exception)
					{
					}
				}
			}
		}
	}
}
