// Devils Inc Studios
// // Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using UnityEngine.UI;

[AddComponentMenu("AI/Towers/Tower Info")]
public class TowerInfo : MonoBehaviour
{
	public Tower tower;
	public GameStateController gsc;
	public HighlightObject highlightObject;

	public void OnEnable()
	{
		gsc = GameObject.Find("Game State").GetComponent<GameStateController>();
		highlightObject = GetComponent<HighlightObject>();
	}

	public void Update()
	{
		if (highlightObject.highlighted) {
			if (Input.GetButtonDown("Tower Select")) {
				DI_Events.EventCenter<Tower>.invoke("OnSelectTower", tower);
			}
		}
	}
}
