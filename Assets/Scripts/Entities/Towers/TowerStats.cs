// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014
//
// TODO: Include a description of the file here.
//

using UnityEngine;

public class TowerStats : ScriptableObject
{
	[Header("Tower Settings")]
	public TowerTypes type;
	public Color towerColor;
	[Multiline]
	public string description;

	[Header("Tower Properties")]
	public bool canBurn;
	public bool canChill;
	public bool canFreeze;
	public bool canPoison;
	public bool canTargetAir;

	[Header("Base Stats")]
	public float baseSpeed;
	public float baseDamage;
	public float baseHealth;
	public float baseCost;

	[Header("Base Effects: Burn")]
	public float baseBurnChance;
	public float baseBurnDuration;
	public float baseBurnDamage;

	[Header("Base Effects: Freeze")]
	public float baseFreezeChance;
	public float baseFreezeDuration;

	[Header("Base Effects: Chill")]
	public float baseChillChance;
	public float baseChillDuration;
	public float baseChillSlow;

	[Header("Base Effects: Poison")]
	public float basePoisonChance;
	public float basePoisonDuration;
	public float basePoisonDamage;

	[Header("Upgraded Stats")]
	public float upgradeSpeed;
	public float upgradeDamage;
	public float upgradeHealth;
	public float upgradeCost;

	[Header("Upgraded Effects: Burn")]
	public float upgradeBurnChance;
	public float upgradeBurnDuration;
	public float upgradeBurnDamage;

	[Header("Upgraded Effects: Freeze")]
	public float upgradeFreezeChance;
	public float upgradeFreezeDuration;

	[Header("Upgraded Effects: Chill")]
	public float upgradeChillChance;
	public float upgradeChillDuration;
	public float upgradeChillSlow;

	[Header("Upgraded Effects: Poison")]
	public float upgradePoisonChance;
	public float upgradePoisonDuration;
	public float upgradePoisonDamage;
}