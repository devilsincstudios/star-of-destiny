// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

#region Includes
using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.UI;
#endregion

[AddComponentMenu("Game State/Hud Controller")]
public class HudController : MonoBehaviour
{
	#region Public Variables
	public List<HudElements> hudElements;
	public Sprite hudSelected;
	public Sprite hudNormal;
	#endregion

	#region References
	public ScoreController scoreController;
	public WaveController waveController;
	#endregion

	#region Public Methods
	public void OnEnable()
	{
		scoreController = GameObject.Find("Game State").GetComponent<ScoreController>();
		waveController = GameObject.Find("Game State").GetComponent<WaveController>();
		DI_Events.EventCenter<PlayerState>.addListener("OnUpdateHudRequest", handleUpdateHudRequest);
	}

	public void OnDisable()
	{
		DI_Events.EventCenter<PlayerState>.removeListener("OnUpdateHudRequest", handleUpdateHudRequest);
	}

	#endregion

	#region Private Methods
	/// <summary>
	/// Updates the selected weapon.
	/// </summary>
	/// <param name="playerState">Player state.</param>
	private void updateSelected(PlayerState playerState)
	{
		hudElements[(playerState.player - 1)].slot1.sprite = hudNormal;
		hudElements[(playerState.player - 1)].slot2.sprite = hudNormal;
		hudElements[(playerState.player - 1)].slot3.sprite = hudNormal;
		hudElements[(playerState.player - 1)].slot4.sprite = hudNormal;
		hudElements[(playerState.player - 1)].slot5.sprite = hudNormal;
		hudElements[(playerState.player - 1)].slot6.sprite = hudNormal;
		hudElements[(playerState.player - 1)].slot7.sprite = hudNormal;

		switch (playerState.getSelectedAmmoType()) {
			case AmmoTypes.AMMO_SNOWBALL:
				hudElements[(playerState.player - 1)].slot1.sprite = hudSelected;
			break;
			case AmmoTypes.AMMO_CANDY_CANE:
				hudElements[(playerState.player - 1)].slot2.sprite = hudSelected;
			break;
			case AmmoTypes.AMMO_ICICLE:
				hudElements[(playerState.player - 1)].slot3.sprite = hudSelected;
			break;
			case AmmoTypes.AMMO_STEAM:
				hudElements[(playerState.player - 1)].slot4.sprite = hudSelected;
			break;
			case AmmoTypes.AMMO_BAUBLE_GRENADE:
				hudElements[(playerState.player - 1)].slot5.sprite = hudSelected;
			break;
			case AmmoTypes.AMMO_PRESENT_BOMB:
				hudElements[(playerState.player - 1)].slot6.sprite = hudSelected;
			break;
			case AmmoTypes.AMMO_SANTA_BOMBER:
				hudElements[(playerState.player - 1)].slot7.sprite = hudSelected;
			break;
		}
	}

	/// <summary>
	/// Updates the hud.
	/// </summary>
	/// <param name="playerState">Player state.</param>
	private void updateHud(PlayerState playerState)
	{
		hudElements[(playerState.player - 1)].score.text = "Score: " + scoreController.getFinalScore(playerState.player);
		hudElements[(playerState.player - 1)].coins.text = "Coins: " + playerState.getCoins();
		hudElements[(playerState.player - 1)].wave.text = "Wave: " + waveController.currentWave;
		hudElements[(playerState.player - 1)].enemies.text = "Enemies: " + waveController.getEnemyCount();
		hudElements[(playerState.player - 1)].candyCaneAmmo.text = playerState.getAmmo(AmmoTypes.AMMO_CANDY_CANE).ToString();
		hudElements[(playerState.player - 1)].icicleAmmo.text = playerState.getAmmo(AmmoTypes.AMMO_ICICLE).ToString();
		hudElements[(playerState.player - 1)].snowBallAmmo.text = playerState.getAmmo(AmmoTypes.AMMO_SNOWBALL).ToString();
		hudElements[(playerState.player - 1)].steamAmmo.text = playerState.getAmmo(AmmoTypes.AMMO_STEAM).ToString();
		hudElements[(playerState.player - 1)].baubleAmmo.text = playerState.getAmmo(AmmoTypes.AMMO_BAUBLE_GRENADE).ToString();
		hudElements[(playerState.player - 1)].presentAmmo.text = playerState.getAmmo(AmmoTypes.AMMO_PRESENT_BOMB).ToString();
		hudElements[(playerState.player - 1)].santaBomberAmmo.text = playerState.getAmmo(AmmoTypes.AMMO_SANTA_BOMBER).ToString();
		hudElements[(playerState.player - 1)].snowCollectorPanel.SetActive(playerState.hasSnowCollector());
		if (playerState.activeTowers > 0) {
			hudElements[(playerState.player - 1)].towerLimitBackground.SetActive(true);
			hudElements[(playerState.player - 1)].towerLimitText.text = "Towers: " + playerState.activeTowers + "/" + playerState.maxTowers;
		}
		else {
			hudElements[(playerState.player - 1)].towerLimitBackground.SetActive(false);
		}

//		if (playerState.selectedTowerType != TowerTypes.TOWER_NONE) {
//			hudElements[(playerState.player - 1)].towerSelectionPanel.SetActive(true);
//			Color towerColor = Color.black;
//			switch (playerState.selectedTowerType) {
//				case TowerTypes.TOWER_AOE_DAMAGE:
//					towerColor = Color.red;
//				break;
//				case TowerTypes.TOWER_AOE_SLOW:
//					towerColor = Color.blue;
//				break;
//				case TowerTypes.TOWER_BASIC:
//					towerColor = Color.green;
//				break;
//				case TowerTypes.TOWER_CANNON:
//					towerColor = new Color(1.0f, 0.7f, 0.0f);
//				break;
//			}
//			hudElements[(playerState.player - 1)].towerSelectionIcon.color = towerColor;
//		}
	}

	/// <summary>
	/// Handles the update hud request.
	/// </summary>
	/// <param name="playerState">Player state.</param>
	private void handleUpdateHudRequest(PlayerState playerState)
	{
		if ((playerState.player - 1) < hudElements.Count) {
			updateHud(playerState);
			updateSelected(playerState);
		}
		else {
			#if DEBUG
				Debug.Log("Requested to update hud for player: " + playerState.player + " but that player does not have hud elements configured.");
			#endif
		}
	}
	#endregion
}
