// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

#region Includes
using UnityEngine;
using UnityEngine.UI;
using System;
#endregion

[Serializable]
public struct HudElements
{
	#region Public variables
	public Text score;
	public Text coins;
	public Text wave;
	public Text enemies;
	
	public Text snowBallAmmo;
	public Text candyCaneAmmo;
	public Text icicleAmmo;
	public Text steamAmmo;
	public Text baubleAmmo;
	public Text presentAmmo;
	public Text santaBomberAmmo;
	public Text towerLimitText;
	public GameObject towerLimitBackground;
	//public Image towerSelectionIcon;

	public GameObject snowCollectorPanel;
	//public GameObject towerSelectionPanel;

	public UnityEngine.UI.Image slot1;
	public UnityEngine.UI.Image slot2;
	public UnityEngine.UI.Image slot3;
	public UnityEngine.UI.Image slot4;
	public UnityEngine.UI.Image slot5;
	public UnityEngine.UI.Image slot6;
	public UnityEngine.UI.Image slot7;
	#endregion
}