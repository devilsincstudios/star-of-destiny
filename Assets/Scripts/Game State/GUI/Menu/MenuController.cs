// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

using UnityEngine;

[AddComponentMenu("Game State/Menu Controller")]
public class MenuController : MonoBehaviour
{
	public GameStateController gsc;
	public Canvas menu;
	public Canvas pause;
	public Canvas options;
	public Canvas shop;
	public Canvas towerSelect;
	public Canvas hud;
	public Canvas cheatMenu;
	public Canvas towerInfo;

	public void disableAllMenus()
	{
		menu.gameObject.SetActive(false);
		shop.gameObject.SetActive(false);
		pause.gameObject.SetActive(false);
		towerSelect.gameObject.SetActive(false);
		cheatMenu.gameObject.SetActive(false);
		hud.gameObject.SetActive(true);
		towerInfo.gameObject.SetActive(false);
		options.gameObject.SetActive(false);
	}

	public void onCheatMenuEnter()
	{
		disableAllMenus();
		cheatMenu.gameObject.SetActive(true);
		gsc.enterMenu();
	}

	public void onCheatMenuExit()
	{
		disableAllMenus();
		gsc.exitMenu();
	}

	public void onTowerSelectEnter()
	{
		disableAllMenus();
		towerSelect.gameObject.SetActive(true);
		gsc.enterMenu();
	}

	public void onTowerSelectExit()
	{
		if (gsc.playerState.isPlacingTowers) {
			gsc.playerState.isPlacingTowers = false;
			DI_Events.EventCenter.invoke("onDisableGrids");
		}
		disableAllMenus();
		gsc.exitMenu();
	}

	public void onMenuEnter()
	{
		disableAllMenus();
		menu.gameObject.SetActive(true);
		gsc.enterMenu();
	}

	public void onOptionsMenuEnter()
	{
		disableAllMenus();
		options.gameObject.SetActive(true);
		gsc.enterMenu();
	}

	public void onShopMenuEnter()
	{
		disableAllMenus();
		shop.gameObject.SetActive(true);
		gsc.enterMenu();
	}

	public void onMenuExit()
	{
		disableAllMenus();
		gsc.exitMenu();
	}

	public void onPause()
	{
		disableAllMenus();
		pause.gameObject.SetActive(true);
		gsc.onPause();
	}

	public void onTowerInfoEnter()
	{
		disableAllMenus();
		towerInfo.gameObject.SetActive(true);
		gsc.enterMenu();
	}

	public void onResume()
	{
		disableAllMenus();
		gsc.onResume();
	}
}
