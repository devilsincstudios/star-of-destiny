// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014
//
// TODO: Include a description of the file here.
//

using System;
[Serializable]
public struct NotificationData
{
	public string name;
	public string text;
	public float time;
}