// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

#region Includes
using UnityEngine;
using System;
using System.Collections.Generic;
#endregion

[AddComponentMenu("Game State/Game State Controller")]
public class GameStateController : MonoBehaviour
{
	#region Public Variables
	public GameStates gameState;
	public MenuController menuController;
	public PlayerState playerState;
	public DifficultyTypes difficultyLevel;
	public static GameStateController instance;

	// Number of players
	public int players = 1;

	//TODO REMOVE ME
	public Canvas skipInfo;

	// Maximum amount of health the star has.
	public float starMaxHealth = 100.0f;
	// Current amount of health the star has.
	public float starHealth = 100.0f;

	public bool cheatStarInvincible = false;
	#endregion
	public bool shopEnabled = true;

	public void OnEnable()
	{
		Cursor.lockState = CursorLockMode.Locked;
		// TODO make this an option
		//Application.targetFrameRate = 60;
		difficultyLevel = (DifficultyTypes)PlayerPrefs.GetInt("Difficulty", 0);
	}

	/// <summary>
	/// Locks the cursor.
	/// </summary>
	public void lockCursor()
	{
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
	}

	/// <summary>
	/// Unlocks the cursor.
	/// </summary>
	public void unlockCursor()
	{
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
	}

	/// <summary>
	/// Called when a cinema starts that is unskipable
	/// </summary>
	public void enterCinemaNoSkip()
	{
		#if DEBUG
			Debug.Log("enterCinemaNoSkip");
		#endif
		gameState = GameStates.IN_CUT_SCENE;
	}

	/// <summary>
	/// Called when a cinema starts
	/// </summary>
	public void enterCinema()
	{
		#if DEBUG
			Debug.Log("enterCinema");
		#endif
		gameState = GameStates.IN_CUT_SCENE;
		skipInfo.gameObject.SetActive(true);
	}

	/// <summary>
	/// Called when the cinema exits
	/// </summary>
	public void exitCinema()
	{
		#if DEBUG
			Debug.Log("exitCinema");
		#endif
		gameState = GameStates.PLAYING;
		skipInfo.gameObject.SetActive(false);
	}

	/// <summary>
	/// Called when the cinema exits
	/// </summary>
	public void exitCinemaNoSkip()
	{
		#if DEBUG
		Debug.Log("exitCinemaNoSkip");
		#endif
		gameState = GameStates.PLAYING;
	}

	/// <summary>
	/// Called when the menu is entered
	/// </summary>
	public void enterMenu()
	{
		#if DEBUG
			Debug.Log("enterMenu");
		#endif
		unlockCursor();
		gameState = GameStates.IN_MENU;
	}

	/// <summary>
	/// Called when the menu is exited
	/// </summary>
	public void exitMenu()
	{
		#if DEBUG
			Debug.Log("exitMenu");
		#endif
		lockCursor();
		gameState = GameStates.PLAYING;
	}

	/// <summary>
	/// Called when the game is paused.
	/// </summary>
	public void onPause()
	{
		#if DEBUG
			Debug.Log("OnPause");
		#endif
		unlockCursor();
		gameState = GameStates.PAUSED;
	}

	/// <summary>
	/// Called when the game is unpaused
	/// </summary>
	public void onResume()
	{
		#if DEBUG
			Debug.Log("OnResume");
		#endif
		lockCursor();
		gameState = GameStates.PLAYING;
	}

	public void Awake()
	{
		instance = this;
	}

	public void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape)) {
			if (gameState == GameStates.IN_MENU) {
				// TODO Replace with events
				menuController.SendMessage("onMenuExit");
			}
			else if (gameState == GameStates.PLAYING) {
				// TODO Replace with events
				menuController.SendMessage("onMenuEnter");
			}
		}

		if (Input.GetKeyDown(KeyCode.Pause)) {
			if (gameState == GameStates.PAUSED) {
				// TODO Replace with events
				menuController.SendMessage("onResume");
			}
			else {
				// TODO Replace with events
				menuController.SendMessage("onPause");
			}
		}

		if (Input.GetButtonDown("Open Tower Menu")) {
			if (gameState == GameStates.IN_MENU) {
				menuController.SendMessage("onMenuExit");
			}
			else if (gameState == GameStates.PLAYING) {
				menuController.SendMessage("onTowerSelectEnter");
			}
		}

		if (shopEnabled) {
			if (Input.GetButtonDown("Open Shop Menu")) {
				if (gameState == GameStates.IN_MENU) {
					menuController.SendMessage("onMenuExit");
				}
				else if (gameState == GameStates.PLAYING) {
					menuController.SendMessage("onShopMenuEnter");
				}
			}
		}
//		if (Input.GetButtonDown("Edit Towers")) {
//			if (playerState.isPlacingTowers) {
//				playerState.isPlacingTowers = false;
//				DI_Events.EventCenter.invoke("onDisableGrids");
//			}
//			else {
//				playerState.isPlacingTowers = true;
//				DI_Events.EventCenter.invoke("onEnableGrids");
//			}
//		}
	}


	public void onCheatChangeUnlimitedAmmo(bool state)
	{
		playerState.unlimitedAmmo = state;
	}

	public void onCheatChangeStarInvincible(bool state)
	{
		cheatStarInvincible = state;
	}

	public void onCheatKillAll()
	{
		EnemyController enemyController = GetComponent<EnemyController>();
		PlayerEntity playerEntity = GameObject.Find("Player One").GetComponent<PlayerEntity>();

		foreach (Enemy enemy in enemyController.spawnedEnemies.ToArray()) {
			if (enemy != null) {
				DI_Events.EventCenter<Entity, Entity, float, AmmoTypes>.invoke("OnHit", enemy, playerEntity, enemy.maxHealth, AmmoTypes.AMMO_CHEAT);
			}
		}
	}

	public void onCheatChangeCoins(string amount)
	{
		int intAmount = 0;
		if (int.TryParse(amount, out intAmount)) {
			playerState.setCoins(intAmount);
		}
	}

	public void onCheatChangeWave(string wave)
	{
		WaveController waveController = GetComponent<WaveController>();

		int intAmount = 0;
		if (int.TryParse(wave, out intAmount)) {
			waveController.currentWave = intAmount;
		}
	}
}
