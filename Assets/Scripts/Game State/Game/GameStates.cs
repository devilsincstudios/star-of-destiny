// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

public enum GameStates
{
	// Player is in full control of the game.
	PLAYING,
	// Player is in a menu
	IN_MENU,
	// The game is paused
	PAUSED,
	// A cut scene is playing, the player has no control.
	IN_CUT_SCENE
}