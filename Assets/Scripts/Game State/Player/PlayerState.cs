// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

#region Includes
using UnityEngine;
using System.Collections.Generic;
using System;
#endregion

[RequireComponent(typeof(PlayerEntity))]
[AddComponentMenu("Game State/Player")]
public class PlayerState : MonoBehaviour
{
	
	#region Events Used
	/*
	 * Listening:
	 * OnPickupAmmo
	 * OnPickupItem
	 * 
	 * Sending:
	 * OnUpdateHudRequest
	 *
	 */
	#endregion

	#region Private Variables
	[SerializeField]
	private PlayerInventory playerInventory;
	#endregion

	#region Public Variables
	public int player;
	public PlayerStates state;

	public float playerWalkSpeedIncrease = 1;
	public float playerRunSpeedIncrease = 1;

	// Is the player trying to place towers?
	public bool isPlacingTowers = false;
	public List<TowerCost> towerCosts;
	public int activeTowers = 0;
	public int maxTowers = 30;
	public TowerTypes selectedTowerType = TowerTypes.TOWER_BASIC;
	public bool unlimitedAmmo = false;

	public List<PlayerXp> xpLevels;
	#endregion

	#region References
	[HideInInspector]
	public GameStateController gsc;
	[HideInInspector]
	public PrefabLoader prefabLoader;
	[HideInInspector]
	public PlayerEntity entity;
	public FPSController fpsController;

	public TowerGrid selectedGrid;
	public TowerGrid highlightedGrid;
	#endregion

	#region Public Methods
	public void OnEnable()
	{
		gsc = GameObject.Find("Game State").GetComponent<GameStateController>();
		prefabLoader = GameObject.Find("Game State").GetComponent<PrefabLoader>();
		entity = this.GetComponent<PlayerEntity>();
		playerInventory.hasSnowCollector = false;
		playerInventory.speedUpgradeLevel = 0;

		// Register the OnPickupAmmo event
		DI_Events.EventCenter<AmmoBox, PlayerState>.addListener("OnPickupAmmo", handlePickupAmmo);
		// Register to the OnPickupItem event.
		DI_Events.EventCenter<Item, PlayerState>.addListener("OnPickupItem", handlePickupItem);
		// Register to the onDamage event
		DI_Events.EventCenter<float, Enemies, AmmoTypes>.addListener("OnDamage", handleOnDamage);
	}

	public void OnDisable()
	{
		// Unregister the OnPickupAmmo event
		DI_Events.EventCenter<AmmoBox, PlayerState>.removeListener("OnPickupAmmo", handlePickupAmmo);
		// Unregister the OnPickupItem event
		DI_Events.EventCenter<Item, PlayerState>.removeListener("OnPickupItem", handlePickupItem);
		DI_Events.EventCenter<float, Enemies, AmmoTypes>.removeListener("OnDamage", handleOnDamage);
	}

	/// <summary>
	/// Handles the pickup of items.
	/// </summary>
	/// <param name="item">Item.</param>
	/// <param name="playerId">Player identifier.</param>
	/// <remarks>
	/// Called by items when the player enters the trigger zone.
	/// Ignores the event if the playerId does not match with our playerId
	/// Ignores items that are not coins, if we add more items we will need to update this.
	/// </remarks>
	public void handlePickupItem(Item item, PlayerState playerState)
	{
		if (playerState.player == player) {
			if (item.itemType == Items.ITEM_COIN) {
				#if DEBUG
					Debug.Log("Adding " + item.amount + "Coins!");
				#endif
				addCoins(item.amount);
			}
		}
	}

	/// <summary>
	/// Handles the pickup of ammo.
	/// </summary>
	/// <param name="ammoBox">Ammo box.</param>
	/// <param name="playerId">Player identifier.</param>
	/// <remarks>
	/// Called by ammo boxes when the player enters the trigger zone.
	/// Ignores the event if the playerId does not match with our playerId
	/// </remarks>
	public void handlePickupAmmo(AmmoBox ammoBox, PlayerState playerState)
	{
		if (playerState.player == player) {
			addAmmo(ammoBox.ammoInfo);
		}
	}

	public void handleOnDamage(float damage, Enemies victim, AmmoTypes ammoType)
	{
		#if DEBUG
		Debug.Log("OnDamage");
		#endif
		if (ammoType != AmmoTypes.AMMO_CHEAT && ammoType != AmmoTypes.AMMO_ENEMY && ammoType != AmmoTypes.AMMO_RAMBO) {
			switch (ammoType) {
				case AmmoTypes.AMMO_BAUBLE_GRENADE:
					addXp(getXpId(ammoType), 10);
				break;

				case AmmoTypes.AMMO_PRESENT_BOMB:
					addXp(getXpId(ammoType), 10);
				break;

				case AmmoTypes.AMMO_SANTA_BOMBER:
					addXp(getXpId(ammoType), 10);
				break;

				case AmmoTypes.AMMO_STEAM:
					addXp(getXpId(ammoType), 0.01f);
				break;

				default:
					addXp(getXpId(ammoType), 1);
				break;
			}
		}
	}

	/// <summary>
	/// Sets the coins the player has.
	/// </summary>
	/// <param name="amount">Amount.</param>
	public void setCoins(float amount)
	{
		playerInventory.coins = amount;
		DI_Events.EventCenter<PlayerState>.invoke("OnUpdateHudRequest", this);
	}

	/// <summary>
	/// Adds coins to the player.
	/// </summary>
	/// <param name="amount">Amount.</param>
	public void addCoins(float amount)
	{
		playerInventory.coins += amount;
		DI_Events.EventCenter<PlayerState>.invoke("OnUpdateHudRequest", this);
	}

	/// <summary>
	/// Removes coins from the player.
	/// </summary>
	/// <param name="amount">Amount.</param>
	public void removeCoins(float amount)
	{
		playerInventory.coins -= amount;
		DI_Events.EventCenter<PlayerState>.invoke("OnUpdateHudRequest", this);
	}

	/// <summary>
	/// Gets the amount of coins the player currently has.
	/// </summary>
	/// <returns>The coins.</returns>
	public float getCoins()
	{
		return playerInventory.coins;
	}

	/// <summary>
	/// Gets the max ammo.
	/// </summary>
	/// <returns>The max ammo.</returns>
	/// <param name="type">Type.</param>
	public float getMaxAmmo(AmmoTypes type)
	{
		foreach (Ammo ammo in playerInventory.maxAmmo.ToArray()) {
			if (ammo.type == type) {
				return ammo.amount;
			}
		}
		#if DEBUG
			Debug.LogError("Max ammo amount requested on type that does not exist.");
		#endif
		return 0;
	}

	/// <summary>
	/// Gets the ammo type identifier.
	/// </summary>
	/// <returns>The ammo type identifier.</returns>
	/// <param name="type">Type.</param>
	public int getAmmoTypeId(AmmoTypes type)
	{
		for (int index = 0; index < playerInventory.playerAmmo.Count; ++index) {
			if (playerInventory.playerAmmo[index].type == type) {
				return index;
			}
		}
		return -1;
	}

	/// <summary>
	/// Sets the ammo.
	/// </summary>
	/// <param name="newAmmo">New ammo.</param>
	public void setAmmo(Ammo newAmmo)
	{
		int ammoId = getAmmoTypeId(newAmmo.type);
		if (ammoId != -1) {
			Ammo tempValue = playerInventory.playerAmmo[ammoId];
			tempValue.amount = newAmmo.amount;
			playerInventory.playerAmmo[ammoId] = tempValue;
			DI_Events.EventCenter<PlayerState>.invoke("OnUpdateHudRequest", this);
		}
	}

	/// <summary>
	/// Removes the ammo.
	/// </summary>
	/// <param name="newAmmo">New ammo.</param>
	public void removeAmmo(Ammo newAmmo)
	{
		if (!unlimitedAmmo) {
			int ammoId = getAmmoTypeId(newAmmo.type);
			if (ammoId != -1) {
				Ammo tempValue = playerInventory.playerAmmo[ammoId];
				tempValue.amount = Mathf.Clamp(newAmmo.amount - 1, 0, getMaxAmmo(newAmmo.type));
				playerInventory.playerAmmo[ammoId] = tempValue;
				DI_Events.EventCenter<PlayerState>.invoke("OnUpdateHudRequest", this);
			}
		}
	}

	/// <summary>
	/// Adds the ammo.
	/// </summary>
	/// <param name="newAmmo">New ammo.</param>
	public void addAmmo(Ammo newAmmo)
	{
		int ammoId = getAmmoTypeId(newAmmo.type);
		if (ammoId != -1) {
			Ammo tempValue = playerInventory.playerAmmo[ammoId];
			tempValue.amount = Mathf.Clamp( tempValue.amount + newAmmo.amount, 0, getMaxAmmo(newAmmo.type));
			playerInventory.playerAmmo[ammoId] = tempValue;
			DI_Events.EventCenter<PlayerState>.invoke("OnUpdateHudRequest", this);
		}
	}

	/// <summary>
	/// Gets the ammo.
	/// </summary>
	/// <returns>The ammo.</returns>
	/// <param name="type">Type.</param>
	public float getAmmo(AmmoTypes type) {
		int ammoId = getAmmoTypeId(type);
		if (ammoId != -1) {
			return playerInventory.playerAmmo[ammoId].amount;
		}
		return -1;
	}

	/// <summary>
	/// Sets the selected weapon.
	/// </summary>
	/// <param name="type">Type.</param>
	public void setSelectedWeapon(WeaponType type)
	{
		playerInventory.selectedWeapon = type;
		DI_Events.EventCenter<PlayerState>.invoke("OnUpdateHudRequest", this);
	}

	/// <summary>
	/// Gets the selected weapon.
	/// </summary>
	/// <returns>The selected weapon.</returns>
	public WeaponType getSelectedWeapon()
	{
		return playerInventory.selectedWeapon;
	}

	/// <summary>
	/// Sets the selected ammo.
	/// </summary>
	/// <param name="type">Type.</param>
	public void setSelectedAmmo(AmmoTypes type)
	{
		playerInventory.selectedAmmo = type;
		DI_Events.EventCenter<PlayerState>.invoke("OnUpdateHudRequest", this);
	}

	/// <summary>
	/// Gets the type of the selected ammo.
	/// </summary>
	/// <returns>The selected ammo type.</returns>
	public AmmoTypes getSelectedAmmoType()
	{
		return playerInventory.selectedAmmo;
	}

	public void addSnowCollector()
	{
		playerInventory.hasSnowCollector = true;
	}

	public bool hasSnowCollector()
	{
		return playerInventory.hasSnowCollector;
	}

	public void addSpeedUpgrade(int level)
	{
		playerInventory.speedUpgradeLevel = level;

		fpsController.setWalkSpeed(fpsController.getWalkSpeed() + playerWalkSpeedIncrease * level);
		fpsController.setRunSpeed(fpsController.getRunSpeed() + playerRunSpeedIncrease * level);
	}

	public int getSpeedUpgrade()
	{
		return playerInventory.speedUpgradeLevel;
	}

	public void selectGrid(TowerGrid grid)
	{
		deselectGrid();
		selectedGrid = grid;
		grid.selectGrid();
	}

	public void deselectGrid()
	{
		if (selectedGrid != null) {
			selectedGrid.deselectGrid();
			selectedGrid = null;
		}
	}

	public void highlightGrid(TowerGrid grid)
	{
		highlightedGrid = grid;
	}

	public void unHighlightGrid()
	{
		highlightedGrid = null;
	}

	public bool gridHighlighted()
	{
		return (highlightedGrid != null);
	}


	public TowerGrid getHighlightedGrid()
	{
		return highlightedGrid;
	}

	public bool gridSelected()
	{
		return (selectedGrid != null);
	}

	public TowerGrid getSelectedGrid()
	{
		return selectedGrid;
	}

	public float getTowerCost(TowerTypes type)
	{
		foreach (TowerCost cost in towerCosts.ToArray()) {
			if (cost.type == type) {
				return cost.cost;
			}
		}
		return 750.0f;
	}

	public int getXpId(AmmoTypes type)
	{
		if (xpLevels != null) {
			int index = 0;
			foreach (PlayerXp xp in xpLevels.ToArray()) {
				if (xp.weaponType == type) {
					return index;
				}
				++index;
			}
			throw new Exception("No XP id exists for that type");
		}
		throw new Exception("XP Levels is null");
	}

	public void addXp(int xpId, float amount)
	{
		try {
			PlayerXp weaponXp = xpLevels[xpId];
			if (weaponXp.currentLevel < 10) {
				if (weaponXp.xpNeededToAdvance == 0) {
					weaponXp.xpNeededToAdvance = 200 * weaponXp.currentLevel;
				}

				weaponXp.currentXp += amount;
				if (weaponXp.currentXp >= weaponXp.xpNeededToAdvance) {
					weaponXp.currentXp -= weaponXp.xpNeededToAdvance;
					weaponXp.currentLevel += 1;
					weaponXp.xpNeededToAdvance = 200 * weaponXp.currentLevel;
					//TODO GUI notification for this.
					string weaponName = xpLevels[xpId].weaponType.ToString();
					weaponName = weaponName.Replace("AMMO_", "");
					weaponName = weaponName.Replace("_", " ");
					weaponName = weaponName.ToLower();

					DI_Events.EventCenter<string, string, float>.invoke("OnDisplayNotification", "Level Up",  weaponName + " has reached level: " + weaponXp.currentLevel, 5.0f);
					Debug.Log("Welcome to level: " + weaponXp.currentLevel);
				}
				xpLevels[xpId] = weaponXp;
				#if DEBUG
					Debug.Log("Gained " + amount + "xp in " + weaponXp.weaponType);
				#endif
			}
		}
		catch (Exception err) {
			Debug.LogException(err);
		}
	}

	public float getXp(int xpId)
	{
		try {
			return xpLevels[xpId].currentXp;
		}
		catch (Exception err) {
			Debug.LogException(err);
			return 0;
		}
	}

	public int getLevel(int xpId)
	{
		try {
			return xpLevels[xpId].currentLevel;
		}
		catch (Exception err) {
			Debug.LogException(err);
			return 0;
		}
	}

	#endregion
}
