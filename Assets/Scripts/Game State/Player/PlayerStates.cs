// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

public enum PlayerStates
{
	// Player is currently on foot
	ON_FOOT,
	// Player is in siege mode in the wagon
	IN_CANNON,
	// Player is in the wagon but not in siege mode.
	IN_WAGON
}