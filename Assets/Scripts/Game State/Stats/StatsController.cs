// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//
using UnityEngine;
using System.Collections.Generic;
using System;
using MBS;

[AddComponentMenu("Game State/Stats Controller")]
public class StatsController : MonoBehaviour
{
	public int towersBuilt = 0;
	public int coinsSpent = 0;
	public int highestWave = 0;
	public Dictionary<Items, int> itemsPickedUp;
	public float starDamageTaken = 0.0f;
	public Dictionary<AmmoTypes, int> shotsFired;
	public Dictionary<AmmoTypes, int> shotsLanded;
	public Dictionary<TowerTypes, float> towerDamage;
	public Dictionary<TowerTypes, int> towerKills;
	public int waveTimersSkipped = 0;
	public Dictionary<AmmoTypes, int> itemsBought;
	public Dictionary<Enemies, int> enemiesKilled;
	public Dictionary<Enemies, float> damageToEnemies;
	public string nextLevel;
	public bool isTutorial = false;

	
	public void saveStats()
	{
			ScoreController scoreController = GameObject.Find("Game State").GetComponent<ScoreController>();
			HighScoreController highscoreController = GameObject.Find("Game State").GetComponent<HighScoreController>();
			WaveController waveController = GameObject.Find ("Game State").GetComponent<WaveController> ();
			highscoreController.CallPostScore(WULogin.UID, WULogin.display_name,
				(int)scoreController.getFinalScore(GameObject.Find("Game State").GetComponent<GameStateController>().playerState.player), (int)waveController.currentWave);

			#region Basic Stats
			// Added Type Stats
			saveAddStat("Towers Built", towersBuilt);
			saveAddStat("Coins Spent", coinsSpent);
			saveAddStat("Star Damage Taken", (int) starDamageTaken);
			saveAddStat("Wave Timers Skipped", waveTimersSkipped);
			// Max Type Stats
			saveMaxStat("Highest Wave", highestWave);
			saveMaxStat (Application.loadedLevelName + "|" + PlayerPrefs.GetInt("Difficulty", 0) + "|" + "Solo", (int)scoreController.getFinalScore(GameObject.Find("Game State").GetComponent<GameStateController>().playerState.player));

			#endregion

			#region Advanced Stats
			try {
				// Items Picked Up
				foreach (KeyValuePair<Items, int> data in itemsPickedUp) {
					saveAddStat("Items Picked Up: " + data.Key.ToString(), data.Value);
				}

				// Shots Fired
				foreach (KeyValuePair<AmmoTypes, int> data in shotsFired) {
					saveAddStat("Shots Fired: " + data.Key.ToString(), data.Value);
				}

				// Shots Landed
				foreach (KeyValuePair<AmmoTypes, int> data in shotsLanded) {
					saveAddStat("Shots Landed: " + data.Key.ToString(), data.Value);
				}

				// Tower Damage
				foreach (KeyValuePair<TowerTypes, float> data in towerDamage) {
					saveAddStat("Tower Damage: " + data.Key.ToString(), (int)data.Value);
				}

				// Tower Kills
				foreach (KeyValuePair<TowerTypes, int> data in towerKills) {
					saveAddStat("Tower Kills: " + data.Key.ToString(), (int)data.Value);
				}

				// Items Bought
				foreach (KeyValuePair<AmmoTypes, int> data in itemsBought) {
					saveAddStat("Items Bought: " + data.Key.ToString(), data.Value);
				}

				// Enemies Killed
				foreach (KeyValuePair<Enemies, int> data in enemiesKilled) {
					saveAddStat("Enemies Killed: " + data.Key.ToString(), data.Value);
				}

				// Damage to enemies
				foreach (KeyValuePair<Enemies, float> data in damageToEnemies) {
					saveAddStat("Damage To Enemies: " + data.Key.ToString(), (int)data.Value);
				}
			}
			catch (Exception err) {
				Debug.LogException(err);
			}
			#endregion
	}
	
	public void OnEnable()
	{
		DI_Events.EventCenter<AmmoTypes, PlayerState>.addListener("OnPurchase", handleOnPurchase);
		DI_Events.EventCenter<int, PlayerState>.addListener("OnSpendCoins", handleOnSpendCoins);
		DI_Events.EventCenter<int>.addListener("OnWaveEnd", handleOnWaveEnd);
		DI_Events.EventCenter<Item, PlayerState>.addListener("OnPickupItem", handlePickupItem);
		DI_Events.EventCenter<float>.addListener("OnStarDamage", handleStarDamage);
		DI_Events.EventCenter<Entity, Entity>.addListener("OnDeath", handleKillEnemy);
		DI_Events.EventCenter<AmmoTypes, PlayerState>.addListener("OnFire", handleOnFire);
		DI_Events.EventCenter<float, Entity>.addListener("OnTowerDamage", handleOnTowerDamage);
		DI_Events.EventCenter<TowerTypes>.addListener("OnBuildTower", handleOnBuildTower);
		DI_Events.EventCenter<Entity, Entity>.addListener("OnTowerKill", handleOnTowerKill);
		DI_Events.EventCenter<float, Enemies, AmmoTypes>.addListener("OnDamage", handleOnDamage);
		DI_Events.EventCenter.addListener("OnStarDeath", handleStarDeath);
		DI_Events.EventCenter.addListener("OnVictory", handleVictory);
		DI_Events.EventCenter<string>.addListener("OnSetNextLevel", handleSetNextLevel);
		DI_Events.EventCenter.addListener("OnSkipWaveTimer", handleSkipWaveTimer);

		itemsPickedUp = new Dictionary<Items, int>();
		shotsFired = new Dictionary<AmmoTypes, int>();
		shotsLanded = new Dictionary<AmmoTypes, int>();
		towerDamage = new Dictionary<TowerTypes, float>();
		towerKills = new Dictionary<TowerTypes, int>();
		itemsBought = new Dictionary<AmmoTypes, int>();
		enemiesKilled = new Dictionary<Enemies, int>();
		damageToEnemies = new Dictionary<Enemies, float>();
	}

	public void OnDisable()
	{
		DI_Events.EventCenter<AmmoTypes, PlayerState>.removeListener("OnPurchase", handleOnPurchase);
		DI_Events.EventCenter<int, PlayerState>.removeListener("OnSpendCoins", handleOnSpendCoins);
		DI_Events.EventCenter<int>.removeListener("OnWaveEnd", handleOnWaveEnd);
		DI_Events.EventCenter<Item, PlayerState>.removeListener("OnPickupItem", handlePickupItem);
		DI_Events.EventCenter<float>.removeListener("OnStarDamage", handleStarDamage);
		DI_Events.EventCenter<Entity, Entity>.removeListener("OnDeath", handleKillEnemy);
		DI_Events.EventCenter<AmmoTypes, PlayerState>.removeListener("OnFire", handleOnFire);
		DI_Events.EventCenter<float, Entity>.removeListener("OnTowerDamage", handleOnTowerDamage);
		DI_Events.EventCenter<TowerTypes>.removeListener("OnBuildTower", handleOnBuildTower);
		DI_Events.EventCenter<Entity, Entity>.removeListener("OnTowerKill", handleOnTowerKill);
		DI_Events.EventCenter<float, Enemies, AmmoTypes>.removeListener("OnDamage", handleOnDamage);
		DI_Events.EventCenter.removeListener("OnStarDeath", handleStarDeath);
		DI_Events.EventCenter.removeListener("OnVictory", handleVictory);
		DI_Events.EventCenter<string>.removeListener("OnSetNextLevel", handleSetNextLevel);
		DI_Events.EventCenter.removeListener("OnSkipWaveTimer", handleSkipWaveTimer);
	}

	public void saveAddStat(string prefName, int currentValue)
	{
		PlayerPrefs.SetInt(prefName, PlayerPrefs.GetInt(prefName, 0) + currentValue);
		Debug.Log("Saving Stat: " + prefName + " value: " + currentValue);
	}

	public void saveMaxStat(string prefName, int currentValue)
	{
		int oldStat = PlayerPrefs.GetInt(prefName, 0);
		if (oldStat < currentValue) {
			PlayerPrefs.SetInt(prefName, currentValue);
			Debug.Log("Saving Stat: " + prefName + " value: " + currentValue);
		}
	}

	public void handleSetNextLevel(string level)
	{
		nextLevel = level;
	}

	public void handleVictory()
	{
		//TODO save all the stats before loading the victory level.
		#if DEBUG
		Debug.Log("OnVictory");
		#endif
		if (!isTutorial) {
			saveStats();
		}
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;

		Application.LoadLevel(nextLevel);
	}

	public void handleStarDeath()
	{
		//TODO save all the stats before loading the defeat level.
		#if DEBUG
		Debug.Log("OnStarDeath");
		#endif
		saveStats();
		
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;

		Application.LoadLevel("Defeat");
	}
	
	public void handleOnPurchase(AmmoTypes ammoType, PlayerState playerState)
	{
		#if DEBUG
		Debug.Log("OnPurchase");
		#endif
		if (itemsBought.ContainsKey(ammoType)) {
			itemsBought[ammoType] += 1;
		}
		else {
			itemsBought.Add(ammoType, 1);
		}
	}

	public void handleOnSpendCoins(int ammount, PlayerState playerState)
	{
		#if DEBUG
		Debug.Log("OnSpendCoins");
		#endif
		coinsSpent += ammount;
	}

	public void handleOnWaveEnd(int wave)
	{
		#if DEBUG
		Debug.Log("OnWaveEnd");
		#endif
		highestWave = wave;
	}

	public void handlePickupItem(Item item, PlayerState playerState)
	{
		#if DEBUG
		Debug.Log("OnPickupItem");
		#endif
//		if (itemsPickedUp.ContainsKey(ammoType)) {
//			itemsBought[ammoType] += 1;
//		}
//		else {
//			itemsBought.Add(ammoType, 1);
//		}
	}

	public void handleStarDamage(float damage)
	{
		#if DEBUG
		Debug.Log("OnStarDamage");
		#endif
		starDamageTaken += damage;
	}

	public void handleKillEnemy(Entity target, Entity attacker)
	{
		#if DEBUG
		Debug.Log("OnKillEnemy");
		#endif
		// Check the tag if its not enemy then a tower has died.
		if (target.tag == "Enemy") {
			Enemy enemy = target.gameObject.GetComponent<Enemy>();
			if (enemiesKilled.ContainsKey(enemy.type)) {
				enemiesKilled[enemy.type] += 1;
			}
			else {
				enemiesKilled.Add(enemy.type, 1);
			}
		}
	}

	public void handleOnFire(AmmoTypes ammoType, PlayerState playerState)
	{
		#if DEBUG
		Debug.Log("OnFire");
		#endif
		if (shotsFired.ContainsKey(ammoType)) {
			shotsFired[ammoType] += 1;
		}
		else {
			shotsFired.Add(ammoType, 1);
		}
	}

	public void handleOnTowerDamage(float damage, Entity tower)
	{
		#if DEBUG
		Debug.Log("OnTowerDamage");
		#endif
//		if (towerDamage.ContainsKey(towerType)) {
//			towerDamage[towerType] += damage;
//		}
//		else {
//			towerDamage.Add(towerType, damage);
//		}
	}

	public void handleOnBuildTower(TowerTypes type)
	{
		#if DEBUG
		Debug.Log("OnBuildTower");
		#endif
		towersBuilt += 1;
	}

	public void handleOnTowerKill(Entity target, Entity attacker)
	{
		#if DEBUG
		Debug.Log("OnTowerKill");
		#endif
//		if (shotsLanded.ContainsKey(ammoType)) {
//			shotsLanded[ammoType] += 1;
//		}
//		else {
//			shotsLanded.Add(ammoType, 1);
//		}
	}

	public void handleOnDamage(float damage, Enemies victim, AmmoTypes ammoType)
	{
		#if DEBUG
		Debug.Log("OnDamage");
		#endif
		if (shotsLanded.ContainsKey(ammoType)) {
			shotsLanded[ammoType] += 1;
		}
		else {
			shotsLanded.Add(ammoType, 1);
		}

		if (damageToEnemies.ContainsKey(victim)) {
			damageToEnemies[victim] += damage;
		}
		else {
			damageToEnemies.Add(victim, damage);
		}
	}

	public void handleSkipWaveTimer()
	{
		#if DEBUG
		Debug.Log("OnSkipWaveTimer");
		#endif
		waveTimersSkipped += 1;
	}
}