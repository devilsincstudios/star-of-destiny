// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

using UnityEngine;
[AddComponentMenu("Items/Ammo/Ammo Box")]
public class AmmoBox : Item
{
	public Ammo ammoInfo;

	new public void OnTriggerEnter(Collider other) {
		if (other.tag == "Player") {
			PlayerState player = other.gameObject.GetComponent<PlayerState>();
			DI_Events.EventCenter<AmmoBox, PlayerState>.invoke("OnPickupAmmo", this, player);
			DI_Events.EventCenter<AudioClip, float>.invoke("OnPlayEffect", pickupSound, pickupVolume);
			this.gameObject.SetActive(false);
		}
	}
}