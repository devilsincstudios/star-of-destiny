// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

public enum AmmoTypes
{
	AMMO_ICICLE,
	AMMO_CANDY_CANE,
	AMMO_SNOWBALL,
	AMMO_STEAM,
	AMMO_SANTA_BOMBER,
	AMMO_PRESENT_BOMB,
	AMMO_BAUBLE_GRENADE,
	AMMO_NONE,
	AMMO_TOWER_BASIC,
	AMMO_TOWER_CANNON,
	AMMO_TOWER_AOE_SLOW,
	AMMO_TOWER_AOE_DAMAGE,
	AMMO_TOWER_LASER,
	AMMO_TOWER_SPLIT_SHOT,
	AMMO_TOWER_SNIPER,
	AMMO_TOWER_ROCKET,
	AMMO_TOWER_BOLT,
	AMMO_CHEAT,
	AMMO_RAMBO,
	AMMO_ENEMY,
	ITEM_SPEED_UPDATE,
	ITEM_SNOW_COLLECTOR,
}