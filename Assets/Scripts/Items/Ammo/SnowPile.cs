// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: When then SnowPile it will be visiable on pickup the SnowPile will
//		Disapper and the timer will be set to respawn the SnowPile

#region Includes
using UnityEngine;
using System;
using System.Collections;
#endregion

[AddComponentMenu("Items/Ammo/Snow Pile")]
public class SnowPile : AmmoBox
{
	#region Public Variables
	public float respawnTime = 30.0f;
	[HideInInspector]
	public float respawnTimer = 0.0f;
	public bool onCoolDown = false;
	#endregion

	#region Public Methods

	/// <summary>
	/// Raises the trigger enter event.
	/// </summary>
	/// <param name="other">Other.</param>
	new public void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player") {
			PlayerState player = other.gameObject.GetComponent<PlayerState>();
			DI_Events.EventCenter<AmmoBox, PlayerState>.invoke("OnPickupAmmo", this, player);
			DI_Events.EventCenter<AudioClip, float>.invoke("OnPlayEffect", pickupSound, pickupVolume);
			StartCoroutine("respawn");
		}
	}

	/// <summary>
	/// Respawn this instance.
	/// </summary>
	public IEnumerator respawn()
	{
		RenderChanger(false);
		this.GetComponent<Collider>().enabled = false;
		yield return new WaitForSeconds(respawnTime);
		this.GetComponent<Collider>().enabled = true;
		RenderChanger(true);
	}

	/// <summary>
	/// Changes the enabled state of all the meshrenderer objects attached to this object
	/// </summary>
	/// <param name="isRender">If set to <c>true</c> is render.</param>
	void RenderChanger(bool isRender){
		foreach(MeshRenderer renderer in GetComponentsInChildren<MeshRenderer>()){
			renderer.enabled = isRender;
		}
	}
	#endregion
}
	