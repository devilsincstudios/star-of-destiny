// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: This script is to detect when the player has walked on the item,
//		when they have a sound will be played and then the item will be destroyed
//

using UnityEngine;
using System.Collections;

[RequireComponent(typeof(DI_Game.RotateObject))]
public class Item : MonoBehaviour
{
	public Items itemType;
	public float amount;
	public AudioClip pickupSound;
	public float pickupVolume;
	public float deSpawnTime = 60.0f;
	public bool canDeSpawn = true;

	public void OnEnable()
	{
		if (canDeSpawn) {
			StartCoroutine("deSpawn");
		}
		DI_Events.EventCenter<Item>.invoke("OnDrop", this);
	}

	public void forcedPickup(PlayerState player)
	{
		if (itemType == Items.ITEM_AMMO_BOX) {
			DI_Events.EventCenter<AmmoBox, PlayerState>.invoke("OnPickupAmmo", (AmmoBox)this, player);
		}
		else {
			DI_Events.EventCenter<Item, PlayerState>.invoke("OnPickupItem", this, player);
		}
		DI_Events.EventCenter<AudioClip, float>.invoke("OnPlayEffect", pickupSound, pickupVolume);
		gameObject.SetActive(false);
	}

	public void forcedDespawn()
	{
		PlayerState player = GameObject.Find("Player One").GetComponent<PlayerState>();
		if (player.hasSnowCollector()) {
			DI_Events.EventCenter<Item, PlayerState>.invoke("OnPickupItem", this, player);
			DI_Events.EventCenter<AudioClip, float>.invoke("OnPlayEffect", pickupSound, pickupVolume);
		}
		DI_Events.EventCenter<Item>.invoke("OnDespawn", this);
		gameObject.SetActive(false);
	}

	public IEnumerator deSpawn()
	{
		yield return new WaitForSeconds(deSpawnTime);
		PlayerState player = GameObject.Find("Player One").GetComponent<PlayerState>();
		if (player.hasSnowCollector()) {
			DI_Events.EventCenter<Item, PlayerState>.invoke("OnPickupItem", this, player);
			DI_Events.EventCenter<AudioClip, float>.invoke("OnPlayEffect", pickupSound, pickupVolume);
		}

		DI_Events.EventCenter<Item>.invoke("OnDespawn", this);
		gameObject.SetActive(false);
	}

	public void OnTriggerEnter(Collider other) {
		if (other.tag == "Player") {
			PlayerState player = other.gameObject.GetComponent<PlayerState>();
			DI_Events.EventCenter<Item, PlayerState>.invoke("OnPickupItem", this, player);
			DI_Events.EventCenter<AudioClip, float>.invoke("OnPlayEffect", pickupSound, pickupVolume);
			gameObject.SetActive(false);
		}
	}
}