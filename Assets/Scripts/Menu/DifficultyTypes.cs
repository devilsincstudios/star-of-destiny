﻿public enum DifficultyTypes{
	EASY,
	MEDIUM,
	HARD,
	INSANE
}