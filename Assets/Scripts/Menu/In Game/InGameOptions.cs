// Devils Inc Studios
// // Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

using UnityEngine;

public class InGameOptions : MonoBehaviour
{
	public GameStateController gsc;

	public void OnEnable()
	{
		gsc = GameObject.Find("Game State").GetComponent<GameStateController>();
	}

	public void OnClick()
	{
		gsc.menuController.SendMessage("onOptionsMenuEnter");
	}

	public void OnBack()
	{
		gsc.menuController.SendMessage("onMenuEnter");
	}
}