// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

using UnityEngine;

namespace DI.Menus
{
	[AddComponentMenu("Menus/Main/Options")]
	public class Options : MonoBehaviour
	{
		public Canvas mainMenu;
		public Canvas optionsMenu;

		public void goBack()
		{
			mainMenu.gameObject.SetActive(true);
			optionsMenu.gameObject.SetActive(false);
			DI_Events.EventCenter.invoke("OnOptionsChanged");
		}

		public void OnClick()
		{
			mainMenu.gameObject.SetActive(false);
			optionsMenu.gameObject.SetActive(true);
		}
	}
}