﻿public enum StarAmountGainable{
	NONE,
	ONE,
	TWO,
	THREE,
	FOUR,
	FIVE
}