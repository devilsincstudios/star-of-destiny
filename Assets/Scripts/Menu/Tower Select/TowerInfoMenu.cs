// Devils Inc Studios
// // Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using UnityEngine.UI;


[AddComponentMenu("AI/Towers/Tower Info Menu Helper")]
public class TowerInfoMenu : MonoBehaviour {

	public GameStateController gsc;
	public Text speed;
	public Text damage;
	public Text chillPercent;
	public Text chillDuration;
	public Text kills;
	public Text value;
	public Text xpLevel;
	public Text xpAmount;
	public Text towerName;
	public Image xpBar;
	public TowerInfoMenuActions actions;

	public void OnEnable()
	{
		DI_Events.EventCenter<Tower>.addListener("OnSelectTower", handleOnTowerSelect);
		gsc = GameObject.Find("Game State").GetComponent<GameStateController>();
	}

	public void OnDisable()
	{
		DI_Events.EventCenter<Tower>.removeListener("OnSelectTower", handleOnTowerSelect);
	}

	public void handleOnTowerSelect(Tower tower)
	{
		if (tower != null) {
			gsc.menuController.SendMessage("onTowerInfoEnter");
			towerName.text = tower.type.ToString();
			speed.text = (tower.stats.baseSpeed + (tower.towerLevel * tower.stats.upgradeSpeed)).ToString();
			damage.text = (tower.stats.baseDamage + (tower.towerLevel * tower.stats.upgradeDamage)).ToString();
			chillPercent.text = (tower.stats.baseChillSlow + (tower.towerLevel * tower.stats.upgradeChillSlow)).ToString();
			chillDuration.text = (tower.stats.baseChillDuration + (tower.towerLevel * tower.stats.upgradeChillDuration)).ToString();
			kills.text = tower.kills.ToString();
			value.text = (tower.stats.baseCost * .75) + tower.towerLevel * tower.stats.upgradeCost + " coins";
			xpLevel.text = "Level: " + tower.towerLevel;
			xpAmount.text = tower.towerXp + "/" + tower.xpToNextLevel;
			xpBar.fillAmount = (tower.towerXp/tower.xpToNextLevel);
			actions.sellAmount = (float) (tower.stats.baseCost * .75) + (tower.towerLevel * tower.stats.upgradeCost);
			actions.tower = tower.gameObject;
		}
	}
}