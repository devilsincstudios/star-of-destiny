// Devils Inc Studios
// // Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System;

[AddComponentMenu("Menus/Tower Select/Tower Info Menu Actions")]
public class TowerInfoMenuActions : MonoBehaviour
{
	public GameStateController gsc;
	public float sellAmount = 0.0f;
	public GameObject tower;

	public void OnEnable()
	{
		gsc = GameObject.Find("Game State").GetComponent<GameStateController>();
	}

	public void sellTower()
	{
		Tower towerScript;

		if (tower.name.Contains("AOE")) {
			towerScript = (Tower)tower.GetComponent<AOETower>();
		}
		else {
			towerScript = (Tower)tower.GetComponent<TargetedTower>();
		}

		if (towerScript.gridSpace != null) {
			towerScript.gridSpace.sellTower();
		}

		Destroy(tower);
		gsc.playerState.addCoins(sellAmount);
		gsc.menuController.SendMessage("onTowerSelectExit");
		gsc.playerState.activeTowers -= 1;
		DI_Events.EventCenter<PlayerState>.invoke("OnUpdateHudRequest", gsc.playerState);
	}

	public void closeMenu()
	{
		gsc.menuController.SendMessage("onTowerSelectExit");
	}
}