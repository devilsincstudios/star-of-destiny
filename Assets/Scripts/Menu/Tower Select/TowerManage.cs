// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

using UnityEngine;

[AddComponentMenu("Menus/Towers/Tower Manage")]
public class TowerManage : MonoBehaviour
{
	public TowerTypes type;
	public PlayerState playerState;
	[HideInInspector]
	public TowerGrid selectedGrid;

	public void OnEnable()
	{
		playerState = GameObject.Find("Player").GetComponent<PlayerState>();
	}
	
	public void buildTower()
	{
		if (playerState.gridSelected())
		{
			selectedGrid = playerState.getSelectedGrid();
			selectedGrid.buildTower();
			selectedGrid.deselectGrid();
			playerState.isPlacingTowers = false;
			DI_Events.EventCenter.invoke("onDisableGrids");
		}
	}

	public void sellTower()
	{
		if (playerState.gridSelected())
		{
			selectedGrid = playerState.getSelectedGrid();
			selectedGrid.hideShadowTower();
			playerState.addCoins(playerState.getTowerCost(selectedGrid.tower) / 2);
			selectedGrid.deselectGrid();
			playerState.isPlacingTowers = false;
			DI_Events.EventCenter.invoke("onDisableGrids");
		}
	}
}