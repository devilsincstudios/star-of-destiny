// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

using UnityEngine;

[AddComponentMenu("Menus/Towers/Tower Menu")]
public class TowerMenu : MonoBehaviour
{
	public MenuController menuController;

	public void enterTowerMenu()
	{
		menuController.SendMessage("onTowerSelectEnter");
	}

	public void exitTowerMenu()
	{
		menuController.SendMessage("onTowerSelectExit");
	}
}
