// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

[AddComponentMenu("Menus/Towers/Tower Select")]
public class TowerSelect : MonoBehaviour
{
	[Header("Tower Data Settings")]
	public List<TowerStats> towers;
	public int currentTower = 0;
	[Header("Tower Description")]
	public Text towerName;
	public Text description;
	[Header("Tower Icon")]
	public Image towerIcon;
	[HideInInspector]
	public PlayerState playerState;
	[Header("Base Stats")]
	public Text baseDamage;
	public Text baseSpeed;
	public Text baseCost;
	public Text baseHealth;
	public Text baseChillSlow;
	public Text baseChillDuration;
	[Header("Upgraded Stats")]
	public Text upgradeDamage;
	public Text upgradeSpeed;
	public Text upgradeCost;
	public Text upgradeHealth;
	public Text upgradeChillSlow;
	public Text upgradeChillDuration;
	[Header("Select Button")]
	public GameObject selectButton;
	[Header("Coins Notification Text")]
	public GameObject notEnoughCoins;

	public void OnEnable()
	{
		playerState = GameObject.Find("Player One").GetComponent<PlayerState>();
		updateTowerData();
	}

	public void previousTower()
	{
		--currentTower;
		if (currentTower < 0) {
			currentTower = (towers.Count - 1);
		}
		updateTowerData();
	}

	public void nextTower()
	{
		++currentTower;
		if (currentTower >= towers.Count) {
			currentTower = 0;
		}
		updateTowerData();
	}

	public void updateTowerData()
	{
		towerName.text = towers[currentTower].type.ToString();
		description.text = towers[currentTower].description;
		towerIcon.color = towers[currentTower].towerColor;
		// Base
		baseDamage.text = towers[currentTower].baseDamage.ToString();
		baseSpeed.text = towers[currentTower].baseSpeed.ToString();
		baseCost.text = towers[currentTower].baseCost.ToString();
		baseHealth.text = towers[currentTower].baseHealth.ToString();
		baseChillDuration.text = towers[currentTower].baseChillDuration.ToString();
		baseChillSlow.text = towers[currentTower].baseChillSlow.ToString();
		// Upgrades
		upgradeDamage.text = towers[currentTower].upgradeDamage.ToString();
		upgradeSpeed.text = towers[currentTower].upgradeSpeed.ToString();
		upgradeCost.text = towers[currentTower].upgradeCost.ToString();
		upgradeHealth.text = towers[currentTower].upgradeHealth.ToString();
		upgradeChillDuration.text = towers[currentTower].upgradeChillDuration.ToString();
		upgradeChillSlow.text = towers[currentTower].upgradeChillSlow.ToString();
		if (playerState.getCoins() >= towers[currentTower].baseCost) {
			selectButton.SetActive(true);
			notEnoughCoins.SetActive(false);
		}
		else {
			selectButton.SetActive(false);
			notEnoughCoins.SetActive(true);
		}
	}

	public void selectTower()
	{
		playerState.selectedTowerType = towers[currentTower].type;
		if (!playerState.isPlacingTowers) {
			playerState.isPlacingTowers = true;
			DI_Events.EventCenter.invoke("onEnableGrids");
		}
		playerState.gsc.menuController.SendMessage("onMenuExit", SendMessageOptions.RequireReceiver);
		DI_Events.EventCenter<PlayerState>.invoke("OnUpdateHudRequest", playerState);
	}
}