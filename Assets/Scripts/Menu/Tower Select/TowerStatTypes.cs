// Devils Inc Studios
// // Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

public enum TowerStatTypes
{
	ATTACK_DAMAGE,
	ATTACK_SPEED,
	SLOW_DURATION,
	SLOW_PERCENT,
}