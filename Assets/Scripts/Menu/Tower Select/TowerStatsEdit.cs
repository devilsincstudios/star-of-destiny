// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

[AddComponentMenu("Menus/Towers/Tower Stats Editor")]
public class TowerStatsEdit : MonoBehaviour
{
	public TowerInfoMenuActions towerInfo;
	public TowerStatTypes statType;
	public Text placeHolderValue;
	public Text value;
	public Tower tower;

	public void OnEnable()
	{
		towerInfo = GameObject.Find("Tower Info").GetComponent<TowerInfoMenuActions>();

		if (towerInfo != null) {
			if (tower == null) {
				tower = towerInfo.tower.GetComponent<Tower>();
			}
		}

		if (tower != null) {
			switch(statType) {
			case TowerStatTypes.ATTACK_DAMAGE:
				placeHolderValue.text = (tower.stats.baseDamage + (tower.towerLevel * tower.stats.upgradeDamage)).ToString();
				value.text = (tower.stats.baseDamage + (tower.towerLevel * tower.stats.upgradeDamage)).ToString();
				break;
			case TowerStatTypes.ATTACK_SPEED:
				placeHolderValue.text = (tower.stats.baseSpeed + (tower.towerLevel * tower.stats.upgradeSpeed)).ToString();
				value.text = (tower.stats.baseSpeed + (tower.towerLevel * tower.stats.upgradeSpeed)).ToString();
				break;
			case TowerStatTypes.SLOW_DURATION:
				placeHolderValue.text = (tower.stats.baseChillDuration + (tower.towerLevel * tower.stats.upgradeChillDuration)).ToString();
				value.text = (tower.stats.baseChillDuration + (tower.towerLevel * tower.stats.upgradeChillDuration)).ToString();
				break;
			case TowerStatTypes.SLOW_PERCENT:
				placeHolderValue.text = (tower.stats.baseChillSlow + (tower.towerLevel * tower.stats.upgradeChillSlow)).ToString();
				value.text = (tower.stats.baseChillSlow + (tower.towerLevel * tower.stats.upgradeChillSlow)).ToString();
				break;
			}
		}
	}
	
	public void OnDisable()
	{
		tower = null;
		towerInfo = null;
	}

	public void onSpeedChange(string speed)
	{
//		if (speed != null) {
//			tower.attackSpeed = float.Parse(speed);
//		}
	}

	public void onDamageChange(string damage)
	{
//		if (damage != null) {
//			tower.damage = float.Parse(damage);
//		}
	}

	public void onSlowPercentChange(string slowPercent)
	{
//		if (slowPercent != null) {
//			tower.slowPercent = float.Parse(slowPercent);
//		}
	}

	public void onSlowDurationChange(string slowDuration)
	{
//		if (slowDuration != null) {
//			tower.slowDuration = float.Parse(slowDuration);
//		}
	}
}
