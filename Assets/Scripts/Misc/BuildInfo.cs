// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
// TODO: Include a description of the file here.
//

using UnityEngine;

public static class BuildInfo
{
	public static float majorRevNumber = PlayerPrefs.GetFloat("Build Major Rev", 0f);
	public static float minorRevNumber = PlayerPrefs.GetFloat("Build Minor Rev", 0f);
	public static float buildNumber = PlayerPrefs.GetFloat("Build Number", 0f);
	public static string buildId = majorRevNumber + "." + minorRevNumber + "b" + buildNumber;
	public static string buildDisplayName = PlayerPrefs.GetString("Build Display Name", "") + ": " + buildId;
	public static string buildName = PlayerPrefs.GetString("Build Name", "") + "_" + buildId;

	public static void refreshInfo()
	{
		majorRevNumber = PlayerPrefs.GetFloat("Build Major Rev", 0f);
		minorRevNumber = PlayerPrefs.GetFloat("Build Minor Rev", 0f);
		buildNumber = PlayerPrefs.GetFloat("Build Number", 0f);
		buildId = majorRevNumber + "." + minorRevNumber + "b" + buildNumber;
		buildDisplayName = PlayerPrefs.GetString("Build Display Name", "") + ": " + buildId;
		buildName = PlayerPrefs.GetString("Build Name", "") + "_" + buildId;
	}
}