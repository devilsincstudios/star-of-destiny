// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

public enum CardinalDirections
{
	NORTH,
	NORTH_EAST,
	NORTH_WEST,
	EAST,
	SOUTH,
	SOUTH_EAST,
	SOUTH_WEST,
	WEST,
}