// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

#region Includes
using UnityEngine;
using System;
#endregion

[Serializable]
public struct PrefabEntry
{
	#region Public Variables
	public string name;
	public GameObject gameObject;
	#endregion
}