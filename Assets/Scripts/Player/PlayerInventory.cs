// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

#region Includes
using UnityEngine;
using System.Collections.Generic;
using System;
#endregion

[Serializable]
public struct PlayerInventory
{
	#region Public Variables
	public float coins;
	public List<Upgrades> upgrades;
	public List<Ammo> playerAmmo;
	public List<Ammo> maxAmmo;
	public WeaponType selectedWeapon;
	public AmmoTypes selectedAmmo;
	public bool hasSnowCollector;
	public int speedUpgradeLevel;
	#endregion
}