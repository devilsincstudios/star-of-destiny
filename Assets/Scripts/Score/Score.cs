// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

using System.Collections.Generic;
using System;

[Serializable]
public class Score
{
	public List<float> enemiesKilled;
	public float coinsCollected;
	public float damageToStar;
	public float points;
}