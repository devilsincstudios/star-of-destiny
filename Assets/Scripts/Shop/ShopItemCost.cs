﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShopItemCost : MonoBehaviour {
	public int snowBallCost;
	public int icicleCost;
	public int candyCaneCost;
	public int steamCost;
	public int baubleGrenadeCost;
	public int presentBombCost;
	public int santaBomberCost;

	public int snowCollectorCost;
	public int speedUpgradeCost;

	public Text snowBallText;
	public Text icicleText;
	public Text candyCaneText;
	public Text steamText;
	public Text baubleGrenadeText;
	public Text presentBombText;
	public Text santaBomberText;

	public Text snowCatherText;
	public Text speedUpgradeText;

	// Use this for initialization
	void Start () {
		UpdateItemCosts(snowBallCost, snowBallText);
		UpdateItemCosts(icicleCost, icicleText);
		UpdateItemCosts(candyCaneCost, candyCaneText);
		UpdateItemCosts(steamCost, steamText);
		UpdateItemCosts(baubleGrenadeCost, baubleGrenadeText);
		UpdateItemCosts(presentBombCost, presentBombText);
		UpdateItemCosts(santaBomberCost, santaBomberText);

		UpdateItemCosts(snowCollectorCost, snowCatherText);
		UpdateItemCosts(speedUpgradeCost, speedUpgradeText);
	}
	
	void UpdateItemCosts(int cost, Text item){
		item.text = cost.ToString() + " Coins";
	}
}
