﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShopPurchaseItem : MonoBehaviour {
	public PlayerState playerStates;
	public ShopItemCost shopItemCost;
	public float playerCoins;
	public Text playerCoinsText;
	// Use this for initialization
	void Start ()
	{
		playerStates = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerState>();
		shopItemCost = GetComponent<ShopItemCost>();
		playerCoins = playerStates.getCoins();
	}

	void Update()
	{
		playerCoins = playerStates.getCoins();
		playerCoinsText.text = "Coins: " + playerCoins.ToString();
	}

	public void PurchaseSnowBalls()
	{
		AmmoAdder(AmmoTypes.AMMO_SNOWBALL, 25, shopItemCost.snowBallCost);
	}

	public void PurchaseIcicle()
	{
		AmmoAdder(AmmoTypes.AMMO_ICICLE, 50, shopItemCost.icicleCost);
	}

	public void PurchaseCandyCane()
	{
		AmmoAdder(AmmoTypes.AMMO_CANDY_CANE, 50, shopItemCost.candyCaneCost);
	}

	public void PurchaseSteam()
	{
		AmmoAdder(AmmoTypes.AMMO_STEAM, 100, shopItemCost.steamCost);
	}

	public void PurchaseBauble()
	{
		AmmoAdder(AmmoTypes.AMMO_BAUBLE_GRENADE, 1, shopItemCost.baubleGrenadeCost);
	}

	public void PurchasePresent()
	{
		AmmoAdder(AmmoTypes.AMMO_PRESENT_BOMB, 1, shopItemCost.presentBombCost);
	}

	public void PurchaseSantaBomber()
	{
		AmmoAdder(AmmoTypes.AMMO_SANTA_BOMBER, 1, shopItemCost.santaBomberCost);
	}

	public void PurchaseSpeedUpgrade()
	{
		if ((int)playerCoins >= shopItemCost.speedUpgradeCost) {
			// Level 3 is max
			if (playerStates.getSpeedUpgrade() < 3) {
				playerStates.addSpeedUpgrade(playerStates.getSpeedUpgrade() + 1);
				playerStates.removeCoins(shopItemCost.speedUpgradeCost);
				DI_Events.EventCenter<AmmoTypes, PlayerState>.invoke("OnPurchase", AmmoTypes.ITEM_SPEED_UPDATE, playerStates);
				DI_Events.EventCenter<int, PlayerState>.invoke("OnSpendCoins", shopItemCost.speedUpgradeCost, playerStates);
			}
			if (playerStates.getSpeedUpgrade() >= 3) {
				shopItemCost.speedUpgradeText.transform.parent.GetComponent<Button>().interactable = false;
				shopItemCost.speedUpgradeText.transform.parent.FindChild("Available text").GetComponent<Text>().text = "Out of Stock";
			}
		}
	}

	public void PurchaseSnowCollector()
	{
		if ((int)playerCoins >= shopItemCost.snowCollectorCost) {
			if (!playerStates.hasSnowCollector()) {
				playerStates.addSnowCollector();
				playerStates.removeCoins(shopItemCost.snowCollectorCost);
				//TODO change this it should be somewhere on the object rather then searching for it by name.
				GameObject.Find("Snow Collector Background").GetComponent<Button>().interactable = false;
				DI_Events.EventCenter<AmmoTypes, PlayerState>.invoke("OnPurchase", AmmoTypes.ITEM_SNOW_COLLECTOR, playerStates);
				DI_Events.EventCenter<int, PlayerState>.invoke("OnSpendCoins", shopItemCost.snowCollectorCost, playerStates);
				shopItemCost.snowCatherText.transform.parent.FindChild("Available text").GetComponent<Text>().text = "Out of Stock";
			}
		}
	}

	public void AmmoAdder(AmmoTypes ammoType, int amount, int itemCost)
	{
		Ammo itemAmmo;

		itemAmmo.type = ammoType;
		itemAmmo.amount = amount;

		if(playerCoins >= itemCost && playerStates.getAmmo(itemAmmo.type) < playerStates.getMaxAmmo(itemAmmo.type)){
			playerStates.removeCoins(itemCost);
			playerStates.addAmmo(itemAmmo);
			DI_Events.EventCenter<AmmoTypes, PlayerState>.invoke("OnPurchase", itemAmmo.type, playerStates);
			DI_Events.EventCenter<int, PlayerState>.invoke("OnSpendCoins", itemCost, playerStates);
		}
	}

	public void exitShopMenu()
	{
		playerStates.gsc.menuController.SendMessage("onMenuExit", SendMessageOptions.RequireReceiver);
	}
}
