// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

public struct Upgrades
{
	public string name;
	public string description;
	public int rank;
	public float cost;
	public bool owned;
}