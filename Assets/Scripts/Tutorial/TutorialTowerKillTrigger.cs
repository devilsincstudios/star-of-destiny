// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System.Collections;

[AddComponentMenu("Tutorial/Dialog Events")]
public class TutorialTowerKillTrigger : MonoBehaviour
{
	public bool triggeredTowerKill = false;
	public bool triggeredPlayerKill = false;
	public string towerKillDialogName;
	public string playerKillDialogName;
	public bool ignoredFirstEvent = false;

	public void OnEnable()
	{
		DI_Events.EventCenter<Entity, Entity>.addListener("OnTowerKill", handleOnTowerKill);
		DI_Events.EventCenter<Entity, Entity>.addListener("OnDeath", handleOnPlayerKill);
		DI_Events.EventCenter<TowerTypes>.addListener ("OnBuildTower", handleOnBuildTower);
	}

	public void OnDisable()
	{
		DI_Events.EventCenter<Entity, Entity>.addListener("OnTowerKill", handleOnTowerKill);
		DI_Events.EventCenter<Entity, Entity>.addListener("OnDeath", handleOnPlayerKill);
		DI_Events.EventCenter<TowerTypes>.addListener ("OnBuildTower", handleOnBuildTower);
	}

	public void handleOnBuildTower(TowerTypes towerType)
	{
		GameObject tower = GameObject.FindGameObjectWithTag("Tower");
		StartCoroutine(setTowerTarget(tower));
	}

	public void handleOnTowerKill(Entity victim, Entity attacker)
	{
		if (attacker.tag == "Tower") {
			if (!triggeredTowerKill) {
				DI_Events.EventCenter<string, DialogEventTypes>.invoke("OnDialogEvent", towerKillDialogName, DialogEventTypes.EVENT_START_DIALOG);
				triggeredTowerKill = true;
			}
		}
	}

	public void handleOnPlayerKill(Entity victim, Entity attacker)
	{
		if (attacker.tag == "Player") {
			if (triggeredTowerKill && !triggeredPlayerKill) {
				if (ignoredFirstEvent) {
					DI_Events.EventCenter<string, DialogEventTypes>.invoke("OnDialogEvent", playerKillDialogName, DialogEventTypes.EVENT_START_DIALOG);
					Debug.Log(System.Reflection.MethodBase.GetCurrentMethod().Name);
					triggeredPlayerKill = true;
				}
				else {
					ignoredFirstEvent = true;
				}
			}
		}
	}

	public IEnumerator setTowerTarget(GameObject tower)
	{
		yield return new WaitForSeconds(1.0f);
		TargetedTower targetedTower = tower.GetComponent<TargetedTower>();
		targetedTower.target = this.GetComponentInParent<Enemy>();
		targetedTower.hasTarget = true;
		targetedTower.laserSight.enabled = true;
	}
}