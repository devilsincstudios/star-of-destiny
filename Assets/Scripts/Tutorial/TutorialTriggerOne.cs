// Devils Inc Studios
// // Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;

[AddComponentMenu("Triggers/Tutorial/One")]
public class TutorialTriggerOne : Trigger
{
	public GameObject waypointTwoField;
	public GameObject shield;
	public GameObject waypointOneField;

	new public void OnTriggerEnter(Collider other)
	{
		if (shouldTrigger(other)) {
			triggered = true;
			waypointOneField.SetActive(false);
			waypointTwoField.SetActive(true);
			PlayerState playerState = GameObject.Find("Player One").GetComponent<PlayerState>();
			playerState.addCoins(750.0f);
			Ammo ammo = default(Ammo);
			ammo.type = AmmoTypes.AMMO_SNOWBALL;
			ammo.amount = 200;
			playerState.addAmmo(ammo);
			ammo = default(Ammo);
			ammo.type = AmmoTypes.AMMO_BAUBLE_GRENADE;
			ammo.amount = 5;
			playerState.addAmmo(ammo);
			ammo = default(Ammo);
			ammo.type = AmmoTypes.AMMO_PRESENT_BOMB;
			ammo.amount = 5;
			playerState.addAmmo(ammo);
			shield.SetActive(false);
			DI_Events.EventCenter<string, string, float>.invoke("OnDisplayNotification", "Ammo Cache!", "You have picked up 750 coins, 200 snowballs and 5 bauble grenades!", 5.0f);
			this.gameObject.SetActive(false);
		}

		base.OnTriggerEnter(other);
	}
}
