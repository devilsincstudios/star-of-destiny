// Devils Inc Studios
// // Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;

[AddComponentMenu("Triggers/Tutorial/Three")]
public class TutorialTriggerThree : Trigger
{
	public GameObject waypointThreeField;
	public SphereCollider sphereCollider;

	new public void OnTriggerEnter(Collider other)
	{
		if (shouldTrigger(other)) {
			triggered = true;
			waypointThreeField.SetActive(false);
			this.gameObject.SetActive(false);
		}

		base.OnTriggerEnter(other);
	}

	public void OnEnable()
	{
		DI_Events.EventCenter.addListener("OnTutorialWrapup", handleWrapUp);
		waypointThreeField.SetActive(false);
		sphereCollider = GetComponent<SphereCollider>();
		sphereCollider.enabled = false;
	}

	public void OnDisable()
	{
		DI_Events.EventCenter.removeListener("OnTutorialWrapup", handleWrapUp);
	}

	public void handleWrapUp()
	{
		waypointThreeField.SetActive(true);
		sphereCollider.enabled = true;
	}
}
