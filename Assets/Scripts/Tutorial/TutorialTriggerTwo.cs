// Devils Inc Studios
// // Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;

[AddComponentMenu("Triggers/Tutorial/Two")]
public class TutorialTriggerTwo : Trigger
{
	public GameObject waypointTwoField;

	new public void OnTriggerEnter(Collider other)
	{
		if (shouldTrigger(other)) {
			triggered = true;
			waypointTwoField.SetActive(false);
			this.gameObject.SetActive(false);
		}

		base.OnTriggerEnter(other);
	}

	public void OnEnable()
	{
		waypointTwoField.SetActive(false);
	}
}
