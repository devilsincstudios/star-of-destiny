// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

using System;
using UnityEngine;

[Serializable]
public struct EnemyData
{
	public float health;
	public float speed;
	public Enemies enemyType;
}