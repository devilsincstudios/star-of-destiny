// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

using System;

[Serializable]
public struct EnemyWeights
{
	public Enemies type;
	public float rangeMin;
	public float rangeMax;
}